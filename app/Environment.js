module.exports = {
    CLIENT_ID: "802010002",

    // DEV
    // SERVER_TYPE: "DEV",
    // AUTH_API: "http://192.170.2.6:10000",
    // BASE_API: "http://192.170.2.6:10001",
    // IMAGE_API: "http://192.170.2.6:10003",

    // UAT
    // SERVER_TYPE: "UAT",
    // AUTH_API: "https://uat-api.hengleasing.com:4346",
    // BASE_API: "https://uat-api.hengleasing.com:4348",
    // IMAGE_API: "https://uat-api.hengleasing.com:4347",
    // MINIO_COM_PATH: "http://mediafileapi-uat.hengleasing.local:8001",
    

    // SERVER_TYPE: "UAT",
    // AUTH_API: "https://dev-api.hengleasing.com:4370",
    // BASE_API: "https://dev-api.hengleasing.com:4372",
    // IMAGE_API: "https://dev-api.hengleasing.com:4371",

    // SERVER_TYPE: "DEV",
    // AUTH_API: "http://localhost:20000",
    // BASE_API: "http://localhost:20002",
    // IMAGE_API: "http://localhost:20003",

    //PORDUCTION
    SERVER_TYPE: "PORDUCTION",
    AUTH_API: "https://api.hengleasing.com:4350",
    BASE_API: "https://api.hengleasing.com:4352",
    IMAGE_API: "https://api.hengleasing.com:4351",
    MINIO_COM_PATH: "http://mediafile-api.hengleasing.local:8001",

    //EDU
    // SERVER_TYPE: "UAT",
    // AUTH_API: "https://edu-api.hengleasing.com:4360",
    // BASE_API: "https://edu-api.hengleasing.com:4362",
    // IMAGE_API: "https://edu-api.hengleasing.com:4361",

    MESSAGE: {
        NO_HAVE_DATA: "ไม่พบข้อมูล!!!!",
        SAVE_SUCCESS: "บันทึกข้อมูลสำเร็จ...",
        DELETE_SUCCESS: "ลบข้อมูลเรียบร้อยแล้ว...",
        CANCEL_SUCCESS: "ยกเลิกข้อมูลเรียบร้อยแล้ว...",
        SESSION_EXPIRED: "เซสชั่นหมดอายุ กรุณาเข้าสู่ระบบใหม่อีกครั้ง"
    }
}