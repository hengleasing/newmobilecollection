import { Dimensions } from "react-native";
import React, { useState, useEffect } from "react";

const useScreenDimensions = () => {

    const [screenData, setscreenData] = useState(Dimensions.get('screen'))

    useEffect(() => {
        const onChange = result => {
            setscreenData(result.screen);
        }

        Dimensions.addEventListener('change', onChange);
        return () => Dimensions.removeEventListener('change', onChange);
    }, [])
    return {
        ...screenData,
        isLandScape: screenData.width > screenData.height
    };
}

export default useScreenDimensions;