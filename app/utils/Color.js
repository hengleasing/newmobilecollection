const gradientStart = "#036939";
const gradientMid   = "#328956";
const gradientEnd   = "#76CC94";

export const PRIMARY_GRADIENT_ARRAY = [gradientStart, gradientMid, gradientEnd];

export const PRIMARY_COLOR  = "#00774F";
export const GRAY           = "#ededed";
export const SILVER         = "#BEBEBE";
export const LIGHT_AZURE    = "#E8F3F5";
export const YELLOW         = "#f4d03f";
export const RED            = "#FC4F5B";
export const LIGTH_YELLOW   = "#FFC22E";
export const NAVI           = "#4E7D94";
export const SOLFBLACK      = "#0e1111";
export const LIGHT_SKY_BLUE = "#D5E6EE";
export const LIGHT_GREEN    = '#D3FADE'

export const GLOBLIN_GREEN      = "#3B4A3F";
export const PRIMARY_GREEN      = "#036939";
export const SECONDARY_GREEN    = "#328956";
export const THIRD_GREEN        = "#74B33B";
export const FORTH_GREEN        = "#70C89B";

export const BQuarkFont = 'Quark-Bold';
export const LQuarkFont = 'Quark-Light';