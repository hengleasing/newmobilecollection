import Moment from 'moment'
import AsyncStorage from '@react-native-community/async-storage'
import accounting from "accounting";
import Environment from "../Environment";
import { Auth, getTokenWithSelBranch, appVersionCheck } from "../state/action/AuthAction";
import deviceInfoModule from 'react-native-device-info';
import Popup from "../components/common/CustomModal/Popup";
import { PermissionsAndroid, Linking, ToastAndroid } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import { setDownloadProgress } from '../state/reducer/AuthReducer';

const buddistEra = (date) => {
    return date !== undefined ? Moment(date).format('DD/MM') + '/' + (parseInt(Moment(date).format('YYYY')) + 543) : '-'
}

const buddistEraWithTime = (date) => {
    return date !== undefined ? Moment(date).format('DD/MM') + '/' + (parseInt(Moment(date).format('YYYY')) + 543) + ' ' + Moment(date).format('HH:mm:ss') : '-'
}

const getAccessToken = async () => {
    let token = await AsyncStorage.getItem('accessToken');
    return token;
}

const getRefreshToken = async () => {
    return await AsyncStorage.getItem('refreshToken');
}

const isCloseToBottom = async (nativeEvent, fetch, curPage, setPage, type, contractId) => {
    if (nativeEvent.layoutMeasurement.height + nativeEvent.contentOffset.y >= nativeEvent.contentSize.height - 1) {


        if (type == "appoint"){
            let index = curPage + 1
            await fetch(contractId,index * 10, 10)
            await setPage(index)
        }
        else{
            let index = curPage + 1
            await fetch(index * 10, 10)
            await setPage(index, type)
        }
    }
}

const formatMoney = (value) => {

    let money = accounting.formatMoney(value, "", 2, ",", ".")
    return money
}

const checkAuth = (dispatch) => {
   
    const initLoad = async () => {
        const token = await getRefreshToken()

        if(token && token != ""){
            getRefresh(token)
        }
        
    }

    const getRefresh = async (token) => {
        const username = await AsyncStorage.getItem('username')
        const data = {
            client_id : Environment.CLIENT_ID,
            username : username,
            refresh_token : token,
            grant_type : "refresh_token"
        }
        try {
            dispatch(Auth(data))

            // NavigationService.navigate('Secondary')
        } catch (error) {
            NavigationService.navigateWithoutStack('Login')
        }
    }

    initLoad()
}

const checkAppVersion = async (dispatch) => {
    const currentVersion = deviceInfoModule.getBuildNumber()
    const lastVersion = await appVersionCheck()(dispatch)

    const downloadNewVersion = () => {
        const fileName = lastVersion.apkUrl.split('/')
        
        Linking.canOpenURL(lastVersion.apkUrl)
        .then(supported => {
            if(supported){
                ToastAndroid.show("กำลังดาวน์โหลด กรุณารอสักครู่...", ToastAndroid.LONG);
                RNFetchBlob.config({
                    path: RNFetchBlob.fs.dirs.DownloadDir + `/${fileName[fileName.length -1]}`
                })
                .fetch('GET',lastVersion.apkUrl)
                .progress({interval: 250 }, (received, total) => {
                    let percent = Math.round((received/total)*100)
                   
                    dispatch(setDownloadProgress(percent))
                })
                .then( res => {
                    dispatch(setDownloadProgress(0))
                    RNFetchBlob.android.actionViewIntent(res.path(), 'application/vnd.android.package-archive')
                })
            }
            else
                Popup.show({
                    type: 'Warning',
                    title: 'พบข้อผิดพลาด',
                    textBody: 'ไม่สามารถอัพเดทแอพจัดเก็บได้ เนื่องจาก url ไม่ถูกต้อง กรุณาติดต่อ 8800',
                    buttonText: 'ตกลง',
                    callback: () => Popup.hide()
                })
        })
        .catch(error => {
            Popup.show({
                type: 'Warning',
                title: 'พบข้อผิดพลาด',
                textBody: JSON.stringify(error),
                buttonText: 'ตกลง',
                callback: () => Popup.hide()
            })
        })
    }

    if(lastVersion.versionCode > currentVersion)
    {
        Popup.show({
            type:   'Confirm',
            title:  'อัพเดตเวอร์ชัน',
            textBody:   `รายละเอียด: ${lastVersion.detl} \n`+
                        `ต้องการอัพเดตเวอร์ชัน ${lastVersion.versionName} หรือไม่?`,
            callback: async (e) => {
                if (e){
                    let granted = await PermissionsAndroid.check("android.permission.WRITE_EXTERNAL_STORAGE")
                    
                    if(granted)
                        downloadNewVersion()
                    else
                        PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
                        .then(res =>{
                            if(res == 'granted')
                            downloadNewVersion()
                            else
                                Popup.show({
                                    type: 'Warning',
                                    title: 'คำเตือน',
                                    textBody: 'ไม่สามารถอัพเดทแอพจัดเก็บได้ เนื่องจากคุณไม่อนุญาติให้เข้าถึงบันทึกข้อมูล',
                                    buttonText: 'ตกลง',
                                    callback: () => Popup.hide()
                                })
                        })
                }

                Popup.hide()
            }
        })
    }
}

export {
    buddistEra, 
    buddistEraWithTime, 
    getAccessToken, 
    getRefreshToken, 
    isCloseToBottom, 
    formatMoney, 
    checkAuth,
    checkAppVersion
}