import { StyleSheet } from "react-native";
import { LQuarkFont, PRIMARY_COLOR, GRAY } from "./Color";

const GeneralStyle = StyleSheet.create({
    PrimaryButton: {
        borderRadius: 50,
        backgroundColor: PRIMARY_COLOR,
        width: '60%',
        alignItems: 'center',
        paddingVertical: 10,
        marginVertical:10,
        borderColor: GRAY,
        borderWidth: 1
    },
    ButtonText: {
        color: 'white',
        fontSize: 30,
        fontFamily: LQuarkFont
    },
    UnderlineInput: {
            marginVertical:10,
            paddingHorizontal: 10,
            color: 'white',
            borderColor: GRAY,
            borderBottomWidth: StyleSheet.hairlineWidth,
            minWidth: '80%',
            fontSize: 20,
            fontFamily: LQuarkFont
    }
})
export default GeneralStyle