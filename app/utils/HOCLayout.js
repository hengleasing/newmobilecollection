import React, { Component, createContext } from 'react'
import LinearGradient from 'react-native-linear-gradient'
import { PRIMARY_GRADIENT_ARRAY } from "../utils/Color";
import { StyleSheet } from 'react-native';
const LayoutContext = createContext();


class GradientLayout extends Component {
    render() {
        return (
            <LayoutContext.Provider>
                <LinearGradient style={styles.container} colors={PRIMARY_GRADIENT_ARRAY} start={{x:1, y:0 }} end={{x:0, y:1 }}>
                    {this.props.children}
                </LinearGradient>
            </LayoutContext.Provider>
        )
    }
}

const styles= StyleSheet.create({
    container: {
        flex: 1
    }
})

export default GradientLayout
