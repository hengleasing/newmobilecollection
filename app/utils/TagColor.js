import moment from "moment";
import { BQuarkFont, PRIMARY_GREEN, YELLOW, THIRD_GREEN, FORTH_GREEN, RED, LIGTH_YELLOW, NAVI } from "./Color";
export default getTagColor = (targetOut, periodOut, inverse, date) => {

    const initTagReverse = (target, period) => {
        if(target === period && period === 0 )
            return FORTH_GREEN;
        else if (target !== period && period === 0)
            return RED;
        else if(target > period && period > 0)
            return LIGTH_YELLOW;
        else if(target <= period && period > 0)
            return FORTH_GREEN
        else
            return NAVI
    }

    const initTag = (period) => {
        switch (parseInt(period)) {
            case 0:
            case 1:
                return FORTH_GREEN;
            case 2:
                return LIGTH_YELLOW;
            case 3:
                return RED;
            default:
                if(period > 3)
                    return NAVI
        }
    }

    const appointDate = (d) =>{
        const datenow = moment().startOf('day')

        return (moment(datenow).diff(d, 'days') >= 0) ?  RED : FORTH_GREEN
    }

    if(date!=null && date!=undefined)
        return appointDate(date)
    else
        if(inverse)
            return initTagReverse(targetOut, periodOut)
        else
            return initTag(periodOut)
}