import { useDispatch, useSelector } from "react-redux";
import axiosInstance from "../services/AxiosService"
import Environment from "../Environment";
import { updateAssignedList, setAssignedListCtl, setAssignLoading } from "../state/reducer/WorkListReducer";
import useAction from "./useAction";

const useAssignedLoadmore = () =>{

    const dispatch = useDispatch();
    const assignedCtl = useSelector(({WorkListReducer})=> WorkListReducer.assignedListCtl);
    const assignedList = useSelector(({WorkListReducer})=> WorkListReducer.assignedList);

    const updateAssign = useAction(updateAssignedList)
    
    const fetch = async (filter) => {
      
        const data = {
            limit_start : assignedCtl.page * assignedCtl.len,
            limit_len : assignedCtl.len,
            assignTo: "",
            paymentStatus: (filter && filter.paymentStatus)?filter.paymentStatus.value:"", //→ สถานะการจ่ายเงิน 0, 1
            orderByRemain:  (filter && filter.orderByRemain)?filter.orderByRemain.value:"",//->เรียงตามเงินต้นคงเหลือ 0:ต่ำสุด 1:สูงสุด
            overAging:  (filter && filter.overAging)?filter.overAging.value:"", //→ 0 ลูกหนี้ ปกติ 1:ลูกหนี้นอก aging
            agingStatus:  (filter && filter.agingStatus)?filter.agingStatus.value:"", //→ กรองตามชั้นลูกหนี้  id
            contractNo: (filter && filter.contractNo)?filter.contractNo:"",
            cusFullname: (filter && filter.cusFullname)?filter.cusFullname:"",
            cusRegis:(filter && filter.cusRegis)?filter.cusRegis:""
        };

        let url = `${Environment.BASE_API}/api/customers/collections/assigned`
        let res = await axiosInstance.post(url, data)
 
        if(res.data){
           
            let newAss = assignedList.concat(res.data)
            dispatch(updateAssignedList(newAss));
            dispatch(setAssignedListCtl({...assignedCtl, page: (assignedCtl.page + 1) }));
            dispatch(setAssignLoading(false))
        }
    }

    const clear = () =>{

        updateAssign([])
        dispatch(setAssignedListCtl({ page:0, len:20}));

    }

    return {fetch, clear}

}


export default useAssignedLoadmore;