import React, { useState } from "react";
import { View, TouchableOpacity, StyleSheet, TextInput, Text } from "react-native";
import { GRAY } from "../../utils/Color";
import FIcon from "react-native-vector-icons/Feather";
import { Auth } from "../../state/action/AuthAction";
import GeneralStyle from "../../utils/GeneralStyle";
import useAction from "../../utils/useAction";

const LoginForm = () => {

    const login = useAction(Auth)
    const [hidePassword, setHidePassword] = useState(true)
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const handleLogin = async() => { 
        let dataUser= {
            client_id: '802010002',
            username: username.toLowerCase(),
            password: password,
            grant_type:'password'
          }

          await login(dataUser)
    }

    const handleHide = () => {
        setHidePassword(!hidePassword)
    }

    const handleUserName = (value) => {
        setUsername(value)
    }

    const handlePassword = (value) => {
        setPassword(value)
    }


    return(
        <View style={styles.container}>
            <View>
                <TextInput
                    style={GeneralStyle.UnderlineInput}
                    value={username}
                    onChangeText={handleUserName}
                    placeholder="ชื่อผู้ใช้งาน"
                    placeholderTextColor={GRAY}
                    returnKeyLabel="ถัดไป"
                    returnKeyType="next"
                />
            </View>
            <View style={styles.InputContent}>
                <TextInput
                    style={GeneralStyle.UnderlineInput}
                    value={password}
                    onChangeText={handlePassword}
                    placeholder="รหัสผ่าน"
                    placeholderTextColor={GRAY}
                    returnKeyLabel="เข้าสู่ระบบ"
                    returnKeyType="done"
                    secureTextEntry={hidePassword}
                />
                <TouchableOpacity style={{ marginLeft: -25 }} onPress={handleHide}>
                    {hidePassword ?
                        <FIcon
                            name="eye-off"
                            size={20}
                            color={GRAY}
                            style={styles.hidePassStyle} /> :
                        <FIcon
                            name="eye"
                            size={20}
                            color={GRAY}
                            style={styles.hidePassStyle} />
                    }
                </TouchableOpacity>
            </View>
            <TouchableOpacity
                style={GeneralStyle.PrimaryButton}
                onPress={handleLogin}
            >
                <Text style={GeneralStyle.ButtonText}>ลงชื่อเข้าใช้</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center'
    },
    hidePassStyle:{
        // paddingBottom: 30
    },
    InputContent: {
        flexDirection: 'row',
        alignItems: 'center'
    }
})

export default LoginForm