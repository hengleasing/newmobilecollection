import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity, StyleSheet, ToastAndroid, Modal, Dimensions, Image } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import GradientLayout from "../../../utils/HOCLayout";
import BranchSelector from "./BranchSelector";
import GeneralStyle from "../../../utils/GeneralStyle";
import FAIcon from "react-native-vector-icons/FontAwesome";
import { SILVER, LIGHT_SKY_BLUE, LQuarkFont, BQuarkFont, YELLOW } from "../../../utils/Color";
import NavigationService from "../../../services/NavigationService";
import useAction from "../../../utils/useAction";
import { GetAnnonucement, getTokenWithSelBranch, saveAnnouncement } from "../../../state/action/AuthAction";
import Environment from "../../../Environment";
import { getRefreshToken, checkAppVersion } from "../../../utils/utils";
import * as Progress from 'react-native-progress';

const Secondary = () => {
    const [branch, setBranch] = useState()
    const [group, setGroup] = useState()
    const [visible, setVisible] = useState(true)

    const dispatch = useDispatch()
    const loginWithBranch = useAction(getTokenWithSelBranch)
    const fetchAnnouncement = useAction(GetAnnonucement)
    const acceptAnnoucement = useAction(saveAnnouncement)
    const tempUser = useSelector(({ AuthReducer }) => AuthReducer.tempUser)
    const userData = useSelector(({ AuthReducer }) => AuthReducer.userData)
    const percent = useSelector(({ AuthReducer: { progress } }) => progress)
    const announcement = useSelector(({ AuthReducer: { announcement } }) => announcement)

    useEffect(() => {
        initialLoad()
    }, [])

    const initialLoad = () => {
        checkAppVersion(dispatch)
        fetchAnnouncement()
    }

    const handlePress = async () => {
        let splitGroup = group.split('_')
        
        if(branch !== splitGroup[1])
            ToastAndroid.showWithGravity('กรุณาเลือกสาขาและตำแหน่งให้ตรงกัน', ToastAndroid.LONG, ToastAndroid.CENTER)
        else
            if (branch && group) {
                const refreshToken = await getRefreshToken()
                const data = {
                    client_id: Environment.CLIENT_ID,
                    username: tempUser.username,
                    password: tempUser.password,
                    grant_type: refreshToken ? "refresh_token" : "password",
                    branchId: branch,
                    groupId: splitGroup[0],
                    refresh_token: refreshToken ? refreshToken : ''
                };

                await loginWithBranch(data);

                NavigationService.navigate('WorkList');
            }
            else {
                ToastAndroid.showWithGravity('กรุณาเลือกสาขาและตำแหน่ง', ToastAndroid.LONG, ToastAndroid.CENTER)
            }
    }

    const handleBack = () => {
        NavigationService.backNavigate()
    }

    const handleAccept = () => {
        let data = {
            userID: userData.employeeID,
            userFname: userData.givenName,
            userLname: userData.surename,
            username: userData.samAccountName,
            channelType: 'Collection'
        }
        acceptAnnoucement(data)
        setVisible(false)
    }
    
    return (
        <GradientLayout>
            {announcement != undefined && announcement.length > 0 &&
                <Modal
                    visible={visible}
                    transparent={true}
                    animationType="fade"
                >
                    <View style={styles.announcementContainer}>
                        <View style={styles.announcement}>
                            <Image
                            source={{uri: announcement[0]}}
                            style={{width: width*0.9, height: Math.round((width*9) / 16)}}
                            resizeMode="center"
                            />
                            <TouchableOpacity
                            style={styles.btnAnnouncement}
                            onPress={handleAccept}
                            >
                                <Text style={styles.textAnnouncementBtn}>รับทราบ</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            }
            <Modal
                transparent={true}
                visible={percent !== 0 && percent !== 100}
                animationType="fade"
            >
                <View style={styles.modalContent}
                >
                    <Progress.Circle
                        progress={percent / 100}
                        color="red"
                        showsText={true}
                        direction="clockwise"
                        indeterminate={false}
                        size={200}
                        textStyle={{ fontFamily: LQuarkFont }}
                    />
                    <Text style={styles.downloadText}>กำลังดาวน์โหลด...</Text>
                </View>
            </Modal>
            <View style={styles.userIcon}>
                <FAIcon name="user-circle" size={200} color={SILVER} />
            </View>
            {
                Environment.SERVER_TYPE != "PORDUCTION" && <Text style={{fontSize: 50, color: 'white', textAlign: 'center'}} >**ตัวทดสอบ**</Text>
            }
            <BranchSelector setInfo={[setBranch, setGroup]} />
            <View style={styles.SignInContain}>
                <TouchableOpacity style={GeneralStyle.PrimaryButton} onPress={handlePress}>
                    <Text style={GeneralStyle.ButtonText} >เข้าสู่ระบบ</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={handleBack}>
                    <Text style={styles.GobackText}>กลับ</Text>
                </TouchableOpacity>
            </View>
        </GradientLayout>
    )
}

const { width, height } = Dimensions.get("window")

const styles = StyleSheet.create({
    SignInContain: {
        flexGrow: 1,
        alignItems: 'center'
    },
    userIcon: {
        flexGrow: 1,
        alignItems: 'center',
        paddingTop: 100
    },
    GobackText: {
        color: 'white',
        fontSize: 16
    },
    downloadText: {
        fontFamily: LQuarkFont,
        fontSize: 20
    },
    modalContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        backgroundColor: 'rgba(216,229,227, 0.3)'
    },
    announcement: {
        backgroundColor: '#FCFCFC',
        borderRadius: 5,
        width: width*0.95,
        height: height*0.45,
        alignItems: 'center'
    },
    announcementContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnAnnouncement: {
        backgroundColor: YELLOW,
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderRadius: 50,
        alignItems: 'center',
        width: width * 0.5
    },
    textAnnouncementBtn: {
        fontSize : 20,
        color: 'white',
        fontFamily: BQuarkFont
    }
})

export default Secondary;