import React, { useEffect, useState } from "react";
import { View, Picker, StyleSheet } from "react-native";
import { useSelector } from "react-redux";

const BranchSelector = ({ setInfo }) => {

    const { Item } = Picker
    const userData = useSelector(({ AuthReducer: { userData } }) => userData)
    const [branch, setBranch] = useState()
    const [group, setGroup] = useState()

    useEffect(() => {
        init()
    }, [userData])


    const init = () => {
        if (userData) {
            setInfo[0](userData.userBranches[0].id)
            setInfo[1](`${userData.userGroups[0].id}_${userData.userGroups[0].branchID}`)
        }
    }

    const handleBranch = (value) => {
        setInfo[0](value)
        setBranch(value)
    }

    const handleGroup = (value) => {
        setGroup(value)
        setInfo[1](value)
    }
    
    return (
        <View style={styles.container}>
            <View style={styles.pickerStyle}>
                <Picker onValueChange={handleBranch} selectedValue={branch}>
                    {userData ? userData.userBranches.map((branch, iBranch) => <Item key={iBranch} label={`(${branch.id}) ${branch.name}`} value={branch.id} />) : <Item label="เลือกสาขา" value="default" />}
                </Picker>
            </View>
            <View style={styles.pickerStyle}>
                <Picker onValueChange={handleGroup} selectedValue={group} >
                    {userData ? userData.userGroups.map((group, iGroup) => <Item key={iGroup} label={`(${group.branchID}) ${group.name}`} value={`${group.id}_${group.branchID}`} />) : <Item label="เลือกตำแหน่ง" value="default" />}
                </Picker>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    pickerStyle: {
        backgroundColor: 'white',
        minWidth: '80%',
        width: '80%',
        marginVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 5
    }

})

export default BranchSelector;