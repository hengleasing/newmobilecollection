import {View, Text, StatusBar, StyleSheet, Image, PermissionsAndroid} from 'react-native';
import React, {Component, useEffect} from 'react';
import LoginForm from './LoginForm';
import GradientLayout from '../../utils/HOCLayout';
import { useDispatch } from 'react-redux';
import { checkAuth } from '../../utils/utils';
import deviceInfoModule from 'react-native-device-info';

const logo = require("../../../assets/images/heng_logo.jpg")

const index = ({navigation}) => {
    
    const dispatch = useDispatch()
    const versionName = deviceInfoModule.getVersion()

    useEffect(()=>{
        initload()
    },[])

    const initload = () => {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(res => {
            if (!res)
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION)
        })
        checkAuth(dispatch)
    }

    return (
      <GradientLayout>
            <StatusBar barStyle="light-content" />
            <View style={styles.imageContainer}>
                <Image source={logo} style={styles.logo} />
            </View>
            <View style={styles.form}>
                <LoginForm />
            </View>
            <View style={styles.version}>
                <Text style={styles.textVersion}>Version {versionName}</Text>
            </View>
        </GradientLayout>
    );
}

const styles = StyleSheet.create({
    form: {
        flexGrow: 1,
    },
    logo: {
        width: 150,
        height: 150
    },
    imageContainer:{
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1
    },
    textVersion: {
        fontSize: 10,
        color:'white'
    },
    version: {
        width:'100%',
        alignItems:'flex-end'
    }
})

export default index