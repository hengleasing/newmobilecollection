import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import useScreenDimensions from "../../../utils/useScreenDimensions";
import { BQuarkFont, LQuarkFont } from "../../../utils/Color";
import { useSelector } from "react-redux";

const Header = () => {
    const { width, fontScale } = useScreenDimensions()
    const {taxId, branchInformation } = useSelector(({PaymentReducer})=> PaymentReducer)
   
    return(
        <View style={styles.container}>
            <View style={styles.contain}>
                <Image 
                    style={[
                        styles.imageContain, 
                        {
                        width: width*0.175,
                        height: width*0.175}]}
                    source={require("../../../../assets/images/heng_logo.jpg")}
                    />
                <View>
                    <Text style={styles.containText}>บริษัท เฮงลิสซิ่ง จำกัด</Text>
                    <Text style={styles.containText}>Heng Leasing Company Limited</Text>
                </View>
            </View>
            <View style={styles.subContain}>
                <Text style={styles.text}>เลขประจำตัวผู้เสียภาษี: {taxId}</Text>
                {branchInformation && <Text style={styles.text}>{branchInformation.bR_AD_NO} หมู่ {branchInformation.bR_MOO} ต.{branchInformation.bR_TAMBOL} อ.{branchInformation.bR_AMPR} จ.{branchInformation.bR_PROV} {branchInformation.bR_ZIP}</Text>}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        // flex:1,
        alignContent: 'center',
    },
    imageContain: {
        marginRight: 10,
    },
    contain: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 5
    },
    subContain: {
        alignItems: 'center'
    },
    containText: {
        fontSize: 20,
        fontFamily: BQuarkFont
    },
    text: {
        fontFamily: LQuarkFont,
        fontSize: 18
    }
})

export default Header;