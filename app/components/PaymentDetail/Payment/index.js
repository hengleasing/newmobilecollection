import React, { useEffect, useState } from "react";
import { View, StyleSheet, Text, KeyboardAvoidingView, Modal, FlatList, TouchableOpacity, PermissionsAndroid, TextInput, Button, DeviceEventEmitter } from "react-native";
import Header from "./header";
import Body from "./body";
import Footer from "./footer";
import useAction from "../../../utils/useAction";
import { checkIsCloseContract, getTaxId, getTempReceipt } from "../../../state/action/PaymentAction";
import { LQuarkFont, BQuarkFont, SECONDARY_GREEN, NAVI, PRIMARY_GREEN, LIGHT_SKY_BLUE, SILVER } from "../../../utils/Color";
import DismissKeyboard from "../../common/DismissKeyboard";
import Anotation from "./anotation";
import RNPosPrinter, { printerCommand, PrinterConstants } from "react-native-pos-printer";
import { Root, Popup } from "../../common/CustomModal";
import useScreenDimensions from "../../../utils/useScreenDimensions";
import { useForm, Controller } from "react-hook-form";
import { useSelector } from "react-redux";
import accounting from "accounting";
import RNFetchBlob from 'rn-fetch-blob'
import { buddistEra } from "../../../utils/utils";
import moment from "moment";
import { ScrollView } from "react-native";


const PaymentPage = (props) => {

  const customerDetail = useSelector(({ WorkListReducer }) => WorkListReducer)
  const { taxId } = useSelector(({PaymentReducer})=> PaymentReducer)
  
  const { width, height } = useScreenDimensions()
  const { register, setValue, handleSubmit, errors, getValues } = useForm({
    defaultValues: {
      contractNo: customerDetail.customerDetail.contractNo,
      receiptAmount: customerDetail.customerPayment.amount.toString()
    }
  });
 
  const fetchTaxId = useAction(getTaxId)
  const postTempReceipt = useAction(getTempReceipt)
  const isCloseContract = useAction(checkIsCloseContract)
  const [devices, setDevices] = useState([])
  const [visible, setVisible] = useState(false)
  const [innerVisible, setInnerVisible] = useState(false)
  const [selectedDevice, setSelectedDevice] = useState()
  const [amount, setAmount] = useState()
  const [innerDisabled, setInnerDisabled] = useState(true)
  const [getReciptDis, setgetReceiptDis] = useState(false)

  useEffect(() => {
    fetchTaxId()
    RNPosPrinter.init()
  }, [])

  useEffect(() => {
    register({ name: 'receiptAmount' })
    register({ name: 'contractNo' })

  }, [register])

  const handlePressGetReceipt = () => {
    Popup.show({
      type: 'Success',
      title: 'ตรวจสอบสถานะสัญญา',
      textBody: 'กรุณารอสักครู่ ...',
      buttonText: 'ตกลง',
      button:false,
      timing: 2000,
      autoClose: true,
      callback: () => {
        Popup.hide()
      }
    })
    setTimeout(async () => {
      let isClose = await isCloseContract(customerDetail.customerDetail.contractNo)
    if(isClose.status){
      if(!isClose.data)
       {
          RNPosPrinter.getDevices()
          .then(res => setDevices(res)
          )
          .catch(error => Popup.show({
            type: 'Danger',
            title: 'พบข้อผิดพลาด',
            textBody: JSON.stringify(error),
            buttonText: 'ตกลง',
            timming: 5000,
            autoClose: true,
            callback: () => {
              Popup.hide()
            }
          }))
          setVisible(!visible)
       }
        else
          Popup.show({
            type: 'Danger',
            title: 'สัญญานี้ได้ปิดบัญชีไปแล้ว',
            textBody: 'ไม่สามารถออกใบเสร็จชั่วคราวได้',
            buttonText: 'ตกลง',
            timing: 5000,
            autoClose: true,
            callback: () => {
              Popup.hide()
            }
          })
    }
    }, 2000);
  }

  const handleSelectDevice = async device => {
    let check = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
 
    if (check) {
      setSelectedDevice(device)
      setInnerVisible(!innerVisible)
    }
    else
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
  }

  const submit = async data => {
    RNPosPrinter.connectDevice(selectedDevice.identifier, 3000).then(async res => {
      setInnerDisabled(true)
      Popup.show({
        type: 'Warning',
        title: 'เครื่องพิมพ์กำลังทำงาน',
        textBody: 'กรุณารอสักครู่ ...',
        button: false,
        buttonText: 'ตกลง',
        timing: 3000,
        autoClose: true,
        callback: () => {
  
        }
      })

      let result = await postTempReceipt(data)

      if (result.status == false) {
        setVisible(!visible)
        setInnerVisible(false)
        Popup.show({
          type: 'Danger',
          title: 'แจ้งเตือน',
          textBody: JSON.stringify(result.data),
          button: true,
          buttonText: 'ตกลง',
          timing: 4000,
          autoClose: false,
          callback: () => {
            Popup.hide()
          }
        })
      }
      else {
        let printRes = printReceipt(result)
        if (printRes) {
          setInnerVisible(!innerVisible)
          setInnerDisabled(!innerDisabled)
          setgetReceiptDis(true)
          setVisible(!visible)
        }
        else {
          Popup.show({
            type: 'Danger',
            title: 'พบข้อผิดพลาด',
            textBody: `ไม่สามารถพิมพ์ใบเสร็จได้`,
            buttonText: 'ตกลง',
            timming: 5000,
            autoClose: true,
            callback: () => {
              Popup.hide()
            }
          })
        }
      }

    })
      .catch(error => {
          setInnerVisible(!innerVisible)
          setInnerDisabled(!innerDisabled)
          setVisible(!visible)
       
        return Popup.show({
          type: 'Danger',
          title: 'พบข้อผิดพลาด',
          textBody: JSON.stringify(error),
          buttonText: 'ตกลง',
          autoClose: false,
          callback: () => {
            Popup.hide()
          }
        })
      })
  }

  const replaceText = (text) => {
    if (text) {
      let splitText = text.split(" ")
      let replacedText = splitText.map(find => {
        let check = find.indexOf('ำ')
        if (check != -1) {
          return find.replace("ำ", "ํา")
        }
        else {
          return find
        }
      })
      return replacedText.join(" ")
    }
  }

  const printReceipt = async data => {
    const receipt = {
      logoURL: "https://www.hengleasing.com/wp-content/uploads/2019/02/Asset-3.png",
      TIN: taxId,
      addressLine1: replaceText(data.temporaryReceipt.addressLine1),
      addressLine2: replaceText(data.temporaryReceipt.addressLine2),
      receiptNo: data.temporaryReceipt.receipT_NO,
      customerName: replaceText(customerDetail.customerDetail.name),
      contractNo: data.temporaryReceipt.sY_NO,
      receiptDate: buddistEra(moment(data.temporaryReceipt.receipT_DATE, "YYYY/MM/DD")),
      receiptTime: moment(data.temporaryReceipt.receipT_DATE).format("HH:mm:ss"),
      receiptAmount: accounting.formatNumber(accounting.toFixed(data.temporaryReceipt.receipT_AMT, 0)),
      collectorName: replaceText(data.temporaryReceipt.debtCollector),
      // isCopy: next.paymentState.paymentButtonState,
      // numOfCopy: next.paymentState.numberOfCopy ? next.paymentState.tempReceiptCount : next.paymentState.tempReceiptCountCopy
    };

    const cmd = [
      printerCommand.setPrinter(PrinterConstants.Command.CODE_PAGE, 26)
    ];

    const logo = await RNFetchBlob.config({
      path: `${RNFetchBlob.fs.dirs.DownloadDir}/heng_logo.png`
    }).fetch("GET", receipt.logoURL);

    cmd.push(printerCommand.printImageFromStorage(logo.path(), 250, 70));

    cmd.push(
      printerCommand.setPrinter(
        PrinterConstants.Command.ALIGN,
        PrinterConstants.Command.ALIGN_CENTER
      ),
      printerCommand.setPrinter(PrinterConstants.Command.FONT_MODE, 1),
      printerCommand.printLine(
        `เลขที่ประจําตัวผู้เสียภาษี: ${receipt.TIN}`
      ),
      printerCommand.printLine(receipt.addressLine1),
      printerCommand.printLine(receipt.addressLine2),
      printerCommand.printSeparator30("-------"),
      printerCommand.printLine("")
    );

    cmd.push(
      printerCommand.setPrinter(
        PrinterConstants.Command.ALIGN,
        PrinterConstants.Command.ALIGN_LEFT
      ),
      printerCommand.setPrinter(PrinterConstants.Command.FONT_MODE, 0),
      printerCommand.printLine(
        `เลขที่ใบเสร็จรับเงิน: ${receipt.receiptNo}`
      ),
      printerCommand.printLine(`ชื่อลูกค้า: ${receipt.customerName}`),
      printerCommand.printLine(`เลขที่สัญญา: ${receipt.contractNo}`),
      printerCommand.printLine(`วันที่ชําระเงิน: ${receipt.receiptDate}`),
      printerCommand.printLine(`เวลา: ${receipt.receiptTime}`),
      printerCommand.printLine(
        `จํานวนเงินที่ชําระ: ${receipt.receiptAmount}`
      ),
      printerCommand.printLine(`ชําระโดย: เงินสด`),
      printerCommand.printLine(`ผู้รับเงิน: ${receipt.collectorName}`),
      printerCommand.setPrinter(PrinterConstants.Command.FONT_MODE, 1),
      printerCommand.printLine(""),
      printerCommand.setPrinter(
        PrinterConstants.Command.ALIGN,
        PrinterConstants.Command.ALIGN_CENTER
      ),
      printerCommand.printLine("ลงชื่อ _____________________________"),
    );

    cmd.push(
      printerCommand.setPrinter(
        PrinterConstants.Command.ALIGN,
        PrinterConstants.Command.ALIGN_CENTER
      ),
      printerCommand.setPrinter(PrinterConstants.Command.FONT_MODE, 1),
      printerCommand.printLine(`(${receipt.collectorName})`),
      printerCommand.printLine(""),
      printerCommand.printLine("โปรดตรวจสอบรายการชําระเงินของท่าน"),
      printerCommand.printLine("และเก็บไว้เป็นหลักฐาน"),
      printerCommand.printLine("หากไม่ถูกต้องกรุณาแจ้งกลับเจ้าหน้าที่ทันที"),
      printerCommand.printLine("หากมีข้อสงสัยประการใด ติดต่อ 1361"),
      printerCommand.printLine(""),
      printerCommand.printLine(`วันที่พิมพ์: ${receipt.receiptDate}`),
      printerCommand.printLine(`เวลาพิมพ์: ${receipt.receiptTime}`),
      // printerCommand.printLine(`พิมพ์ครั้งที่ ${receipt.numOfCopy} ${receipt.isCopy == 2 ? '(สําเนา)' : ''}`),
      printerCommand.printLine(""),

      printerCommand.printLine(""),
      printerCommand.printLine("")
    );

    try {
      
      RNPosPrinter.printerModule.addCommands(cmd);
      // if (this.deviceEventEmitter) this.deviceEventEmitter.remove();
      // DeviceEventEmitter.removeListener()
      return true
    } catch (e) {
      return false
    }
  }

  const handleCancel = () => {
    setInnerVisible(!innerVisible)
    setInnerDisabled(true)
  }

  const calculateChange = value => {
    let amount = getValues("receiptAmount")
    let paymentAmount = accounting.toFixed(parseInt(amount), 0)
    let receive = accounting.toFixed(parseInt(value), 0)
    let total = accounting.toFixed(receive - paymentAmount)
     
      if(Math.sign(total) == -1 || amount == 0 ){
        setInnerDisabled(true)
        setAmount(0)
      }
      else{
        setInnerDisabled(false)
        setAmount(accounting.formatMoney(total, ''))
      }
  }

  const handleFinished = () => {
    RNPosPrinter.init()
    props.navigation.goBack()
  }

  const modalBluetooth = () => (
    <Modal
      animationType="fade"
      transparent={true}
      onRequestClose={() => setVisible(!visible)}
      visible={visible}
    >
      <View style={styles.modalLayout}>
        <View style={[styles.bluetoothList, { height: height * 0.45, width: width * 0.95 }]}>
          <Text style={{ textAlign: 'center', fontFamily: BQuarkFont, fontSize: 18 }} >อุปกรณ์บลูทูธที่เคยเชื่อมต่อ</Text>
          <FlatList
            data={devices}
            keyExtractor={device => device.identifier}
            renderItem={({ item }) => (

              <TouchableOpacity
                style={styles.deviceList}
                // onPress={handleSubmit(submit)}
                onPress={() => handleSelectDevice(item)}
                title={item.name}
              >
                <Text style={styles.deviceText} >{item.name}</Text>
              </TouchableOpacity>
            )}
          />

          <View style={{ alignItems: 'center', marginBottom: 10 }}>
            <TouchableOpacity
              style={styles.closeButton}
              onPress={() => setVisible(!visible)}
            >
              <Text style={styles.deviceText}>ปิด</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <Modal
        visible={innerVisible}
        transparent={true}
        animationType="fade"
        onRequestClose={() => { }}
      >
        <View style={styles.modalLayout}>
          <View style={[styles.innerModal, { height: height * 0.3, width: width * 0.7 }]}>
            <View style={styles.innderModalContent}>
                <Text style={styles.text}>ยอดเงินที่ชำระ </Text>
                <Text style={styles.text}>{getValues('receiptAmount')}</Text>
              </View>
            <View style={styles.innderModalContent}>
              <Text style={styles.text}>รับเงิน </Text>
              <TextInput
                keyboardType="numeric"
                style={{
                  borderColor: '#B7B7B7',
                  borderWidth: 1,
                  borderRadius: 5,
                  paddingVertical: 0,
                  width: 80
                }}
                onChangeText={calculateChange}
              />
            </View>
            <View style={styles.innderModalContent}>
                <Text style={styles.text}>เงินทอน </Text>
                <Text style={styles.text} >{amount}</Text>
              </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 10, justifyContent: 'space-between', marginHorizontal: 30 }}>
                <TouchableOpacity
                  style={[styles.closeButton, innerDisabled && {backgroundColor: SILVER}]}
                  onPress={handleSubmit(submit)}
                  disabled={innerDisabled}
                >
                  <Text style={styles.deviceText} >บันทึก</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.closeButton}
                  onPress={handleCancel}
                >
                  <Text style={styles.deviceText} >ยกเลิก</Text>
                </TouchableOpacity>
              </View>
          </View>
        </View>
      </Modal>
    </Modal>
  )

  return (
    <DismissKeyboard>
      <Root>
        <KeyboardAvoidingView style={visible ? styles.containerWithOpa : styles.container} behavior="height" enabled keyboardVerticalOffset={100}>
          <ScrollView>
          <View style={styles.headerContainer}>
            <Header />
          </View>
          <View>
            <Body setValue={setValue} errors={errors} />
          </View>
          <Anotation />
          <View style={styles.footerContainer}>
            <Footer />
          </View>
          <TouchableOpacity
            onPress={handlePressGetReceipt}
            disabled={getReciptDis}
            style={[styles.paymentButton, getReciptDis ? { backgroundColor: SILVER } : { backgroundColor: SECONDARY_GREEN }]}
          >
            <Text style={styles.buttonText} >ออกใบเสร็จรับชำระเงิน</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.paymentButton}
          onPress={handleFinished}
          >
            <Text style={styles.buttonText}>เสร็จสิ้น</Text>
          </TouchableOpacity>
          {visible && modalBluetooth()}
          </ScrollView>
        </KeyboardAvoidingView>
      </Root>
    </DismissKeyboard>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#FFF',
    padding: 15,
    justifyContent: 'space-between',
  },
  containerWithOpa: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#FFF',
    padding: 15,
    justifyContent: 'space-between',
    opacity: .2,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 15,
    paddingTop: 15
  },
  bodyContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 15,
    marginBottom: 10
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  paymentButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: PRIMARY_GREEN,
    paddingVertical: 8,
    borderRadius: 5,
    marginVertical: 5
  },
  buttonText: {
    color: 'white',
    fontSize: 30,
    fontFamily: LQuarkFont
  },
  deviceList: {
    backgroundColor: SECONDARY_GREEN,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 5,
    marginVertical: 5
  },
  deviceText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: LQuarkFont
  },
  closeButton: {
    backgroundColor: NAVI,
    borderRadius: 5,
    paddingHorizontal: 20,
    paddingVertical: 5
  },
  modalLayout: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bluetoothList: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    borderRadius: 5,
    backgroundColor: 'white'
  },
  innerModal: {
    backgroundColor: '#E9E9E9',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    borderRadius: 5,
    justifyContent: 'space-between'
  },
  text:{ 
    fontSize: 18,
    fontFamily: BQuarkFont 
  },
  innderModalContent: {
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center', 
    marginHorizontal: 25
  }
})

export default PaymentPage;