import React from "react";
import { Text, StyleSheet, View } from "react-native";
import { LQuarkFont } from "../../../utils/Color";

const Anotation = () => {

    return(
        <View style={styles.container}>
            <Text style={styles.text}>หมายเหตุ: โปรดตรวจสอบรายการชำระของท่าน</Text>
            <Text style={styles.text}>หากไม่ถูกต้องกรุณาแจ้งกลับเจ้าหน้าที่ทันที</Text>
            <Text style={styles.text}>หากมีข้อสงสัยประการใด ติดต่อ 1361</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    text: {
        fontSize: 16,
        fontFamily: LQuarkFont
    }
})

export default Anotation;