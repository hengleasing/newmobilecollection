import React, {useEffect, useState} from "react";
import { View, Text, StyleSheet, TextInput } from "react-native";
import moment from "moment";
import { buddistEra } from "../../../utils/utils";
import { useSelector } from "react-redux";
import { LQuarkFont, BQuarkFont } from "../../../utils/Color";
import { Controller } from "react-hook-form";

const Body = ({
    setValue, errors,
    receiptNo,
    collector
}) => {

    const [amount, setAmount] = useState(0)
    const customerDetail = useSelector(({WorkListReducer}) => WorkListReducer)
    useEffect(() => {
        setValue("receiptAmount", customerDetail.customerPayment.amount.toString())
        setAmount(customerDetail.customerPayment.amount)
    }, [customerDetail.customerPayment.amount])
  

    const field = [
        {
            name: 'เลขที่ใบเสร็จรับเงิน',
            value: receiptNo?receiptNo:'แสดงเมื่อออกใบเสร็จแล้ว'
        },
        {
            name: 'ชื่อผู้กู้',
            value: customerDetail.customerDetail.name
        },
        {
            name: 'เลขที่สัญญา',
            value: customerDetail.customerDetail.contractNo
        },
        {
            name: 'วันที่ชำระเงิน',
            value: buddistEra(moment().format('YYYY-MM-DD hh:mm:ss'))
        },
        {
            name: 'เวลา',
            value: moment().format('hh:mm:ss น.')
        },
        {
           name: 'จำนวนที่ชำระ',
           type: 'input',
           value: amount
        },
        {
            name: 'ชำระโดย',
            value: 'เงินสด'
        },
        {
            name: 'ผู้รับเงิน',
            value: collector?collector:''
        }
    ]

    const handleInputChange = (values) => {
        setValue("receiptAmount",values)
        setAmount(values)
    }
    
    return(
        <View style={styles.container}>
            { field.map((name, index)=>
            <View key={index} style={styles.rowContain}>
                <View style={styles.titleContain}>
                    <Text style={styles.title}>{name.name}:</Text>
                </View>
                <View style={{justifyContent:'flex-end', flex:2}}>
                    {name.type? 
                    <View>

                    <TextInput style={styles.textInputStyle} value={name.value.toString()} keyboardType="numeric" onChangeText={text=>handleInputChange(text)} /> 
                    {errors.receiptAmount && <Text>This is required.</Text>}
                    </View>
                    :
                    <Text style={styles.textContent}>{name.value}</Text>
                    }
                </View>
            </View>
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignContent: 'center',
        paddingHorizontal: 15
    },
    rowContain: {
        flexDirection: 'row',
        paddingVertical: 5
    },
    titleContain: {
        flex: 2,
        flexDirection:'row',
        justifyContent:'flex-start',
    },
    title: {
        color: 'black', 
        fontSize: 18,
        fontFamily: BQuarkFont
    },
    textInputStyle: {
        borderColor: '#d7d7d9',
        borderWidth: 1,
        borderRadius: 5,
        width: 150,
        paddingTop: 0,
        paddingBottom: 0
    },
    textContent: {
        fontFamily: LQuarkFont,
        fontSize: 18,
    }

})

export default Body;