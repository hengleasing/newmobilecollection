import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { LQuarkFont } from "../../../utils/Color";

const Footer = () => {

    return(
        <View style={styles.container}>
            <Text style={styles.text}>บริษัท เฮงลิสซิ่ง</Text>
            <Text style={styles.text}>Heng Leasing Company Limited</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 15
    },
    text: {
        fontFamily: LQuarkFont,
        fontSize: 18
    }
})
export default Footer;