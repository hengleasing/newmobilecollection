import React, { useEffect, useState } from "react";
import { View, StyleSheet, Text, Dimensions } from "react-native";
import { BQuarkFont, SILVER, FORTH_GREEN, YELLOW, RED, NAVI } from "../../utils/Color";

const StickyBar = ({ data }) => {
    // data = []
    let arr = [1, 2, 3, 4]
    const [period, setPeriod] = useState([])

    useEffect(() => {
        initLoad()
    }, [])

    const initBadge = (outstandingPeriod) => {
        switch (parseInt(outstandingPeriod)) {
            case 0:
            case 1:
                return {
                    period: outstandingPeriod,
                    color: FORTH_GREEN
                }
            case 2:
                return {
                    period: outstandingPeriod,
                    color: YELLOW
                }
            case 3:
                return {
                    period: outstandingPeriod,
                    color: RED
                }
            default:
                if (outstandingPeriod > 3)
                    return {
                        period: outstandingPeriod,
                        color: NAVI
                    }
                break
        }
    }

    const initBadgeInverse = (targetOutstandingPeriod, outstandingPeriod) => {
        if (targetOutstandingPeriod === outstandingPeriod && outstandingPeriod === 0)
            return {
                period: outstandingPeriod,
                color: FORTH_GREEN
            }
        else if (targetOutstandingPeriod !== outstandingPeriod && outstandingPeriod === 0)
            return {
                period: outstandingPeriod,
                color: RED
            }
        else if (targetOutstandingPeriod > outstandingPeriod && outstandingPeriod > 0)
            return {
                period: outstandingPeriod,
                color: YELLOW
            }
        else if (targetOutstandingPeriod <= outstandingPeriod && outstandingPeriod > 0)
            return {
                period: outstandingPeriod,
                color: FORTH_GREEN
            }
        else
            return {
                period: outstandingPeriod,
                color: NAVI
            }
    }

    const getPeriod = (targetOutstandingPeriod, outstandingPeriod, reverse) => {
        if (reverse)
            return initBadgeInverse(targetOutstandingPeriod, outstandingPeriod)
        else
            return initBadge(outstandingPeriod)
    }

    const initLoad = () => {
        let result = arr.map((item, index) => {
            if (data.payables.length > 0)
                switch (index) {
                    case 0:
                        return { ...getPeriod(data.outstdPeriod, data.outstdPeriod, false), name: 'ค้าง' }

                    case 1:
                        return { ...getPeriod(data.outstdPeriod, data.paidBtwCollPeriod, true), name: 'ชำระมาแล้ว' }

                    case 2:
                        return { ...getPeriod(data.outstdPeriod, data.rmngPeriod, false), name: 'คงเหลือ' }

                    case 3:
                        return { ...getPeriod(data.outstdPeriod, data.currPeriod, false), name: 'คงเหลือรวมงวดล่าสุด' }

                }
            else
                switch (index) {
                    case 0:
                        return { ...getPeriod(data.outstdPeriod, data.outstdPeriod, false), name: 'ค้าง' }

                    case 1:
                        return { ...getPeriod(data.outstdPeriod, data.paidBtwCollPeriod, true), name: 'ชำระมาแล้ว' }

                    case 2:
                        return { ...getPeriod(data.outstdPeriod, data.rmngPeriod, false), name: 'คงเหลือ' }

                    case 3:
                        return { ...getPeriod(data.outstdPeriod, data.currPeriod, false), name: 'คงเหลือรวมงวดล่าสุด' }

                }
        })
        setPeriod(result)
    }

    return (
        <View style={styles.container}>
            {period.length > 0 && period.map((item, index) => (
                <View key={index} style={{ alignContent: 'center', alignItems: 'center', width: width / 4, flexWrap: 'nowrap' }}>
                    <Text style={{ textAlign: 'center', marginBottom: 5 }} ellipsizeMode="tail" numberOfLines={1} >{item.name}</Text>
                    <View style={[styles.circle, { backgroundColor: item.color }]}>
                        <Text style={styles.numText}>{item.period}</Text>
                    </View>
                </View>
            ))
            }
        </View>
    )
}
const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        borderBottomWidth: 1,
        borderBottomColor: SILVER,
        padding: 10
    },
    circle: {
        borderRadius: 30 / 2,
        width: 30,
        height: 30,
        color: 'red',
        backgroundColor: 'red',
        borderColor: 'white',
        borderWidth: 1,
        textAlign: 'center'
    },
    numText: {
        color: 'white',
        fontFamily: BQuarkFont,
        fontSize: 16,
        textAlign: 'center',
        lineHeight: 28
    }
})

export default StickyBar;