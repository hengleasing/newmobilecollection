import React, { useEffect, useMemo, useState } from "react";
import { View, Text, ScrollView, StyleSheet } from "react-native";
import useAction from "../../utils/useAction";
import { getPaymentHistory } from "../../state/action/WorkListAction";
import { useSelector } from "react-redux";
import { buddistEra, formatMoney } from "../../utils/utils";
import { LQuarkFont, SILVER, BQuarkFont, THIRD_GREEN, RED, FORTH_GREEN } from "../../utils/Color";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Spinner from "react-native-loading-spinner-overlay";
import LottieView from 'lottie-react-native';
import loadingAnimate from "../../../assets/icon/Loading.json";

const PaymentHistory = ({ route }) => {
    const fetchPaymentHistory = useAction(getPaymentHistory)
    const paymentHistorySource = useSelector(({ WorkListReducer: { customerPaymentHistory } }) => customerPaymentHistory)
    const [historylist, setHistorylist] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        initLoad()
     
    }, [])
    
    useMemo(() => setLoading(true), [paymentHistorySource])
    useEffect(() => {
        mapPaymentlist()
    }, [paymentHistorySource])

    const initLoad = () => {
        const collectionID = route.params?.collectionId
        fetchPaymentHistory(collectionID)
    }

    const mapPaymentlist = async () => {

        let item = []
        paymentHistorySource.id && paymentHistorySource.histories.forEach((element, index) => {
            let obj = {
                label: `งวดที่ ${element.period}「${buddistEra(element.dueDate)}」`,
                installment: element.installment,
                paidStatus: element.paidDate ? `ชำระแล้ว ${buddistEra(element.paidDate)}` : 'ยังไม่ได้ชำระ',
                status: element.paidDate,
                totalInstallment: element.collectInstallment,
                interestLate: element.fines,
                interestLatePaid: element.paidFines,
                interestOverdue: element.collectFines,
                discount: element.discount,
            }
            item.push(obj)
        })
        setHistorylist(item)
        await setLoading(false)
    }
    
    return (
        <ScrollView style={{ paddingHorizontal: 10 }}>
            {historylist.length == 0 && loading && 
            <View style={{ minHeight: 100}}>
                <LottieView source={loadingAnimate} autoPlay loop speed={2} />
            </View>
            }
            {
               ( historylist.length == 0 && !loading) &&  <View >
                   <Text style={[styles.textContent, {textAlign: 'center', paddingTop: 10}]} >ไม่มีประวัติการชำระ</Text>
                   </View>
            }
            {
                historylist.map((payment, index) => (
                    <View key={index} style={styles.container}>
                        <View style={styles.headerContent}>
                            <MaterialCommunityIcons name="tag-text-outline" size={25} />
                            <Text style={styles.textHeader}> {payment.label}</Text>
                        </View>
                        <View style={styles.jusBetweenRow}>
                            <Text style={[styles.textContent, { fontSize: 18 }]}>ค่างวด</Text>
                            <Text style={styles.textContent}>{formatMoney(payment.installment)} บาท</Text>
                        </View>
                        <View style={styles.jusBetweenRow}>
                            <Text style={[styles.textContent, { fontSize: 18 }]}>สถานะ</Text>
                            <Text
                                style={[
                                    styles.textContent,
                                    payment.status != undefined ? {
                                        backgroundColor: FORTH_GREEN,
                                        borderRadius: 5,
                                        paddingTop: 5,
                                        paddingHorizontal: 5,
                                        color: 'white'
                                    } : {
                                            backgroundColor: RED,
                                            borderRadius: 5,
                                            paddingTop: 5,
                                            paddingHorizontal: 5,
                                            color: 'white'
                                        }
                                ]}
                            ><MaterialCommunityIcons name={payment.status != undefined ? 'check-bold' : 'close'} /> {payment.paidStatus}</Text>
                        </View>
                        <View style={styles.jusBetweenRow}>
                            <Text style={[styles.textContent, { fontSize: 18 }]}>รวมชำระ</Text>
                            <Text style={styles.textContent}>{formatMoney(payment.totalInstallment)} บาท</Text>
                        </View>
                        <View style={styles.jusBetweenRow}>
                            <Text style={[styles.textContent, { fontSize: 18 }]}>ดอกเบี้ยล่าช้า</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                {payment.interestLate != '0' && <MaterialCommunityIcons name="menu-up" size={25} color={RED} />}
                                <Text
                                    style={
                                        [
                                            styles.textContent,
                                            payment.interestLate != '0' && { color: RED, fontFamily: BQuarkFont }
                                        ]
                                    }
                                >
                                    {formatMoney(payment.interestLate)} บาท
                                </Text>
                            </View>

                        </View>
                        <View style={styles.jusBetweenRow}>
                            <Text style={[styles.textContent, { fontSize: 18 }]}>ดอกเบี้ยล่าช้าชำระแล้ว</Text>
                            <Text style={styles.textContent}>{formatMoney(payment.interestLatePaid)} บาท</Text>
                        </View>
                        <View style={styles.jusBetweenRow}>
                            <Text style={[styles.textContent, { fontSize: 18 }]}>ดอกเบี้ยล่าช้าค้างชำระ</Text>
                            <Text style={styles.textContent}>{formatMoney(payment.interestOverdue)} บาท</Text>
                        </View>
                        <View style={styles.jusBetweenRow}>
                            <Text style={[styles.textContent, { fontSize: 18 }]}>ส่วนลดตรงกำหนด</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                {payment.discount != '0' && <MaterialCommunityIcons name="menu-down" size={25} color={THIRD_GREEN} />}

                                <Text style={[styles.textContent, payment.discount != '0' && { color: THIRD_GREEN, fontFamily: BQuarkFont }]}>{formatMoney(payment.discount)} บาท</Text>
                            </View>
                        </View>
                    </View>
                ))
            }
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        borderBottomColor: SILVER,
        borderBottomWidth: 1,
        marginVertical: 5
    },
    headerContent: {
        flexDirection: 'row',

    },
    jusBetweenRow: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textHeader: {
        fontFamily: BQuarkFont,
        fontSize: 20
    },
    textContent: {
        fontFamily: LQuarkFont,
        fontSize: 16
    }
})

export default PaymentHistory;