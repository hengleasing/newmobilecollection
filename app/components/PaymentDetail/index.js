import React, { useEffect, useState } from "react";
import { View, Text, ScrollView, StyleSheet, Dimensions, TouchableOpacity, ProgressBarAndroid } from "react-native";
import StickyBar from "./StickyBar";
import { useSelector, useDispatch } from "react-redux";
import { Card } from "react-native-shadow-cards";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { LQuarkFont, SILVER, BQuarkFont, SECONDARY_GREEN } from "../../utils/Color";
import { buddistEra, formatMoney } from "../../utils/utils";
import moment from "moment";
import DatePickerRow from "../common/DatePicker";
import { setDateVisible } from "../../state/reducer/GlobalReducer";
import PaymentList from "./PaymentList";
import useAction from "../../utils/useAction";
import { getCustomerPaymentByDate, totalPaidOnMobile } from "../../state/action/WorkListAction";



const PaymentDetail = (props) => {
    const data = useSelector(({ WorkListReducer: { customerPayment } }) => customerPayment)
    const eirData = useSelector(({PaymentReducer:{eirRemainingPrinciple}})=> eirRemainingPrinciple)
    const CusDetail = useSelector(({ WorkListReducer: { customerDetail } }) => customerDetail)
    const Visible = useSelector(({ GlobalReducer: { datePickerVisible } }) => datePickerVisible)
    const dateFromStore = useSelector(({GlobalReducer:{date}})=> date)

    const dispatch = useDispatch()
    const paymentByDate = useAction(getCustomerPaymentByDate)
    const totalMobilePaid = useAction(totalPaidOnMobile)

    const [basicDetail, setBasicDetail] = useState([])
    const [paymentBasic, setPaymentBasic] = useState([])
    const [paymentDetail, setPaymentDetail] = useState([])

    useEffect(() => {
        initLoad()
    }, [data,eirData])

    const initLoad = async () => {
        let cus, payment, paymentDeta = []
        let mobilePaided = 0
        
        if (CusDetail.id){

            cus = [
                {
                    title: CusDetail.name + " " + CusDetail.nickName,
                    contractNo: CusDetail.contractNo,
                    icon: 'account'
                },
                {
                    title: CusDetail.carRegisNo,
                    icon: 'account-card-details-outline'
                },
                {
                    title: CusDetail.carDescr,
                    icon: 'car'
                }
            ]
            mobilePaided = await totalMobilePaid(CusDetail.contractNo)
            
        }

        if (data.id && eirData.beforeEIRAgingRemainCost != undefined) {
            payment = [
                {
                    title: 'ยอดจัด',
                    value: formatMoney(data.price) + " บาท"
                },
                {
                    title: 'เงินต้นคงเหลือ (Flat rate)',
                    value: formatMoney(data.remaining) + " บาท"
                },
                {
                    title: 'เงินต้นคงเหลือ (EIR)',
                    value: formatMoney(eirData.beforeEIRAgingRemainCost) + " บาท"
                },
                {
                    title: 'เงินรับฝากคงเหลือ',
                    value: formatMoney(data.depositAmount) + " บาท"
                },
                {
                    title: 'ชำระทุกวันที่',
                    value: data.payInDay
                },
                {
                    title: 'วันที่ชำระล่าสุด',
                    value: buddistEra(data.lastPayDate)
                }
            ]

            paymentDeta = [
                {
                    title: 'สรุปยอดค้างชำระทั้งหมด',
                    name: 'totalAmount',
                    value: formatMoney(data.amount) + " บาท"
                },
                {
                    title: 'ค่างวดค้างชำระ',
                    name: 'overDueAmount',
                    value: formatMoney(data.overdueAmnt) + " บาท"
                },
                {
                    title: 'ค่าปรับ',
                    name: '',
                    value: formatMoney(data.overdueFines) + " บาท"
                },
                {
                    title: 'ค่าบริการจัดเก็บ',
                    value: formatMoney(data.trackFee) + " บาท"
                },
                {
                    title: 'ค่าบอกเลิกสัญญา',
                    value: formatMoney(data.terminateFee) + " บาท"
                },
                {
                    title: 'รับชำระผ่าน Mobile',
                    value: mobilePaided + " ครั้ง"
                },
                {
                    title: 'จำนวนงวดที่จ่ายมาแล้ว',
                    value: `${data.periodLatestPaid}/${data.periodQty}`
                }
            ]
        }
        setPaymentDetail(paymentDeta)
        setBasicDetail(cus)
        setPaymentBasic(payment)
    }

    const handleGetPaid = () => {
        props.navigation.navigate('printTempReceipt')
    }

    return (
        <View style={{ flex: 1 }}>
            {
                CusDetail.customerLoanTypeId == 0 &&
                <StickyBar data={data} />
            }
            <ScrollView>
                <View style={{ alignItems: 'center' }}>
                    <Card style={styles.container}>
                        {
                            basicDetail.map((detail, index) => (
                                <View key={index} style={[styles.spaceBetween, index == basicDetail.length - 1 && styles.borderBottom]}>
                                    <View style={styles.rowContainer}>
                                        <MaterialCommunityIcons name={detail.icon} size={25} />
                                        <Text style={styles.title}>{detail.title}</Text>
                                    </View>
                                    {
                                        detail.contractNo &&
                                        <View>
                                            <Text style={styles.title}>{detail.contractNo}</Text>
                                        </View>
                                    }
                                </View>
                            ))
                        }
                        {
                            CusDetail.customerLoanTypeId == 0 ? paymentBasic.map((payment, index) => (
                                <View key={index} style={styles.spaceBetween}>
                                    <Text style={styles.title} >{payment.title}</Text>
                                    <Text style={[styles.title, { fontSize: 14 }]}>{payment.value}</Text>
                                </View>
                            )) :
                                <View style={styles.spaceBetween}>
                                    <Text style={styles.title} >ยอดหนี้ติดตามต่อ</Text>
                                    <Text style={styles.title}>{formatMoney(data.remaining)}</Text>
                                </View>
                        }
                        <View style={styles.spaceBetween}>
                            <Text style={[styles.title, { fontFamily: BQuarkFont, fontSize: 18 }]} >ยอดค้างชำระ ณ วันที่</Text>
                            <TouchableOpacity onPress={() => dispatch(setDateVisible(true))}>
                                <Text style={[styles.title, { fontFamily: BQuarkFont, fontSize: 18 }]} >
                                    {buddistEra(moment(dateFromStore).format('YYYY-MM-DD'))}
                                    <MaterialCommunityIcons name="chevron-down" size={20} />
                                </Text>
                            </TouchableOpacity>
                        </View>
                        {
                           CusDetail.customerLoanTypeId == 0 && paymentDetail.map((pay, index) => (
                                <View key={index}>
                                    <View style={[styles.spaceBetween, index == 4 && { marginBottom: 10 }]}>
                                        <Text style={[styles.title, pay.name == 'totalAmount' && {fontFamily: BQuarkFont}]}>{pay.title}</Text>
                                        <Text style={[styles.title, { fontSize: 14 }, pay.name == 'totalAmount' && {fontFamily: BQuarkFont}]}>{pay.value}</Text>
                                    </View>
                                    {index == paymentDetail.length - 1 &&
                                        <View style={styles.spaceBetween}>
                                            <ProgressBarAndroid
                                                style={{ flex: 1 }}
                                                progress={(data.periodLatestPaid / data.periodQty)}
                                                indeterminate={false}
                                                styleAttr="Horizontal"
                                                color="#2196F3" />
                                        </View>
                                    }
                                </View>
                            ))
                        }
                    </Card>
                         <View style={[styles.container, {marginBottom: 5, }]}>
                            <TouchableOpacity style={styles.paymentButton} onPress={handleGetPaid} >
                                <Text style={{fontSize:35, color: 'white', fontFamily: LQuarkFont}}>รับชำระเงิน</Text>
                            </TouchableOpacity>
                        </View>
                    <Card style={styles.container}>
                        <PaymentList paymentSource={data.payables} collectionId={data.id} />
                    </Card>
                </View>

            </ScrollView>
            {
                Visible &&
                <View style={{ position: 'absolute', bottom: 0, flex: 1, backgroundColor: 'white' }}>
                    <DatePickerRow saveFunction={paymentByDate} id={data.id} type="0" />
                </View>
            }
        </View>
    )
}
const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        width: width * 0.98,
        paddingTop: 5
    },
    rowContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        maxWidth: '70%'
    },
    title: {
        fontFamily: LQuarkFont,
        fontSize: 16,
        marginLeft: 5,
        textAlignVertical: 'center',
        paddingTop: 5,
        flexShrink: 1
    },
    borderBottom: {
        borderBottomColor: SILVER,
        borderBottomWidth: 1,
        paddingBottom: 10
    },
    spaceBetween: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    paymentButton: {
        flexDirection:'row',
        justifyContent:'center',
        backgroundColor: SECONDARY_GREEN,
        paddingVertical:10 ,
        borderRadius:5
    }
})

export default PaymentDetail;