import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import { BQuarkFont, SECONDARY_GREEN, LQuarkFont, SILVER } from "../../utils/Color";
import { TouchableOpacity } from "react-native-gesture-handler";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { formatMoney } from "../../utils/utils";
import NavigationService from "../../services/NavigationService";

const PaymentList = ({ paymentSource, collectionId }) => {
    
    useEffect(() => {
        initLoad();
    }, [paymentSource])

    const [paymentData, setPaymentData] = useState([])

    const initLoad = () => {
        let item = []
        paymentSource && paymentSource.length > 0  && paymentSource.forEach(element => {
            let obj = {
                label: element.periodText,
                installmentPeriod: element.period,
                installmentOverdue: element.principleOverdue,
                interestOverdue: element.overdueFines,
                serviceFee: element.trackFee,
                contractTerminationFee: element.terminateFee,
                amount: element.amount
            }
            item.push(obj)
        });
        setPaymentData(item)
    }

    const handlePaymentHistory = () => {
        NavigationService.navigate("paymentHistory",{collectionId})
    }

    return (
        <View style={styles.container}>
            <View style={styles.title}>
                <View style={styles.titleContent}>
                    <MaterialCommunityIcons name="file-document-outline" size={25} color={SECONDARY_GREEN} />
                    <Text style={styles.HistoryPayment}> รายการที่ต้องชำระ</Text>
                </View>
                <TouchableOpacity style={styles.TransferButton} onPress={handlePaymentHistory}>
                    <Text style={{ color: SECONDARY_GREEN, fontFamily: LQuarkFont, fontSize: 18 }}>ดูประวัติการชำระ</Text>
                    <MaterialCommunityIcons name="chevron-right" size={25} color={SECONDARY_GREEN} />
                </TouchableOpacity>
            </View>
            {
                paymentData.length == 0 ?
                <View style={{alignItems:'center', marginBottom: 5}}>
                    <Text style={styles.TextContent}>ไม่มีรายการที่ต้องชำระ</Text>
                </View> :
                paymentData.map((payment, index) => (
                    <View key={index} style={styles.paymentContent}>
                        <View style={styles.Header}>
                            <MaterialCommunityIcons name="cash-100" size={25} />
                            <Text style={styles.TextHeader}> {payment.label}</Text>
                        </View>
                        <View style={styles.JusBetweenRow}>
                            <Text style={styles.TextContent}>ค่างวดที่ {payment.installmentPeriod}</Text>
                            <Text style={styles.TextContent}>{formatMoney(payment.installmentOverdue)} บาท</Text>
                        </View>
                        <View style={styles.JusBetweenRow}>
                            <Text style={styles.TextContent}>ค่าปรับ</Text>
                            <Text style={styles.TextContent}>{formatMoney(payment.interestOverdue)} บาท</Text>
                        </View>
                        <View style={styles.JusBetweenRow}>
                            <Text style={styles.TextContent}>ค่าบริการจัดเก็บ</Text>
                            <Text style={styles.TextContent}>{formatMoney(payment.serviceFee)} บาท</Text>
                        </View>
                        <View style={styles.JusBetweenRow}>
                            <Text style={styles.TextContent}>ค่าบอกเลิกสัญญา</Text>
                            <Text style={styles.TextContent}>{formatMoney(payment.contractTerminationFee)} บาท</Text>
                        </View>
                        <View style={styles.JusBetweenRow}>
                            <Text style={styles.TextHeader}>รวม</Text>
                            <Text style={styles.TextHeader}>{formatMoney(payment.amount)} บาท</Text>
                        </View>
                    </View>
                ))
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 10
    },
    HistoryPayment: {
        fontFamily: BQuarkFont,
        fontSize: 25,
        color: SECONDARY_GREEN
    },
    TransferButton: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    title: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    titleContent: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    Header: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    JusBetweenRow: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    TextHeader: {
        fontFamily: BQuarkFont,
        fontSize: 20
    },
    TextContent: {
        fontFamily: LQuarkFont,
        fontSize: 16
    },
    paymentContent: {
        borderBottomWidth: 1,
        marginBottom: 5,
        borderBottomColor: SILVER
    }

})

export default PaymentList;