import React, { useCallback, useEffect, useMemo, useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Modal, Picker } from "react-native";
import { BQuarkFont, SECONDARY_GREEN, LIGHT_SKY_BLUE, LQuarkFont, SILVER, NAVI } from "../../../utils/Color";
import useAction from "../../../utils/useAction";
import { receiptList, cancelReceipt } from "../../../state/action/PaymentAction";
import { useSelector } from "react-redux";
import accounting, { formatMoney } from "accounting";
import { isTablet } from "react-native-device-info"
import useScreenDimensions from "../../../utils/useScreenDimensions";
import RNPickerSelect from "react-native-picker-select";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import VegaScrollList from "../../common/VegaScroll/VegaScrollList";
import { useFocusEffect } from "@react-navigation/native";

const resons = [
    {
        label: 'ออกใบเสร็จให้แก่ลูกค้าผิดราย',
        value: '01'
    },
    {
        label: 'ออกใบเสร็จให้แก่ลูกค้าผิดสัญญา',
        value: '02'
    },
    {
        label: 'จำนวนเงินผิด',
        value: '03'
    },
    {
        label: 'ความไม่สมบูรณ์ใบเสร็จ ยับ พิมพ์ไม่ครบ',
        value: '04'
    },
    {
        label: 'พิมพ์ใบเสร็จไม่ออก',
        value: '05'
    },
    {
        label: 'ออกใบเสร็จซ้ำ',
        value: '06'
    }
]

const TempReceiptReport = () => {
    const fetchReceiptList = useAction(receiptList)
    const postCancelReceipt = useAction(cancelReceipt)
    const tempReceiptList = useSelector(({ PaymentReducer: { receiptList } }) => receiptList)
    const info = useSelector(({ PaymentReducer }) => PaymentReducer)
    const tablet = useMemo(() => isTablet(), [])
    const { width, height } = useScreenDimensions()

    const [limit, setLimit] = useState(17)
    const [start, setStart] = useState(1)
    const [visible, setVisible] = useState(false)
    const [information, setInfomation] = useState()
    const [cancelReson, setCancelReson] = useState("00")

    useFocusEffect(
        useCallback(()=>{
            if(tablet) {
                fetchReceiptList(0,17)
            }
            else {
                fetchReceiptList(0, 14)
                setLimit(14)
            }
        },[])
    )

    // useEffect(() => {
    //     if (tablet) {
    //         fetchReceiptList(0, 17)
    //     }
    //     else {
    //         fetchReceiptList(0, 14)
    //         setLimit(14)
    //     }
    // }, [])

    const handleChangePage = () => {
        let st = (limit*start) + 1
        
        fetchReceiptList(st, limit)
        setStart(start+1)
    }

    const handleCancleReceipt = receipt => {
        setVisible(!visible)
        setInfomation(receipt)
    }

    const handleChangePicker = value => {
        setCancelReson(value)
    }

    const handleSave = () => {
        postCancelReceipt(information.id,cancelReson, limit)
        setStart(1)
        setCancelReson("00")
        setVisible(!visible)
    }

    const confirmModal = () => {
        return (
            <Modal
                visible={visible}
                animationType="fade"
                transparent={true}
                onRequestClose={() => {}}
            >
                <View style={styles.layout} >
                    <View style={[styles.modalLayout,{ height: height * 0.3, width: width* 0.6, zIndex:11}]}>
                        <View style={styles.modalTitleLayout}>
                            <Text style={styles.modalTitle}>ต้องการยกเลิกใบเสร็จรับเงิน?</Text>
                        </View>
                        <View style={styles.modalContentLayout}>
                            <Text style={{fontFamily: BQuarkFont, fontSize: 16}}>รายละเอียด</Text>
                            <Text style={styles.detail}>เลขที่ใบเสร็จ: {information?.receiptNo}</Text>
                            <Text style={styles.detail}>เลขที่สัญญา: {information?.contractNo}</Text>
                            <Text style={styles.detail}>ชื่อ: {information?.customerName}</Text>
                        </View>
                        <View>
                            <Text style={{fontFamily: BQuarkFont, fontSize: 16}} >สาเหตุ</Text>
                            <RNPickerSelect
                                placeholder={{label:'เลือกสาเหตุการขอยกเลิก', value: '00', color: SILVER}}
                                style={styles}
                                items={resons}
                                useNativeAndroidPickerStyle={false}
                                onValueChange={handleChangePicker}
                                Icon={()=> <MaterialCommunityIcons name="chevron-down" size={25} />}
                            />
                        </View>
                        <View style={styles.buttonLayout}>
                            <TouchableOpacity style={[styles.closeButton, cancelReson == '00' && {backgroundColor: SILVER}]} onPress={handleSave} disabled={cancelReson == '00'}>
                                <Text style={[styles.detail, {color: 'white'}]} >บันทึก</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>setVisible(!visible)} style={styles.closeButton}>
                                <Text style={[styles.detail, {color: 'white'}]} >ยกเลิก</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    const Card = ({item}) => {
        let categoryColor = '#DCDCDC';
    
        switch (item.receiptStatus) {
            case '0':
                categoryColor = '#0087E7'
                break;
            case '1':
                categoryColor = '#D21F3C'
                break;
            case '2':
                categoryColor = '#98FB98'
                break;
            case '3':
                categoryColor = '#5A4B86'
                break;
            case '4':
                categoryColor = '#AE4DB9'
                break;
            case '5':
                categoryColor = '#D2227E'
                break;
            default:
                categoryColor = '#DCDCDC'
                break;
        }
        
        return (
          <TouchableOpacity style={[{flex: 1, flexDirection: 'row'}, item.receiptStatus != 1 &&{ backgroundColor:'#F7F7F7' }]} onPress={() => handleCancleReceipt(item)} disabled={item.receiptStatus != 1} >
          
            <View
              style={styles.startCard}>
                <Text style={styles.cardText}>
                เลขที่ใบเสร็จ {item.receiptNo}
              </Text>
              <Text
                style={[styles.cardText,{
                  // flex: 1,
                  fontSize: 22,
                  fontFamily: BQuarkFont,
                  marginBottom: 4,
                  color: '#808080',
                }]}>
                {item.contractNo}
              </Text>
              <Text style={styles.cardText}>
                {item.customerName}
              </Text>
            </View>
            <View
              style={styles.endCard}>
              <Text
                style={[styles.cardText, {fontSize: 18, fontFamily: BQuarkFont}]}>
                {formatMoney(item.amount," ")} บาท
              </Text>
              <Text style={[styles.cardText, {fontSize: 16, fontFamily: BQuarkFont, color: categoryColor}]}>
                    {item.receiptStatus == 0 && 'ยกเลิกสำเร็จ'}
                    {item.receiptStatus == 1 && 'ขอยกเลิก'}
                    {item.receiptStatus == 2 && 'ออกใบเสร็จรับเงินแล้ว'}
                    {item.receiptStatus == 3 && 'รอจัดเก็บอนุมัติ'}
                    {item.receiptStatus == 4 && 'รอการเงินHQอนุมัติ'}
                    {item.receiptStatus == 5 && 'รอผู้จัดการสาขาอนุมัติ'}
              </Text>
            </View>
          </TouchableOpacity>
        );
      };

    return (
        <View style={styles.container}>
            <View elevation={5} style={styles.summarybox}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text style={[styles.detail, {fontSize: 20}]}>ยอดชำระทั้งหมด</Text>
                    <Text style={styles.detail}> {formatMoney(info.total, " ")} บาท</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text style={[styles.detail, {fontSize: 20}]}>รอยกเลิก</Text>
                    <Text style={styles.detail}> {formatMoney(info.waitCancel, " ")} บาท</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text style={[styles.detail, {fontSize: 20}]}>รวม</Text>
                    <Text style={styles.detail}> {formatMoney(info.summary," ")} บาท</Text>
                </View>
            </View>
            <VegaScrollList
                    style={{marginTop: 8}}
                    distanceBetweenItem={0}
                    data={tempReceiptList}
                    keyExtractor={(item, index) => index.toString()}
                    onEndReached={handleChangePage}
                    
                    renderItem={data => {
                    return (
                        <View
                            elevation={5}
                            style={[styles.cardView, {width: width - 30}]}>
                            <Card item={data.item} />
                        </View>
                    );
                    }}
                />
            {confirmModal()}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
    },
    headerLayout: {
        flexDirection: 'row',
        maxHeight: 30,
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 5,
    },
    header: {
        alignItems: 'center',
        fontSize: 16,
        fontFamily: BQuarkFont
    },
    center: {
        flexDirection: 'column',
        alignItems: 'center',
        fontFamily: LQuarkFont,
        fontSize: 16
    },
    button: {
        borderRadius: 5,
        backgroundColor: '#73C2FB',
        paddingVertical: 5,
        paddingHorizontal: 5,
        alignItems: 'center'
    },
    circle: {
        height: 30,
        width: 30,
        borderRadius: 30 / 2,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderColor: 'white',
        marginHorizontal: 5
    },
    closeButton: {
        backgroundColor: '#51aee8',
        borderRadius: 5,
        paddingHorizontal: 20,
        paddingVertical: 5
    },
    recordLayout: {
        flexDirection: 'row',
        alignItems: 'center',
        maxHeight: 40,
        justifyContent: 'space-between',
        flex: 5
    },
    pagenationsLayout: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    layout: {
        flex: 1,
        justifyContent:'center',
        alignItems: 'center',
        backgroundColor: 'rgba(234, 244, 246, 0.71)'
    },
    modalLayout: {
        backgroundColor : LIGHT_SKY_BLUE,
        padding: 10,
        borderRadius: 5,
        justifyContent: 'space-between'
    },
    modalTitleLayout: {
        padding: 10
    },
    modalTitle: {
        fontFamily: BQuarkFont,
        fontSize: 18,
        textAlign: 'center'
    },
    buttonLayout: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 5
    },
    detail: {
        fontFamily: LQuarkFont,
        fontSize: 16
    },
    inputAndroid: {
        fontSize: 18,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: NAVI,
        borderRadius: 8,
        paddingRight: 30,
        fontFamily: LQuarkFont,
    },
    iconContainer: {
        top: 10,
        right: 5,
    },
    placeholder:{
        fontFamily: LQuarkFont, 
        fontSize: 18
    },
    cardView: {
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowRadius: 3,
        shadowOpacity: 0.5,
        height:100,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        borderRadius: 6,
    },
    startCard: {
        flex: 1,
        flexDirection: 'column',
        height: 50,
        width: '90%',
        marginHorizontal: 10,
        marginVertical: 8,
    },
    cardText: {
        alignItems: 'flex-end', 
        color: '#808080',
        fontFamily: LQuarkFont,
        fontSize:16
    },
    endCard: {
        flex: 1,
        flexDirection: 'column',
        width: '90%',
        marginHorizontal: 20,
        marginVertical: 10,
        alignItems: 'flex-end',
        justifyContent:'space-between'
    },
    summarybox:{
        borderRadius: 5,
        backgroundColor: 'white',
        marginHorizontal: 5,
        padding:5,
        paddingHorizontal: 10
    }

})

export default TempReceiptReport;