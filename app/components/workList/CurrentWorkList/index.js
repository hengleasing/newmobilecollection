import React, { useEffect, useCallback, useState } from "react";
import { View, StyleSheet, ScrollView, Linking, Text } from "react-native";
import { useFocusEffect } from '@react-navigation/native';
import CardWorkList from "../../common/CardWorkList";
import { SILVER, BQuarkFont, LIGHT_AZURE } from "../../../utils/Color";
import { GetCurrentWorkList, SetLimit, GetAssigned, getCustomerDetail, finishWork } from "../../../state/action/WorkListAction";
import useAction from "../../../utils/useAction";
import { useSelector, useDispatch } from "react-redux";
import Spinner from "react-native-loading-spinner-overlay";
import { isCloseToBottom, getRefreshToken } from "../../../utils/utils";
import FabButton from "../../common/FabButton";
import { clearCusDetail, setCurPage, clearCurrentWorkList, setCurLoading } from "../../../state/reducer/WorkListReducer";
import { resetForm, setPrevPage } from "../../../state/reducer/CollectionFilter";
import { Root, Popup } from "../../common/CustomModal";
import LottieView from 'lottie-react-native';
import loadingAnimate from "../../../../assets/icon/Loading.json";

const CurrentWorkList = (props) => {

    const fetchWorkList = useAction(GetCurrentWorkList)
    const updateWorklist = useAction(finishWork)
    const SetPage = useAction(SetLimit)
    //const fetchAssigned = useAction(GetAssigned)
    const getDetail = useAction(getCustomerDetail)
    const CurrentList = useSelector(({ WorkListReducer: { currentList } }) => currentList)
    const Page = useSelector(({ WorkListReducer: { curPage } }) => curPage)
    const loading = useSelector(({ WorkListReducer: { curLoading } }) => curLoading)
    const dispatch = useDispatch();
   
    useFocusEffect(

        useCallback(() => {
            init()

            return () => {
                dispatch(setCurPage(0))
                dispatch(clearCurrentWorkList())
                dispatch(setCurLoading(true))
            };
        }, [])
    );

    const init = async () => {
        await fetchWorkList(0, 10)
        dispatch(setPrevPage("WorkList"));
    }

    const handlePressFab = () => {
        props.navigation.navigate("AssignedWorklist")
    }

    const handleAction = async (id, buttonId, contractNo) => {

        let res = await getDetail(id, contractNo)
        if (res?.status)
            switch (buttonId) {
                case 0:
                    props.navigation.navigate('MainDetail', { screen: 'CustomerAppointment' })
                    break;
                case 1:
                    Popup.show({
                        type: 'Confirm',
                        title: 'ยืนยันเสร็จงาน',
                        callback: (e) => {
                            if (e) {
                                updateWorklist(res?.cusDetail.id)
                            }
                            Popup.hide()
                        }
                    })
                    break;
                case 2:
                    props.navigation.navigate('MainDetail', { screen: 'CustomerPayment' })
                    break;
                case 3:
                    props.navigation.navigate('MainDetail', { screen: 'CustomerAddress' })
                    break;
                case 4:
                    Linking.openURL('tel:' + res?.cusDetail.tel).then(res => dispatch(clearCusDetail()))
                    break;
            }
    }

    return (
        <View style={styles.container}>
            {CurrentList.length == 0 && loading && <LottieView source={loadingAnimate} autoPlay loop speed={2} />}
            {
                CurrentList.length == 0 && !loading && <Text style={{ fontFamily: BQuarkFont, fontSize: 20, color: LIGHT_AZURE }}>ไม่มีข้อมูล</Text>
            }
            <ScrollView onScroll={({ nativeEvent }) => isCloseToBottom(nativeEvent, fetchWorkList, Page, SetPage, 'cur')} >
                {
                    CurrentList.length > 0 && CurrentList.map((item, index) => (
                        <CardWorkList key={index} data={item} action={handleAction} />
                    ))
                }
            </ScrollView>
            <FabButton icon="library-plus" action={handlePressFab} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 5,
        backgroundColor: SILVER
    }
})

export default CurrentWorkList