import React,{ useCallback, useEffect, useState } from "react";
import { View, StyleSheet, ScrollView, Text, Linking } from "react-native";
import { GetCompleteWorkList, getCustomerDetail, SetLimit } from "../../../state/action/WorkListAction";
import { clearCompleteList, clearCusDetail, setComLoading } from "../../../state/reducer/WorkListReducer";
import useAction from "../../../utils/useAction";
import { useSelector, useDispatch } from "react-redux";
import { SILVER, BQuarkFont, LIGHT_AZURE } from "../../../utils/Color";
import CardWorkList from "../../common/CardWorkList";
import { isCloseToBottom } from "../../../utils/utils";
import Spinner from "react-native-loading-spinner-overlay";
import LottieView from 'lottie-react-native';
import loadingAnimate from "../../../../assets/icon/Loading.json";
import { useFocusEffect } from "@react-navigation/native";

const FinishedWorkList = (props) => {

    const dispatch = useDispatch();
    
    useFocusEffect(
        useCallback(()=>{
            init()
            return () =>{ 
                dispatch(clearCompleteList())
                dispatch(setComLoading(true))
            };
        },[])
    )

    const Page = useSelector(({WorkListReducer:{completePage}})=>completePage)
    const loading = useSelector(({WorkListReducer:{comLoading}})=>comLoading)
    const completeList = useSelector(({WorkListReducer:{completeList}})=>completeList)
    const fetchComplete = useAction(GetCompleteWorkList)
    const SetPage = useAction(SetLimit)
    const getDetail = useAction(getCustomerDetail)

    const init = async () => {
        await fetchComplete(0,10)
    }

    const handleAction = async(id, buttonID, contractNo) => {
        let res = await getDetail(id, contractNo)
        if (res?.status)
            switch (buttonID) {
                case 0:
                    props.navigation.navigate('MainDetail', { screen: 'CustomerAppointment' })
                    break;
                case 2:
                    props.navigation.navigate('MainDetail', { screen: 'CustomerPayment' })
                    break;
                case 3:
                    props.navigation.navigate('MainDetail', { screen: 'CustomerAddress' })
                    break;
                case 4:
                    Linking.openURL('tel:' + res?.cusDetail.tel).then(res => dispatch(clearCusDetail()))
                    break;
            }
    }

    return(
        <View style={styles.container}>
            {completeList.length == 0 && loading && <LottieView source={loadingAnimate} autoPlay loop speed={2} />}
            {
               ( completeList.length == 0 && !loading) && <Text style={{fontFamily: BQuarkFont, fontSize: 20, color: LIGHT_AZURE}}>ไม่มีข้อมูล</Text>
            }
            <ScrollView onScroll={({nativeEvent})=>isCloseToBottom(nativeEvent, fetchComplete, Page, SetPage, 'complete')} >
                {completeList.length > 0 && completeList.map((item, index)=>(
                    <CardWorkList key={index} data={item} action={handleAction} />
                ))}
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center',
        padding: 5,
        backgroundColor: SILVER
    }
})

export default FinishedWorkList;