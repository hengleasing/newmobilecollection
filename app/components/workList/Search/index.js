import React from "react";
import { View, Text, StyleSheet } from "react-native";
import CollectionControlTxtInput from "../../CollectionFilter/CollectionControlTxtInput";
import { BQuarkFont } from "../../../utils/Color";

const Search = () => {
    return(
        <View style={styles.container}>
            <Text style={styles.headerText}>เลขที่สัญญา</Text>
            <View style={styles.selectStyle}>
                <CollectionControlTxtInput name="contractNo" placeholder="เลขที่สัญญา" />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 10
    },
    headerText: {
        fontFamily: BQuarkFont,
        fontSize: 25,
    },
    selectStyle: {
        borderRadius: 6,
        borderColor: '#328956',
        borderWidth: 1,
    }
})

export default Search