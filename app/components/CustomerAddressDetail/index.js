import React, { useState, useEffect, useRef, useCallback, Fragment } from "react";
import { View, Text, StyleSheet, Linking, TouchableOpacity } from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import Geolocation from '@react-native-community/geolocation';
import { useSelector } from "react-redux";
import { Root, Popup, Toast } from "../common/CustomModal";
import FabButton from "../common/FabButton";
// import Menu, { MenuItem } from "react-native-material-menu";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { SECONDARY_GREEN, LQuarkFont } from "../../utils/Color";
import { FloatingAction } from "react-native-floating-action";
import { useFocusEffect } from "@react-navigation/native";

const initialValue = {
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.015,
    longitudeDelta: 0.0121,
}

const DEFAULT_PADDING = { top: 40, right: 40, bottom: 40, left: 40 };

const CustomerAddressDetail = () => {
    const [coords, setCoords] = useState(initialValue)
    const [markers, setMarkers] = useState([])
    const [menu, setMenu] = useState([])
    const [_mount, setMount] = useState(true)
    const _ref = useRef()
    const addressMark = useSelector(({ WorkListReducer }) => WorkListReducer)

    useFocusEffect(
         useCallback(() => {
            
                mapReady()
                initLocation()
                return () => {
                    setMount(false)
                }
            },[coords],
            )
        
    )
    useEffect(() => {
        
    }, [])

    // useEffect(() => {
    //     mapReady()
    // }, [markers])

    useEffect(() => {
        addFloatingAction()
    }, [markers])

    const addFloatingAction = () => {
        let action = []
        markers.map((btn, index) => {
            action.push({
                text: btn.title,
                icon: <MaterialCommunityIcons name="account-circle" size={20} color="white" />,
                position: index+1,
                coords: btn.coordinate,
                name: index.toString(),
                color: SECONDARY_GREEN,
                textStyle: {
                    fontFamily: LQuarkFont,
                    fontSize: 16
                },
            })
        })
        setMenu(action)
    }

    const initLocation = async () => {
        Popup.show({
            type: 'Success',
            title: 'กำลังดึงพิกัดแผนที่',
            textBody: 'กรุณารอสักครู่ ...',
            button: false,
            timing: 5000,
            autoClose: true,
            callback: ()=> {
                Toast.hide()
            }
        })
        await Geolocation.getCurrentPosition(info => {
            if(_mount){

                setCoords(
                    {
                        ...coords,
                        latitude: info.coords.latitude,
                        longitude: info.coords.longitude
                    }
                )

            }
            
        },error=>{
            Toast.show({
                title: 'ดึงข้อมูลพิกัดผิดพลาด',
                text: 'กรุณารอสักครู่ ...',
                color: 'red',
                icon: <MaterialCommunityIcons name="compass" size={25} color="white" />,
                timing: 5000
        })}, {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000});

        let markerArray = []

        addressMark.customerDetail.addresses.map(ad => {
            if (ad.location.lat && ad.location.lon)
                markerArray.push({
                    title: ad.name + ' (ลูกค้า)',
                    coordinate: {
                        latitude: ad.location.lat,
                        longitude: ad.location.lon
                    },
                    description: ad.location.address
                })
        })
        addressMark.customerBoundsManDetail.map(bounds => (
            bounds.addresses.map(boundsAdress => {
                if (boundsAdress.location.lat && boundsAdress.location.lon)
                    markerArray.push({
                        title: boundsAdress.name + ' ' + bounds.relation,
                        coordinate: {
                            latitude: boundsAdress.location.lat,
                            longitude: boundsAdress.location.lon
                        },
                        description: boundsAdress.location.address
                    })
            })
        ))
        setMarkers(markerArray)
    }

    const mapReady = async (e,i) => {
        let coordsFit = await markers.map(mark => mark.coordinate)
        coordsFit.push({ latitude: coords.latitude, longitude: coords.longitude })
        await _ref.current.fitToCoordinates(coordsFit, {
            edgePadding: DEFAULT_PADDING,
            animated: true,
        })

    }

    const handlMarkerPress = (coords) => {
        const googleMapUrl = "https://www.google.com/maps/dir/?api=1&destination=" + coords.latitude + "," + coords.longitude + "&travelmode=driving"
        Linking.canOpenURL(googleMapUrl).then(supported => {
            if (!supported){
                
            }
            else
                return Linking.openURL(googleMapUrl)
        })
    }

    const handleDirectButton = value => {
        let directSelect = menu[value]
        handlMarkerPress(directSelect.coords)
    }

    return (
        <Fragment> 
            <MapView
                ref={_ref}
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={styles.map}
                // region={coords}
                onMapReady={(e,i)=>mapReady(e,i)}
            >
                <MapView.Marker coordinate={coords}>
                    <View style={styles.radius}>
                        <View style={styles.marker} />
                    </View>
                </MapView.Marker>
                {
                    markers
                        .map((locate, index) => {
                            return <Marker
                                key={index}
                                coordinate={locate.coordinate}
                                pinColor="red"
                                title={locate.title}
                                description={locate.description}
                                onCalloutPress={() => handlMarkerPress(locate.coordinate)}
                            />
                        })
                }
            </MapView>
            
                <FloatingAction 
                    actions={menu}
                    onPressItem={handleDirectButton}
                    position="right"
                    floatingIcon={<MaterialCommunityIcons name="directions" size={30} color="white" />}
                    color={SECONDARY_GREEN}
                    animated={false}
                    actionsPaddingTopBottom ={0}
                />
        </Fragment>
    )
}

const styles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    radius: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        backgroundColor: 'rgba(0,122,255,0.1)',
        borderColor: 'rgba(0,122,255,0.3)',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    marker: {
        height: 20,
        width: 20,
        borderWidth: 3,
        borderColor: 'white',
        borderRadius: 20 / 2,
        overflow: 'hidden',
        backgroundColor: '#007AFF',
        alignItems: 'center',
    },
    radiusButton: {
        backgroundColor: 'white',
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        borderColor: 'grey',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonContainer: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        margin: 20,
    },
    buttontab: {
        margin: 50,
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 300
    },
    input: {
        height: 100,
        width: 150,
        backgroundColor: '#a5dc',
        marginBottom: 20,
        paddingHorizontal: 10,
    },
    FabButton: {
        position: 'absolute',
        height: 60,
        width: 60,
        bottom: 30,
        right: 30,
    },
    Button: {
        backgroundColor: SECONDARY_GREEN,
        borderRadius: 60 / 2,
        width: 60,
        height: 60,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        elevation: 5,
    }
})


export default CustomerAddressDetail;