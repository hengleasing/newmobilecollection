import React, { useState, useCallback, useEffect } from 'react'
import { View, FlatList, TouchableOpacity, Text, StyleSheet, Modal } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { ListItem } from 'react-native-elements'
import { useSelector, useDispatch } from 'react-redux'
import Tag from '../common/Tag'
import TagColor from '../../utils/TagColor'
import { NAVI, LQuarkFont, BQuarkFont, LIGHT_GREEN, LIGHT_AZURE } from '../../utils/Color'
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import FontistoIcons from "react-native-vector-icons/Fontisto";
import useAction from "../../utils/useAction";
import { updateAssignedList } from '../../state/action/WorkListAction'
import NavigationService from '../../services/NavigationService'
import { clearCurrentWorkList, clearAssignedList, setAssignLoading } from '../../state/reducer/WorkListReducer'
import useAssignedLoadmore from "../../utils/useAssignedLoadmore";
import { resetForm, setPrevPage } from "../../state/reducer/CollectionFilter";
import LottieView from "lottie-react-native";
import animateLoading from "../../../assets/icon/Loading.json";
import { Popup } from '../common/CustomModal';
import accounting from "accounting";
import moment from 'moment';
import { buddistEra } from '../../utils/utils';

const AssinedWorkList = (props) => {


    const dispatch = useDispatch()
    const [selectedList, setSelectedList] = useState(new Map())
    const [isSelect, setisSelect] = useState(false)
    const save = useAction(updateAssignedList)
    const assignedList = useSelector(({ WorkListReducer: { assignedList } }) => assignedList);
    const loading = useSelector(({ WorkListReducer: { assignLoading } }) => assignLoading)
    const filter = useSelector(({CollectionFilter})=>CollectionFilter.filter);
    const addListLoading = useSelector(({WorkListReducer: {addAssignLoading}}) => addAssignLoading)
    const loadmore = useAssignedLoadmore();


    useFocusEffect(

        useCallback(() => {
            loadmore.fetch(filter);
            return () => {
                loadmore.clear();
                dispatch(setAssignLoading(true))
            };
        }, [filter])
    );

    useEffect(() => {
        init()
    }, [isSelect, selectedList])

    const init = () => {
        props.navigation.setOptions({
            headerRight: () => {
                if (isSelect && selectedList.size != 0)
                    return (
                        <View style={styles.topRightBotton}>
                            <TouchableOpacity onPress={() => handleSave(selectedList)} >
                                <MaterialCommunityIcons name="content-save-outline" size={30} color={LIGHT_AZURE} />
                            </TouchableOpacity>
                        </View>
                    )
                else
                    return (
                        <View style={styles.topRightBotton}>
                            {/* <TouchableOpacity  >
                                <FontistoIcons name="search" size={30} color={LIGHT_AZURE} />
                            </TouchableOpacity> */}
                            <TouchableOpacity onPress={handleFilter} >
                                <MaterialCommunityIcons name="filter-outline" size={30} color={LIGHT_AZURE} />
                            </TouchableOpacity>
                        </View>
                    )

            },
        })
    }

    const handleFilter = () => {

        dispatch(setPrevPage("AssignedWorklist"));
        props.navigation.navigate("CollectionFilter");
    }

    const handleSave = () => {
        Popup.show({
            type: 'Confirm',
            title: 'เลือกรายชื่อออกติดตาม',
            textBody: 'คุณต้องการบันทึกรายชื่อออกติดตามหรือไม่',
            callback: async (e) => {
                if (e) {

                    const res = await save(Array.from(selectedList.keys()));

                    dispatch(clearAssignedList())
                    props.navigation.reset({
                        index: 0,
                        routes: [{ name: 'WorkList' }]
                    })

                }
                Popup.hide()
            }
        })

    }

    const handleSelect = props => {
        if (isSelect) {
            const newSelect = new Map(selectedList)
            newSelect.set(props.item.contractNo, !selectedList.get(props.item.contractNo))
            setSelectedList(newSelect)
          
            if(newSelect.size == 0){
                setisSelect(false)
                setSelectedList(new Map())
            }
        }
    }

    const handleLongPress = (props) => {
        if (isSelect) {
            setisSelect(false)
            setSelectedList(new Map())
        }
        else {
            setisSelect(true)
            const newSelect = new Map(selectedList)
            newSelect.set(props.item.contractNo, !selectedList.get(props.item.contractNo))
            setSelectedList(newSelect)
            dispatch(clearCurrentWorkList())
        }
    }

    const touch = (props) => {

        const tags = [
            {
                label: 'ค้าง',
                name: 'overdue',
                value: props.item.outstdPeriod.toString(),
                color: TagColor(props.item.outstdPeriod, props.item.outstdPeriod, false)
            },
            {
                label: 'ชำระ',
                name: 'mustPay',
                value: props.item.paidBtwCollPeriod.toString(),
                color: TagColor(props.item.outstdPeriod, props.item.paidBtwCollPeriod, true)
            },
            {
                label: 'เหลือ',
                name: 'remain',
                value: props.item.rmngPeriod.toString(),
                color: TagColor(props.item.outstdPeriod, props.item.rmngPeriod, false)
            },
            {
                label: 'เหลือ + ล่าสุด',
                name: 'remainLast',
                value: props.item.currPeriod.toString(),
                color: TagColor(props.item.outstdPeriod, props.item.currPeriod, false)
            },
            {
                label: props.item.statusAgingEIR,
                name: 'statusAgingEIR',
                value: props.item.currPeriod.toString(),
                color: NAVI
            },
            {
                label: null,
                name: 'appointmentDate',
                value: props.item.appointmentDate && buddistEra(props.item.appointmentDate),
                color: TagColor(null, null, null, props.item.appointmentDate)
            }
        ]
        const icons = [
            {
                name: 'cash-100',
                text: 'เงินต้นคงเหลือ: ' + accounting.formatNumber(props.item.remaining, 2, ",")
            },
            {
                name: 'calendar-multiselect',
                text: 'วันที่เริ่มผิดนัดชำระ: ' + buddistEra(moment(props.overdueDate).format("YYYY-MM-DD"))
            }
        ]

        return (
            <View>
                <View style={[styles.item]}>
                    {tags.map((tag, index) => {
                        if (tag.value)
                            return (
                                <Tag
                                    key={index}
                                    label={tag.label}
                                    name={tag.name}
                                    value={tag.value}
                                    color={tag.color}
                                />
                            )
                    })}
                </View>
                <View>
                    {
                        icons.map((icon, index) => (
                            <Text style={styles.description} key={index}>
                                <MaterialCommunityIcons name={icon.name} /> {icon.text}
                            </Text>
                        ))
                    }
                </View>
            </View>
        )
    }

    const renderItem = (props, selected) => (
        <ListItem
            onLongPress={() => handleLongPress(props)}
            onPress={() => handleSelect(props)}
            leftAvatar={{ source: require("../../../assets/images/defaul-avatar_0.jpg") }}
            bottomDivider
            title={props.item.name}
            subtitle={touch(props)}
            titleStyle={{ fontFamily: BQuarkFont, fontSize: 20, }}
            containerStyle={{ backgroundColor: selected ? LIGHT_GREEN : 'white', }}
            checkmark={selected}
        />
    )

    const keyExtractor = (item, index) => index.toString()

    if (assignedList.length < 1 && loading) {
        return (
            <LottieView source={animateLoading} autoPlay loop />
        )
    }

    if (assignedList.length < 1 && !loading)
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={{ fontFamily: BQuarkFont, fontSize: 20 }}>ไม่มีข้อมูล</Text>
            </View>
        )

    return (
        <View>
            <FlatList
                data={assignedList}
                keyExtractor={keyExtractor}
                renderItem={(e) => renderItem(e, !!selectedList.get(e.item.contractNo))}
                extraData={selectedList}
                onEndReached={(props) => {
                    loadmore.fetch();
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    description: {
        fontFamily: LQuarkFont
    },
    topRightBotton: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        minWidth: 120
    }
})

export default AssinedWorkList;