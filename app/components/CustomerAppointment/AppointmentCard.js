import React from "react";
import { View, Text, Dimensions, StyleSheet } from "react-native";
import CardHeader from "./CardHeader";
import CardBody from "./CardBody";
import CardFooter from "./CardFooter";
import { Card } from "react-native-shadow-cards";
import { buddistEra } from "../../utils/utils";

const AppointmentCard = ({datasource}) => {
    const header = {
        id: datasource.id,
        name: datasource.employeeName,
        branch: datasource.employeeBranch,
        createDate: datasource.createdDate,
        latitude: datasource.lat,
        longitude: datasource.lon,
        contractNo: datasource.contractNo,
    }
    const body = {
        detail: datasource.detl
    }
    const footer = {
        gps: datasource.lat == 0 && datasource.lon == 0 ?"ไม่ระบุ":(datasource.lat + ", " +datasource.lon),
        appointmentTime: "นัดหมายครั้งต่อไป: " + buddistEra(datasource.appointmentDate) + " เวลา: " + datasource.appointmentTime + " น."
    }
    return(
        <Card style={styles.container}>
            <CardHeader data={header} />
            <CardBody data={body} />
            <CardFooter data={footer} />
        </Card>
    )
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        width: width * 0.98,
        // flexDirection: 'row',
        marginBottom:5
    },
})


export default AppointmentCard