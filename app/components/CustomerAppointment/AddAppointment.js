import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, ScrollView, PermissionsAndroid, BackHandler, Dimensions, Modal, TouchableOpacity, KeyboardAvoidingView } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { LQuarkFont, BQuarkFont, SILVER, SECONDARY_GREEN } from "../../utils/Color";
import SegmentedControls from "../common/SegmentButton";
import ImageButton from "../common/ImageButton";
import ImagePicker from "react-native-image-picker";
import DeviceInfo from 'react-native-device-info';
import DatePickerRow from "../common/DatePicker";
import { TextInput } from "react-native-gesture-handler";
import useAction from "../../utils/useAction";
import { setDateVisible, setLoading } from "../../state/reducer/GlobalReducer";
import { DateGlobalAction } from "../../state/action/GlobalAction";
import { buddistEra } from "../../utils/utils";
import { preSaveData, setflag } from "../../state/action/AppointmentAction";
import { Root } from '../common/CustomModal'
import { destroyFormValue } from "../../state/reducer/AppointmentReducer";
import DatePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import * as Progress from 'react-native-progress';

const AddAppointment = (props) => {


    const preSave = useAction(preSaveData)
    const showDate = useAction(setDateVisible)
    const loadingGlobal = useAction(setLoading)
    const dateGlobal = useAction(DateGlobalAction)
    const Flag = useAction(setflag)
    const [type, setType] = useState(0)
    const [calendarVisible, setCalendarVisible] = useState(false)
    const [contractType, setContractType] = useState(5)
    const [fullHeight, setFullHeight] = useState(true)
    const dispatch = useDispatch()


    const Segmented = useSelector(({ AppointmentReducer: { segmented } }) => segmented)
    const progress = useSelector(({ AppointmentReducer: { progress } }) => progress)
    const CriteriaRelation = useSelector(({ AppointmentReducer: { criteriaRelation } }) => criteriaRelation)
    const Visible = useSelector(({ GlobalReducer: { datePickerVisible } }) => datePickerVisible)
    const loading = useSelector(({ GlobalReducer: { loading } }) => loading)
    const preSaveValue = useSelector(({ AppointmentReducer: { postData } }) => postData)

    let isTablet = DeviceInfo.isTablet()

    useEffect(() => {
        if (Segmented != undefined)
            init();
    }, [Segmented])

    useEffect(() => {
        const backAction = () => {
            Flag(true)
            dispatch(destroyFormValue())
            dispatch(loadingGlobal(false))
        };

        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
        );

        return () => backHandler.remove();
    }, []);

    const init = () => {

        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then(res => {
            if (!res)
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION)
        })

        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(res => {
            if (!res)
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        })

        Segmented.personGroup.criteriaCustomer.map(person => {
            person.questions.map(question => {
                if (question.selected !== undefined && question.selected == null && question.selected.length > 0)
                    preSave('select', { ...preSaveValue.selectedSegment, ...{ [question.id]: question.options[0] } })
            })
        })

        if (Segmented.personGroup.criteriaCustomer[0]?.contractNo.includes('PL')) {
            setContractType(5)
        }
        else if (Segmented.personGroup.criteriaCustomer[0]?.contractNo.includes('NF') || Segmented.personGroup.criteriaCustomer[0]?.contractNo.includes('PU')) {
            setContractType(3)
        }
    }

    const showQuestion = question => {
        var curr = Object.entries(preSaveValue.selectedSegment).filter((item) => {
            if (item[1].id) {
                if (!CriteriaRelation[item[1].id]) {
                    return false;
                }

                var curMs = CriteriaRelation[item[1].id].findIndex(mlist => mlist == question.id);

                return (curMs >= 0 || item[0] == question.id)
            } else {
                return false;
            }
        })
        return (curr.length > 0);
    }

    const selected = (value, refId) => {

        let k = preSaveValue.questionState.findIndex(quest => quest == 68)

        if (value.id == 59 || k == 0) {
            setFullHeight(false)
        }
        else {
            setFullHeight(true)
        }

        var newSelectedSegment = { ...preSaveValue.selectedSegment }
        var selectValue = newSelectedSegment[refId];

        if (selectValue) {
            //update
            newSelectedSegment[refId] = { ...value };

            let allRemove = recursiveSelect(selectValue.id, [])

            if (allRemove) {
                allRemove.forEach(valId => {
                    delete newSelectedSegment[valId]
                })
            }
            preSave('select', newSelectedSegment)
        }
        else {
            //add
            newSelectedSegment[refId] = value;
            preSave('select', newSelectedSegment)
        }

        let questionDelIds = []
        Object.entries(newSelectedSegment).forEach(item => {
            if (item[1].nextRelation) {
                questionDelIds = [...questionDelIds, ...item[1].nextRelation]
            }
        })
        preSave('question', questionDelIds)
    }

    const recursiveSelect = (selectId, refRemove) => {
        var selectMaster = CriteriaRelation[selectId]
        let tempSelectedSegment = { ...preSaveValue.selectedSegment }

        if (selectMaster) {
            let result = [];
            selectMaster.forEach(curMaster => {
                let selectMaster = curMaster;
                let selectValue = tempSelectedSegment[selectMaster];

                let selectId = null;

                if (selectValue) {
                    selectId = selectValue.id;
                    const res = recursiveSelect(selectId, [...refRemove, selectMaster]);
                    result.push(...res);
                }
                else {
                    const res = refRemove;
                    result.push(res);
                }
            });
            return result;
        }
        else {
            return refRemove;
        }
    }

    const saveAppointmentDate = (id, value) => {
        preSave('date', value)
    }

    const saveAppointmentTime = (id, value) => {
        preSave('time', value)
    }

    const handleAddDate = () => {
        setCalendarVisible(true)
    }

    const handleDatePickerChange = e => {
        if (e.type == 'set') {
            setCalendarVisible(false)
            preSave('date', moment(e.nativeEvent.timestamp).format('YYYY-MM-DD'))
        }
        else
            setCalendarVisible(false)
    }

    const handleAddTime = () => {
        showDate(!Visible)
        setType(1)
        dateGlobal(new Date())
    }

    const commentChange = (e) => {
        preSave('comment', e)
    }

    const TakeImage = async (id, subId,) => {

        const options = {
            noData: false,
            mediaType: 'photo',
            cameraType: 'back',

            permissionDenied: {
                title: 'ปฎิเสธการอนุญาต',
                text: 'กรุณาเปิดการอนุญาตให้ใช้กล้องถ่ายรูปและที่จัดเก็บข้อมูล',
                reTryTitle: 'ลองอีกครั้ง',
                okTitle: 'ยกเลิก'

            },
            storageOptions: {
                skipBackup: true,
            },
            chooseFromLibraryButtonTitle: 'เลือกจากแกลลอรี่',
            takePhotoButtonTitle: 'ถ่ายรูป',
            title: 'เลือกรูปภาพ',
            cancelButtonTitle: 'ยกเลิก'
        }

        let item = {}

        await ImagePicker.showImagePicker(options, async response => {

            if (response.didCancel) {

            } else if (response.error) {
                alert("การอัพโหลดรูปภาพมีปัญหา")
            }
            else if (response.type !== "image/jpeg") {
                alert('กรุณาอัพโหลดรูปภาพที่นามสกุลไฟล์คือ ".jpeg" เท่านั้น')
            }
            else if (response.latitude == undefined && response.longitude == undefined) {
                alert('ไม่พบพิกัดในรูปภาพ กรุณาถ่ายรูปนี้ใหม่')
            }
            else {
                let img = { ...preSaveValue.imageSource }
                item[subId] = response
                img[id] = { ...img[id], ...item }
                preSave('imageSource', img)
            }
        })
        return;
    }


    return (
        <KeyboardAvoidingView style={{flex: 1}}>
            <View style={{flex:1}}>
                <Modal
                    transparent={true}
                    visible={progress !== 0 && progress !== 101}
                    animationType="fade"
                >
                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: "center",
                        backgroundColor: 'rgba(216,229,227, 0.3)'
                    }}
                    >
                        <Text style={{ fontFamily: BQuarkFont, fontSize: 18, color: SECONDARY_GREEN }}>{progress + '%'}</Text>
                        <Progress.Bar progress={progress / 100} width={200} color={SECONDARY_GREEN} />
                    </View>

                </Modal>
                <ScrollView style={[styles.container]}>

                    <View style={styles.header}>
                        <Text style={styles.title}>
                            {Segmented.personGroup.groupName}
                        </Text>
                    </View>
                    {
                        Segmented.personGroup.criteriaCustomer.map((person, Pindex) => {
                            return (
                                <View key={Pindex}>

                                    <Text style={styles.customerName}>
                                        {person.customerName}
                                    </Text>
                                    <View>
                                        {
                                            person.questions.map((question, index) => {
                                                return ((index == 0 || showQuestion(question)) &&
                                                    <View key={index}>
                                                        {
                                                            question.feildType == "select" &&
                                                            <View style={styles.ImageContainner}>
                                                                <View>
                                                                    <Text style={styles.question}>
                                                                        {question.name}
                                                                    </Text>
                                                                </View>
                                                                {question.feildType == "select" && <SegmentedControls
                                                                    options={question.options}
                                                                    extractText={'text'}
                                                                    onSelect={e => selected(e, question.id)}
                                                                />}
                                                            </View>
                                                        }
                                                        {
                                                            question.feildType == "image" &&
                                                            <View style={{ paddingHorizontal: 5, flexDirection: 'row', justifyContent: 'space-between', }}>
                                                                <View>
                                                                    <Text >รูปภาพ</Text>
                                                                </View>
                                                                <View style={styles.ImageButtonContainer}>
                                                                    {
                                                                        [0, 1, 2].map((imgBtn, indexImgBtn) => (
                                                                            <ImageButton
                                                                                id={question.id}
                                                                                subId={indexImgBtn + 1}
                                                                                key={indexImgBtn}
                                                                                imageSource={preSaveValue.imageSource}
                                                                                handlePress={
                                                                                    () => TakeImage(question.id, (indexImgBtn + 1))
                                                                                }
                                                                            />
                                                                        ))
                                                                    }
                                                                </View>
                                                            </View>
                                                        }
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>
                                </View>
                            )
                        })
                    }
                    {
                        preSaveValue.selectedSegment[52] && preSaveValue.selectedSegment[52].id == '59' &&
                        <View>
                            <View style={styles.header}>
                                <Text style={styles.title}>
                                    {Segmented.assetGroup.groupName}
                                </Text>
                            </View>
                            {
                                Segmented.assetGroup.questions.map((asset, assetIndex) => {
                                    return ((showQuestion(asset)) &&
                                        <View key={assetIndex}>
                                            {
                                                asset.feildType == "select" &&
                                                <View style={styles.ImageContainner}>
                                                    <View>
                                                        <Text>
                                                            {asset.name}
                                                        </Text>
                                                    </View>
                                                    {
                                                        asset.feildType == "select" && <SegmentedControls
                                                            options={asset.options}
                                                            extractText={'text'}
                                                            onSelect={e => selected(e, asset.id)}
                                                        />
                                                    }
                                                </View>
                                            }
                                            {
                                                asset.feildType == 'image' &&
                                                <View style={styles.ImageContainner}>
                                                    <View>
                                                        <Text >รูปภาพ</Text>
                                                    </View>
                                                    <View style={styles.ImageButtonContainer}>
                                                        {
                                                            [0, 1, 2].map((imgBtn, indexImgBtn) => (
                                                                <ImageButton
                                                                    id={asset.id}
                                                                    subId={indexImgBtn + 1}
                                                                    key={indexImgBtn}
                                                                    imageSource={preSaveValue.imageSource}
                                                                    handlePress={
                                                                        () => TakeImage(asset.id, (indexImgBtn + 1))
                                                                    }
                                                                />
                                                            ))
                                                        }
                                                    </View>
                                                </View>
                                            }
                                        </View>
                                    )
                                })
                            }
                        </View>
                    }
                    {
                        preSaveValue.selectedSegment[52] && preSaveValue.selectedSegment[52].id == '59' &&
                        <View>
                            <View style={styles.header}>
                                <Text style={styles.title}>
                                    {Segmented.businessGroup.groupName}
                                </Text>
                            </View>
                            {
                                Segmented.businessGroup.questions.map((business, businessIndex) => {
                                    return ((showQuestion(business)) &&
                                        <View key={businessIndex}>
                                            {
                                                business.feildType == "select" &&
                                                <View style={styles.ImageContainner}>
                                                    <View>
                                                        <Text style={!isTablet && { maxWidth: 100 }}>
                                                            {business.name}
                                                        </Text>
                                                    </View>
                                                    {
                                                        business.feildType == "select" && <SegmentedControls
                                                            options={business.options}
                                                            extractText={'text'}
                                                            onSelect={e => selected(e, business.id)}
                                                        />
                                                    }
                                                </View>
                                            }
                                            {
                                                business.feildType == 'image' &&
                                                <View style={styles.ImageContainner}>
                                                    <View>
                                                        <Text >รูปภาพ</Text>
                                                    </View>
                                                    <View style={styles.ImageButtonContainer}>
                                                        {
                                                            [0, 1, 2].map((imgBtn, indexImgBtn) => (
                                                                <ImageButton
                                                                    id={business.id}
                                                                    subId={indexImgBtn + 1}
                                                                    key={indexImgBtn}
                                                                    imageSource={preSaveValue.imageSource}
                                                                    handlePress={
                                                                        () => TakeImage(business.id, (indexImgBtn + 1))
                                                                    }
                                                                />
                                                            ))
                                                        }
                                                    </View>
                                                </View>
                                            }
                                        </View>
                                    )
                                })
                            }
                        </View>
                    }
                    <View style={styles.dateTimeContainer}>
                        <TouchableOpacity style={styles.dateContainer} onPress={() => handleAddDate()} >
                            <View style={{ marginRight: 10 }}>
                                <Text style={[styles.question, { fontFamily: BQuarkFont }]}>วันที่นัดหมาย</Text>
                            </View>
                            <View>
                                <Text style={styles.question} >{buddistEra(preSaveValue.appointmentDate)}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.timeContainer} onPress={handleAddTime} >
                            <View style={{ marginRight: 10 }}>
                                <Text style={[styles.question, { fontFamily: BQuarkFont }]}>เวลา</Text>
                            </View>
                            <View style={{ marginHorizontal: 20 }}>
                                <Text style={styles.question} >{preSaveValue.appointmentTime}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.comment}>
                        <Text style={[styles.question, { fontFamily: BQuarkFont }]} >ความคิดเห็นบันทึกนัดหมาย</Text>
                        <TextInput
                            style={styles.textInput}
                            multiline
                            numberOfLines={5}
                            onChangeText={commentChange}
                            placeholder="ความคิดเห็นของบันทึกนัดหมาย" />
                    </View>
                </ScrollView>
                {calendarVisible &&
                    <View>
                        <DatePicker
                            display="calendar"
                            mode="date"
                            is24Hour={true}
                            value={new Date()}
                            minimumDate={new Date(Date.now())}
                            maximumDate={new Date().setDate(new Date().getDate() + contractType)}
                            onChange={handleDatePickerChange} />
                    </View>
                }
                {(Visible && type == 1) &&
                    <View style={{ zIndex: 999, position: 'absolute', bottom: 0, top: 0, backgroundColor: 'initial', borderWidth: 1 }}>
                        <View style={{ backgroundColor: 'white', opacity: 0.1, minHeight: isTablet ? width * 0.9 : width * 1.3, borderWidth: 1 }}>

                        </View>
                        <View>
                            <DatePickerRow
                                mode="time"
                                saveFunction={saveAppointmentTime}
                                type="1"
                                interval={15}
                            // maxDate={moment().add(5,"days").format('YYYY-MM-DD')}
                            // minDate={moment().format('YYYY-MM-DD')}
                            />
                        </View>
                    </View>
                }
            </View>
        </KeyboardAvoidingView>
    )
}

const { width, height } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        padding: 15,
        // alignContent:'space-between'
    },
    header: {
        borderRadius: 30,
        marginVertical: 15,
        backgroundColor: '#2F4858'
    },
    title: {
        color: 'white',
        paddingVertical: 10,
        fontSize: 25,
        fontFamily: BQuarkFont,
        textAlign: 'center'
    },
    customerName: {
        fontFamily: BQuarkFont,
        fontSize: 20
    },
    ImageButtonContainer: {
        flex: 1,
        height: 150,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    ImageContainner: {
        paddingHorizontal: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    dateTimeContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10
    },
    dateContainer: {
        flexDirection: 'row',
        marginTop: 10,
    },
    timeContainer: {
        flexDirection: 'row',
        marginTop: 10,
    },
    question: {
        fontFamily: LQuarkFont,
        fontSize: 18
    },
    textInput: {
        borderWidth: 1,
        borderColor: SILVER,
        borderRadius: 5
    },
    comment: {
        marginBottom: 20,
        paddingVertical: 10
    }
})

export default AddAppointment;