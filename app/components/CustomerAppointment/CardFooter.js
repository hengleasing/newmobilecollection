import React from "react";
import { View, Text, StyleSheet } from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { LQuarkFont } from "../../utils/Color";

const CardFooter = ({ data }) => {

    const lists = [
        {
            label: data.gps,
            icon: 'map-marker-outline',
        },
        {
            label: data.appointmentTime,
            icon: 'calendar-clock'
        }
    ]
    return (
        <View style={styles.container}>
            {
                lists.map((item, index) => (
                    <View style={styles.row} key={index}>
                        <MaterialCommunityIcons name={item.icon} size={16} style={{marginRight: 5}} />
                        <Text style={styles.text}>
                            {item.label}
                        </Text>
                    </View>
                ))
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 10
    },
    row: {
        flexDirection: 'row'
    },
    text: {
        fontFamily: LQuarkFont,
    }
})

export default CardFooter;