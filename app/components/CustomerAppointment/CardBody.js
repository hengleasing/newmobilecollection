import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { LQuarkFont } from "../../utils/Color";

const CardBody = ({data}) => {
    return(
        <View style={styles.container}>
            <Text style={styles.text}>
               {data.detail}
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 15
    },
    text: {
        fontFamily: LQuarkFont,
        fontSize: 20
    }
})

export default CardBody;