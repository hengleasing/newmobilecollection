import React, { useEffect, useState } from "react";
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, BackHandler } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import useAction from "../../utils/useAction";
import { getAppointment, setflag, setLimitAppointment, getCriteriaRealation, getSegmented } from "../../state/action/AppointmentAction";
import AppointmentCard from "./AppointmentCard";
import { BQuarkFont, SILVER } from "../../utils/Color";
import FabButton from "../common/FabButton";
import { isCloseToBottom } from "../../utils/utils";
import { CommonActions } from "@react-navigation/native";
import { clearAppointmentList, setAppointmentLoading } from "../../state/reducer/AppointmentReducer";
import loadingAnimate from "../../../assets/icon/Loading.json";
import LottieView from 'lottie-react-native';

const CutomerAppointment = ({navigation}) => {

    const dispatch = useDispatch()
    const CusDetail = useSelector(({ WorkListReducer: { customerDetail } }) => customerDetail)
    const AppointmentList = useSelector(({ AppointmentReducer: { appointmentList } }) => appointmentList)
    const Flag = useSelector(({ AppointmentReducer: { flagAppointList } }) => flagAppointList)
    const Page = useSelector(({AppointmentReducer:{appointmentListLimit}})=>appointmentListLimit)
    const loading = useSelector(({AppointmentReducer:{appointmentLoading}})=>appointmentLoading)

    const fetchAppointmentList = useAction(getAppointment)
    const limitAppointList = useAction(setLimitAppointment)
    const fetchCriteriaRelataion = useAction(getCriteriaRealation)
    const fetchSegmented = useAction(getSegmented)
    const flag = useAction(setflag)

    useEffect(()=>{ 
        initLoad()
        return () => {
            dispatch(clearAppointmentList())
            dispatch(setAppointmentLoading(true))
        }
    },[Flag])

    const initLoad = async () =>  {
        if(CusDetail.id && (Flag !== undefined)){
            await fetchAppointmentList(CusDetail.contractNo,0,10)
        }
    }

    useEffect(() => {
        
        const backAction = () => {
            navigation.dispatch(
                CommonActions.reset({
                    index:0,
                    routes: [
                        {name: 'WorkList'}
                    ]
                })
            )
           };
        const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          backAction
        );
    
        return () => backHandler.remove();
      }, []);
      
    const handlePressFab = async () => {
        flag(false)
        dispatch(setAppointmentLoading(true))
        await fetchCriteriaRelataion()
        await fetchSegmented(CusDetail.contractNo)
        navigation.navigate("AddAppointment")
    }

    if(AppointmentList.length < 1 && loading)
        return(
            <LottieView source={loadingAnimate} autoPlay loop speed={2} />
        )
        
    if (AppointmentList.length < 1 && !loading)
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ fontFamily: BQuarkFont, fontSize: 20 }}>ไม่มีข้อมูล</Text>
                </View>
                {CusDetail.docFlag != 300 && <FabButton icon="square-edit-outline" action={handlePressFab} />}
            </View>
        )

    return(
        <View style={styles.container}>
            <ScrollView onScroll={({nativeEvent})=>isCloseToBottom(nativeEvent, fetchAppointmentList, Page, limitAppointList, 'appoint',CusDetail.contractNo )} >
                {
                    AppointmentList.map(appoint => (
                        <AppointmentCard key={appoint.id} datasource={appoint} />
                ))
                }
            </ScrollView>
            <FabButton icon="square-edit-outline" action={handlePressFab} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center',
        padding: 5,
        backgroundColor: SILVER
    }
})
export default CutomerAppointment;