import React, { useState } from "react";
import { View, Text, StyleSheet, Linking } from "react-native";
import CircleImage from "../common/CircleImage";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { SILVER, LQuarkFont, SOLFBLACK, PRIMARY_GREEN } from "../../utils/Color";
import { TouchableOpacity } from "react-native-gesture-handler";
import moment from "moment";
import { buddistEraWithTime } from "../../utils/utils";
import { RNSelectionMenu } from 'react-native-selection-menu'
import useAction from "../../utils/useAction";
import { deleteAppointDetail } from "../../state/action/WorkListAction";
import { useDispatch } from "react-redux";
import { clearAppointmentList } from "../../state/reducer/AppointmentReducer";

const CardHeader = ({data}) => {

    const lists = [
        {
            label: data.name,
            icon:undefined,
            name: 'customerName'
        },
        {
            label: data.branch,
            icon: 'office-building',
            name: 'branchName'
        },
        {
            label: data.createDate,
            icon: 'clock-outline',
            name: 'createDate'
        }
    ]

    const deleteDetail = useAction(deleteAppointDetail)
    const dispatch = useDispatch()

    const handleMoreOption = (e, i ,u) => {
        RNSelectionMenu.Show({
            values: [
               'ลบ'
            ],
            selectedValues: ["แก้ไข", "ลบ"],
            selectionType: 2,
            presentationType: 0,
            enableSearch: false,
            onSelection: selectedValues => {
                switch (selectedValues) {
                    case 'ลบ':
                        dispatch(clearAppointmentList())
                        deleteDetail(data.id, { contractNo: data.contractNo })
                        break;
                
                    default:
                        break;
                }
            },
            title: 'เพิ่มเติม'
        })
    }

    const handlePressMap = () =>{
        Linking.openURL(`https://www.google.com/maps/dir/?api=1&destination=${data.latitude},${data.longitude}&travelmode=driving`)
    }

    return(
        <View style={styles.container}>
            <View style={styles.row}>
                <CircleImage />
                <View style={{marginLeft: 5}}>
                    {
                        lists.map((item, index) => (
                            <View style={styles.row} key={index}>
                                {item.icon && <MaterialCommunityIcons name={item.icon} size={16} color={SILVER} />}
                                <Text style={[styles.text, item.name == 'customerName' && { fontSize: 20, color: SOLFBLACK}]}>
                                    {item.name == 'createDate'? buddistEraWithTime(item.label) + " น." : item.label}
                                </Text>
                            </View>
                        ))
                    }
                </View>
            </View>
            <View style={styles.iconMenu}>
                {
                    data.latitude != 0 && data.longitude != 0 &&
                    <TouchableOpacity>
                        <MaterialCommunityIcons name="map-legend" size={25} color={PRIMARY_GREEN} onPress={handlePressMap} />
                    </TouchableOpacity>
                }
                {
                    moment(data.createDate).format("YYYY-MM-DD") === moment(new Date()).format("YYYY-MM-DD") && <View>
                       
                        <TouchableOpacity style={{marginLeft:10}}>
                            <MaterialCommunityIcons name="dots-vertical-circle-outline" size={25} color={PRIMARY_GREEN} onPress={handleMoreOption} />
                        </TouchableOpacity>
                    </View>
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 15,
        justifyContent:'space-between'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    text: {
        fontFamily: LQuarkFont,
        color: '#969696'
    },
    iconMenu: {
        flexDirection: 'row',
        justifyContent:'space-around',
    }
})

export default CardHeader;