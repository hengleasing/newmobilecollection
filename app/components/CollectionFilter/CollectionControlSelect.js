import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { setForm } from "../../state/reducer/CollectionFilter";
import { Picker, StyleSheet } from "react-native";
const { Item } = Picker

const CollectionControlSelect = ({  name, options }) => {

    const dispatch = useDispatch();
    const val = useSelector(({CollectionFilter})=> {

        if(CollectionFilter.filter[name] == null){
            return null;
        }
        return CollectionFilter.filter[name].value;
    });

  
 
    const onChangePicker = (val) =>{

        dispatch(setForm({
            name,
            value:options.find(i => i.value == val)
        }));
    }

    return (
        <Picker 
            name={name} 
            onValueChange={onChangePicker} 
            selectedValue={val} >
            {options.map(i => <Item key={i.value} label={i.label} value={i.value}  />)}
        </Picker>
    );
};

const styles = StyleSheet.create({
    selectStyle: {
        borderRadius: 20,
        //backgroundColor: PRIMARY_COLOR,
        //alignItems: 'center',
        //paddingVertical: 10,
       // marginVertical: 10,
        borderColor: '#328956',
        borderWidth: 1,
        //flexGrow: 50,
        //height: 50
    }
});

export default CollectionControlSelect;