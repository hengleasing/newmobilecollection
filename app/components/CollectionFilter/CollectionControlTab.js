import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import SegmentedControlTab from "react-native-segmented-control-tab";
import { StyleSheet, Text } from "react-native";
import { setForm } from "../../state/reducer/CollectionFilter";
import { LQuarkFont, BQuarkFont } from '../../utils/Color';

const CollectionControlTab = ({  name, options }) => {

    const [values, setValues] = useState([]);
    const dispatch = useDispatch();
    const ctrVal = useSelector(({CollectionFilter})=> {

        if(CollectionFilter.filter[name] == null){
            return [];
        }

        return CollectionFilter.filter[name].no;
    });
 
    useEffect(() => {
        setValues(options.map(i => i.label))
    }, []);

    const onChange = (val) => {
        
        let setVal = [];

        if (ctrVal.includes(val)) {
            setVal = ctrVal.filter(i => i != val);
        } else {
            setVal = [val];
        }

        dispatch(setForm({
            name:name,
            value:(setVal.length > 0)?{...options[setVal], no:setVal}:null
        }));

    }

    return (
      
        <SegmentedControlTab
            values={values}
            multiple
            selectedIndices={ctrVal}
            onTabPress={onChange}
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.activeTabStyle}
            tabTextStyle={styles.tabStyleTxt}
            activeTabTextStyle={styles.activeTabStyleTxt}
            tabsContainerStyle={{ height: 45 }}

        />
    );
};

const styles = StyleSheet.create({
    tabStyle: {
        borderColor: '#328956'
    },
    tabStyleTxt: {
        color:'#328956',
        fontSize: 25,
        fontFamily: LQuarkFont
    },
    activeTabStyle: {
        backgroundColor: '#328956',
    },
    activeTabStyleTxt: {
        color:'#FFF',
        fontFamily: BQuarkFont
    }
});

export default CollectionControlTab;