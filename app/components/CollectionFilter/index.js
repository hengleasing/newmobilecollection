import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView, TextInput } from "react-native";
import { CommonActions, useNavigation, StackActions } from '@react-navigation/native';
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import CollectionControlTab from "./CollectionControlTab";
import CollectionControlSelect from "./CollectionControlSelect";
import CollectionControlTxtInput from "./CollectionControlTxtInput";
import { LQuarkFont, PRIMARY_COLOR, GRAY, LIGHT_GREEN, BQuarkFont } from "../../utils/Color";
import { resetForm, setPageStack } from "../../state/reducer/CollectionFilter";
import { loadEIRFilter } from "../../state/action/WorkListAction";

const orderByRemainOptions = [
    {
        label: 'เงินต้นคงเหลือต่ำสุด',
        value: '0'
    },
    {
        label: 'เงินต้นคงเหลือสูงสุด',
        value: '1'
    }
];

const paymentStatusOptions = [
    {
        label: 'ไม่ได้จ่ายเงิน',
        value: '0'
    },
    {
        label: 'จ่ายเงินแล้ว',
        value: '1'
    }
];

const overAgingOptions = [
    {
        label: 'ปกติ',
        value: '0'
    },
    {
        label: 'นอก aging',
        value: '1'
    }
];



const CollectionFilter = () => {

    const dispatch = useDispatch();
    const navigationHook = useNavigation();
    const prevPage = useSelector(({ CollectionFilter }) => CollectionFilter.prevPage);
    const agingStatusOptions = useSelector(({ WorkListReducer }) => WorkListReducer.agingEIRGroup);


    useEffect(() => {

        loadEIRFilter()(dispatch);
    }, []);

    const onSubmit = () => {

        if (!prevPage) {
            return false;
        }

        dispatch(setPageStack(""));

        navigationHook.dispatch(
            (e) => {
                return CommonActions.navigate({
                    name: prevPage,
                    params:{ isCompletePage: true, key: 1},
                })
                return CommonActions.reset({
                    index: 0,
                    routes: [
                        {
                            name: prevPage,
                            params: { isCompletePage: true, key: 1},
                            
                        }
                    ]
                })}
        )

    }

    const onReset = () => {
        dispatch(resetForm())
    }

    return (
        <View>
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.mainHeaderText}>เรียงข้อมูลตาม</Text>
                <View>
                    <CollectionControlTab
                        name="orderByRemain"
                        options={orderByRemainOptions} />
                </View>
                <View style={styles.Seperator} />
                <Text style={styles.mainHeaderText}>กรองข้อมูล</Text>

                <Text style={styles.headerText}>เลขที่สัญญา</Text>
                <View style={styles.selectStyle}>
                    <CollectionControlTxtInput name="contractNo" placeholder="เลขที่สัญญา" />
                </View>
                {prevPage == "WorkList" &&
                        <>
                            <Text style={styles.headerText}>ชื่อลูกค้า</Text>
                            <View style={styles.selectStyle}>
                                <CollectionControlTxtInput name="cusName" placeholder="ชื่อลูกค้า" />
                            </View>
                            <Text style={styles.headerText}>ทะเบียนรถ</Text>
                            <View style={styles.selectStyle}>
                                <CollectionControlTxtInput name="carRegis" placeholder="ทะเบียนรถ" />
                            </View>
                        </>
                    }
                    {prevPage == "AssignedWorklist" &&
                        <>
                            <Text style={styles.headerText}>ชื่อลูกค้า</Text>
                            <View style={styles.selectStyle}>
                                <CollectionControlTxtInput name="cusFullname" placeholder="ชื่อลูกค้า" />
                            </View>
                            <Text style={styles.headerText}>ทะเบียนรถ</Text>
                            <View style={styles.selectStyle}>
                                <CollectionControlTxtInput name="cusRegis" placeholder="ทะเบียนรถ" />
                            </View>
                        </>
                    }


                <Text style={styles.headerText}>สถานะการจ่ายเงิน</Text>
                <View>
                    <CollectionControlTab
                        name="paymentStatus"
                        options={paymentStatusOptions} />
                </View>
                {/* <Text style={styles.headerText}>สถานะลูกหนี้</Text>
                <View>
                    <CollectionControlTab
                        name="overAging"
                        options={overAgingOptions} />
                </View> */}
                <Text style={styles.headerText}>ชั้นลูกหนี้</Text>
                <View style={styles.selectStyle}>
                    <CollectionControlSelect
                        name="agingStatus"
                        options={agingStatusOptions} />
                </View>
            </View>
        </ScrollView>
        <View style={styles.btnContainer}>
                    <TouchableOpacity style={styles.resetBtn} onPress={onReset} >
                        <Text style={styles.btn1}>เคลียร์</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.submitBtn} onPress={onSubmit} >
                        <Text style={styles.btn2}>ตกลง</Text>
                    </TouchableOpacity>
                </View>
        </View>
    );
};

const styles = StyleSheet.create({
    btnContainer: {
        flexGrow: 1,
        justifyContent: 'space-between',
        padding: 10,
        marginTop:10,
        flexDirection: 'row',
        position:'absolute',
        left:0,
        bottom:0,
        backgroundColor: 'rgba(255, 255, 255, 0.8)'
        
    },
    submitBtn: {
        borderRadius: 50,
        backgroundColor: PRIMARY_COLOR,
        alignItems: 'center',
        paddingVertical: 10,
        marginVertical: 10,
        borderColor: GRAY,
        borderWidth: 1,
        flexGrow: 50,
        height: 50
    },
    resetBtn: {
        borderRadius: 50,
        backgroundColor: '#0e1111',
        alignItems: 'center',
        paddingVertical: 10,
        marginVertical: 10,
        borderColor: GRAY,
        borderWidth: 1,
        flexGrow: 50,
        height: 50
    },
    btn1: {
        alignItems: 'center',
        color: '#fff',
        fontSize: 25,
        fontFamily: LQuarkFont
    },
    btn2: {
        alignItems: 'center',
        color: '#fff',
        fontSize: 25,
        fontFamily: LQuarkFont
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        //alignItems: 'center',
        backgroundColor: 'white',
        padding: 10,
        paddingBottom:90
    },
    mainHeaderText: {
        marginTop: 8,
        padding: 3,
        fontSize: 30,
        color: '#444444',
        textAlign: 'center',
        fontFamily: BQuarkFont
    },
    headerText: {
        marginTop: 10,
        padding: 8,
        color: '#444444',
        textAlign: 'left',
        fontFamily: BQuarkFont,
        fontSize: 18
    },
    Seperator: {
        marginHorizontal: -10,
        alignSelf: 'stretch',
        borderTopWidth: 1,
        borderTopColor: '#BEBEBE',
        marginTop: 24,
    },
    selectStyle: {
        borderRadius: 6,
        //backgroundColor: PRIMARY_COLOR,
        //alignItems: 'center',
        //paddingVertical: 10,
        // marginVertical: 10,
        borderColor: '#328956',
        borderWidth: 1,
        //flexGrow: 50,
        //height: 50
    }

});

export default CollectionFilter;