import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView, TextInput } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { setForm } from "../../state/reducer/CollectionFilter";
import { LQuarkFont, SILVER } from "../../utils/Color";

const CollectionControlTxtInput = ({ name, placeholder }) => {

    const dispatch = useDispatch();
    const val = useSelector(({CollectionFilter})=> {

        if(CollectionFilter.filter[name] == null){
            return null;
        }
        return CollectionFilter.filter[name];
    });

  
 
    const onChange = (val) =>{

        dispatch(setForm({
            name,
            value:val
        }));
    }

    return (
        <TextInput
            value={val}
            onChangeText={onChange}
            placeholder={placeholder}
            placeholderTextColor={SILVER}
            style={{fontFamily: LQuarkFont, fontSize: 20}}
        />
    );
};

export default CollectionControlTxtInput;