import React, { useEffect, useMemo, useCallback, useRef } from "react";
import { View, Text, TouchableOpacity, PermissionsAndroid } from "react-native";
import { NavigationContainer, CommonActions, useNavigation, useNavigationState, useFocusEffect } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import NavigationService, { navigationRef } from "../services/NavigationService";
import { StackActions } from '@react-navigation/native';

import LoginPage from "./login";
import SecondaryPage from "./login/secondScreen";
import CurrentWorkListPage from "./workList/CurrentWorkList";
import FinishedWorkListPage from "./workList/FinishedWorkList";
import SearchScreen from "./workList/Search";
import Report from "./report";
import CustomerDetailPage from "./CustomerDetail";
import CustomerAddressDetailPage from "./CustomerAddressDetail";
import CustomerAppointmentPage from "./CustomerAppointment";
import PaymentDetailPage from "./PaymentDetail";
import BoundsmanDetailPage from "./CustomerDetail/BoundsmanDetail";
import PaymentHistoryPage from "./PaymentDetail/PaymentHistory";
import AddAppointmentPage from "./CustomerAppointment/AddAppointment";
import AssignedWorklistPage from "./AssignedWorklist";
import PaymentPage from "./PaymentDetail/Payment";
import TempReceiptReport from "./PaymentDetail/TempReceiptReport";
import CollectionFilter from "./CollectionFilter";

import {
    LQuarkFont,
    BQuarkFont,
    GLOBLIN_GREEN,
    LIGHT_AZURE,
    SECONDARY_GREEN,
    PRIMARY_COLOR,
    PRIMARY_GREEN,
    RED
} from "../utils/Color";
import { clearBoundDetail, clearCusDetail, clearCurrentWorkList, clearCompleteList } from "../state/reducer/WorkListReducer";
import { setPageStack, resetForm, setPrevPage } from "../state/reducer/CollectionFilter";
import { useDispatch, useSelector } from "react-redux";
import { SwitchActions, NavigationActions } from "@react-navigation/compat";
import useAction from "../utils/useAction";
import { setflag, getAnswer, postAppointmentData } from "../state/action/AppointmentAction";
import { GetCurrentWorkList, GetCompleteWorkList } from "../state/action/WorkListAction";
import { destroyFormValue, clearAppointmentList } from "../state/reducer/AppointmentReducer";
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import { Popup, Root } from './common/CustomModal'
import { resetAuthReducer } from "../state/reducer/AuthReducer";
import AsyncStorage from "@react-native-community/async-storage";
import { checkAuth } from "../utils/utils";
import LottieView from 'lottie-react-native';
import loading from "../../assets/icon/Loading.json";
import {useNetInfo} from "@react-native-community/netinfo";
import DropdownAlert from 'react-native-dropdownalert';
import Environment from "../Environment";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const MainTabs = createMaterialTopTabNavigator();

const Logout = ({ navigation, route }) => {
    const dispatch = useDispatch()

    useFocusEffect(
        useCallback(
            () => {
                init()
            },
            [],
        )
    )

    const init = async () => {
        navigation.dangerouslyGetParent().reset({
            index: 0,
            routes: [{ name: 'Login' }],
        })
        dispatch(resetAuthReducer())
        AsyncStorage.clear()
    }

    return (
        <View style={{width:'100%',height:'100%',alignItems:'center',justifyContent:'center'}}>
            <View style={{width: '100%', height: '20%', flex: 1, alignItems: 'center', justifyContent: 'center', }}>
            <LottieView source={loading} autoPlay loop speed={2}  resizeMode='center' />
            <Text style={{fontSize: 20, paddingTop: 100, fontFamily: LQuarkFont}}>กำลังลงชื่อออกจากระบบ...</Text>
            </View>
        </View>
    )
}

const TabDetailList = [
    {
        name: 'CustomerDetail',
        component: CustomerDetailPage,
        options: {
            tabBarLabel: 'ข้อมูลลูกค้า',
            tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="account" color={color} size={20} />
            ),
        }
    },
    {
        name: 'CustomerPayment',
        component: PaymentDetailPage,
        options: {
            tabBarLabel: 'ข้อมูลการชำระเงิน',
            tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="file-document-outline" color={color} size={20} />
            ),
        }
    },
    {
        name: 'CustomerAddress',
        component: CustomerAddressDetailPage,
        options: {
            tabBarLabel: 'ข้อมูลแผนที่',
            tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="map" color={color} size={20} />
            ),
        }
    },
    {
        name: 'CustomerAppointment',
        component: CustomerAppointmentPage,
        options: {
            tabBarLabel: 'ข้อมูลบันทึกนัดหมาย',
            tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="message-alert" color={color} size={20} />
            ),
        }
    }
]

const TabDetail = (props) => {

    return (
        <Root>
            <MainTabs.Navigator tabBarPosition="top" tabBarOptions={{
                activeTintColor: LIGHT_AZURE,
                inactiveTintColor: GLOBLIN_GREEN,
                showIcon: true,
                labelStyle: {
                    fontFamily: LQuarkFont,
                },
                tabStyle: {
                    backgroundColor: SECONDARY_GREEN,
                    paddingHorizontal: 0
                },
                allowFontScaling: true
            }}
                initialRouteName="CustomerDetail"
                swipeEnabled={false}
            >
                {
                    TabDetailList.map((tab, index) => (
                        <MainTabs.Screen key={index} name={tab.name} component={tab.component} options={tab.options} />
                    ))
                }
            </MainTabs.Navigator>
        </Root>
    )
}

const TabList = [
    {
        name: 'Current',
        component: CurrentWorkListPage,
        options: {
            tabBarLabel: 'รายการงานที่กำลังติดตาม',
            tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="archive" color={color} size={20} />
            ),
        }
    },
    {
        name: 'Finish',
        component: FinishedWorkListPage,
        options: {
            tabBarLabel: 'รายการงานที่เสร็จแล้ว',
            tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="cloud-upload" color={color} size={20} />
            ),

        }
    },
    // {
    //     name: 'Search',
    //     component: SearchScreen,
    //     options: {
    //         tabBarLabel: 'ค้นหา',
    //         tabBarIcon: ({ color, size }) => (
    //             <MaterialCommunityIcons name="feature-search-outline" color={color} size={20} />
    //         ),
    //     }
    // },
    {
        name: 'TempReceiptReport',
        component: TempReceiptReport,
        options: {
            tabBarLabel: 'รายการใบเสร็จรับเงินชั่วคราว',
            tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="chart-bar" color={color} size={20} />
            )
        }
    }
]

const TabNavigator = ({ navigation }) => {

    return (
        <MainTabs.Navigator
            tabBarPosition="top"
            tabBarOptions={{
                activeTintColor: LIGHT_AZURE,
                inactiveTintColor: GLOBLIN_GREEN,
                showIcon: true,
                labelStyle: {
                    fontFamily: LQuarkFont,
                    // fontSize: 20,
                },
                tabStyle: {
                    backgroundColor: SECONDARY_GREEN,
                    paddingHorizontal: 0
                }
            }}
        >
            {
                TabList.map((tab, index) => <MainTabs.Screen
                    key={index}
                    name={tab.name}
                    component={tab.component}
                    navigation={navigation}
                    options={tab.options} />)
            }
        </MainTabs.Navigator>
    )
}

const AppNavigator = ({ navigation, route }) => {
    const dispatch = useDispatch()
    const navigationHook = useNavigation();
    // const naviState = useNavigationState(state => state.routes[0]);
    const naviState = useNavigationState(({ routes }) => {

        let naviState = routes[0];

        let tabActive = "Current";
        if (naviState.state && naviState.state.routes[0]) {

            const workPage = naviState.state.routes[0];
            if (workPage.state && workPage.state.routes[workPage.state.index]) {
                dispatch(resetForm())
                tabActive = workPage.state.routes[workPage.state.index].name;
            }
        }

        return tabActive;

    });
    const prevPage = useSelector(({ CollectionFilter }) => CollectionFilter.prevPage);
    const fetchCurrentWorklist = useAction(GetCurrentWorkList);
    const fetchCompleteWorkList = useAction(GetCompleteWorkList);


    const handleDrawer = () => {
        navigation.openDrawer()
    }

    const handleFilter = () => {
       
        //add page stack
        dispatch(setPageStack("WorkList"));
        dispatch(setPrevPage("WorkList"));
        navigationHook.navigate("CollectionFilter");


    }

    const handleRefresh = () => {

        dispatch(resetForm());
        //fetch
        switch (naviState) {
            case 'Current':
                dispatch(clearCurrentWorkList())
                fetchCurrentWorklist(0, 10);
                break;
            case 'Finish':
                dispatch(clearCompleteList())
                fetchCompleteWorkList(0, 10);
                break;
            default:
                break;
        }

        navigation.navigate('MainWorkList', { screen: naviState });

    }

    const icons = [
        {
            name: 'filter',
            onPress: handleFilter
        },
        {
            name: 'autorenew',
            onPress: handleRefresh
        }
    ]

    const handleBack = () => {
        dispatch(clearCusDetail())

        // switch (naviState) {
        //     case 'Current':
        //         dispatch(clearCurrentWorkList())
        //         fetchCurrentWorklist(0,10);
        //         break;
        //     case 'Finish':
        //         dispatch(clearCompleteList())
        //         fetchCompleteWorkList(0,10);
        //         break;
        //     default:
        //         break;
        // }
        if (prevPage == 'AssignedWorklist') {
            navigation.navigate(prevPage);
            //NavigationService.backNavigate()
        } else {
            navigation.navigate('MainWorkList', { screen: prevPage });
        }
    }

    return (
        <Stack.Navigator >
            <Stack.Screen
                name="MainWorkList"
                component={TabNavigator}
                options={{
                    headerTitle: 'รายการงาน',
                    headerStyle: {
                        backgroundColor: Environment.SERVER_TYPE == "PORDUCTION" ? PRIMARY_COLOR : RED,
                    },
                    headerTintColor: 'white',
                    headerTitleStyle: {
                        fontSize: 30,
                        fontFamily: BQuarkFont
                    },
                    headerLeft: () => (
                        <View style={{ paddingLeft: 10 }}>
                            <TouchableOpacity onPress={handleDrawer} >
                                <MaterialCommunityIcons name="menu" size={30} color={LIGHT_AZURE} />
                            </TouchableOpacity>

                        </View>
                    ),
                    headerRight: () => {

                        if(!["Current", "Finish"].includes(naviState)){
                        // if (!["Current"].includes(naviState)) {
                            return null;
                        }

                        return (
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', minWidth: 180 }}>
                                {
                                    icons.map((icon, index) => (
                                        <TouchableOpacity key={index} onPress={icon.onPress} >
                                            <MaterialCommunityIcons name={icon.name} size={30} color={LIGHT_AZURE} />
                                        </TouchableOpacity>
                                    ))
                                }
                            </View>
                        )
                    }
                }}
            />
            <Stack.Screen name="MainDetail" component={TabDetail} options={{
                title: 'รายการข้อมูลลูกค้า',
                headerTitleStyle: {
                    fontFamily: BQuarkFont,
                    fontSize: 30,
                    color: LIGHT_AZURE,
                },
                headerStyle: {
                    backgroundColor: Environment.SERVER_TYPE == "PORDUCTION" ? PRIMARY_COLOR : RED
                },
                headerLeft: () => (
                    <View style={{ paddingLeft: 10 }}>
                        <TouchableOpacity onPress={handleBack} >
                            <MaterialCommunityIcons name="keyboard-backspace" size={30} color={LIGHT_AZURE} />
                        </TouchableOpacity>

                    </View>
                ),
            }} />
            {/* <Stack.Screen 
                name="CollectionFilter" 
                component={CollectionFilter} 
                options={{
                    headerTitle: 'ค้นหารายการ',
                    headerStyle: {
                        backgroundColor: PRIMARY_COLOR,
                    },
                    headerTintColor: 'white',
                    headerTitleStyle: {
                        fontSize: 30,
                        fontFamily: BQuarkFont
                    },
                    headerLeft: () => (
                        <View style={{ paddingLeft: 10 }}>
                            <TouchableOpacity onPress={handleBack} >
                                <MaterialCommunityIcons name="keyboard-backspace" size={30} color={LIGHT_AZURE} />
                            </TouchableOpacity>
    
                        </View>
                    ),
                }}
                
            /> */}
        </Stack.Navigator>
    )
}

const drawerList = [
    {
        name: "Home",
        component: AppNavigator,
        options: {
            title: "หน้าหลัก"
        }
    },
    {
        name: 'Logout',
        component: Logout,
        options: {
            title: 'ออกจากระบบ'
        }
    }
]

const DrawerNavigator = () => {
    return (
        <Drawer.Navigator initialRouteName="Home"  >
            {
                drawerList.map((drawer, index) => (
                    <Drawer.Screen key={index} name={drawer.name} component={drawer.component} options={drawer.options} />
                ))
            }
        </Drawer.Navigator>
    )
}

const MainNavigator = () => {

    const netInfo = useNetInfo()
    const dispatch = useDispatch()
    const ref = useRef(null)

    const Flag = useAction(setflag)
    const answerStep = useAction(getAnswer)
    const postAppointment = useAction(postAppointmentData)

    const prevPage = useSelector(({ CollectionFilter }) => CollectionFilter.prevPage);
    const customeDetail = useSelector(({ WorkListReducer: { customerDetail } }) => customerDetail)
    const preSaveValue = useSelector(({ AppointmentReducer: { postData } }) => postData)

    useEffect(()=>{
     if(netInfo.isConnected)
        if(!netInfo.isInternetReachable)
            ref.current.alertWithType('warn', 'คำเตือน', 'ไม่มีอินเตอร์เน็ต')
        else
            ref.current.alertWithType('success', 'แจ้งเตือน', 'เชื่อมต่ออินเตอร์เน็ตแล้ว')
    },[ref, netInfo.isInternetReachable])

    const handleBack = () => {

        dispatch(clearBoundDetail())
        NavigationService.backNavigate()

    }

    const handleBackFromAssigned = () => {
        dispatch(clearCusDetail())

        if (prevPage == 'AssignedWorklist') {
            NavigationService.navigate(prevPage);
            //NavigationService.backNavigate()
        } else {
            NavigationService.navigate('WorkList', { screen: prevPage });
        }



    }

    const handleBackAddpointment = () => {
        dispatch(destroyFormValue())
        Flag(true)
        NavigationService.backNavigate()
    }

    const Save = async () => {
        const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION)

        if (!granted)
            RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
                .then(data => Popup.show({
                    type: 'Success',
                    title: 'ตำแหน่งที่ตั้ง',
                    textBody: 'คุณเปิดตำแหน่งที่ตั้งแล้ว',
                    buttonText: 'ตกลง',
                    autoClose: true,
                    timing: 1,
                    callback: () => Popup.hide()
                }))
                .catch(error => Popup.show({
                    type: 'Danger',
                    title: 'ตำแหน่งที่ตั้ง',
                    textBody: JSON.stringify(error),
                    buttonText: 'ตกลง',
                    autoClose: true,
                    timing: 1,
                    callback: () => Popup.hide()
                }))

        const validateImageResult = validateImage(preSaveValue)

        if (validateImageResult.isValidate) {
            const validateResult = await validateValues(preSaveValue)
            if (!validateResult.isValidate) { 
                // setTimeout(() => {
                    Popup.show({
                        type: 'Warning',
                        title: 'กรอกข้อมูลไม่ครบถ้วน',
                        textBody: validateResult.message,
                        buttonText: 'ตกลง',
                        autoClose: true,
                        timing: 5000  ,
                        callback: () => Popup.hide()
                    })
                // }, 1000);
               
                return
            }
        }
        else {

            if (!validateImageResult.isValidate) {
                // setTimeout(() => {
                    Popup.show({
                        type: 'Warning',
                        title: 'การอัพรูปถ่ายไม่ครบถ้วน',
                        textBody: validateImageResult.message,
                        buttonText: 'ตกลง',
                        autoClose: true,
                        timing: 5000,
                        callback: () => Popup.hide()
                    })
                // }, 5000);
                return
            }
        }

        const data = prepareData(preSaveValue)
        const mediaFiles = prepareMediaFile(preSaveValue)
        let res = await postAppointment(data, mediaFiles)

        if (res == true) {
            Flag(true)
            const popActions = StackActions.pop(1);
            navigationRef.current?.dispatch(popActions);
        }
        else {
            Popup.show({
                type: 'Warning',
                title: 'ข้อผิดพลาด ' + res.status,
                textBody: res.data,
                buttonText: 'ตกลง',
                callback: () => Popup.hide()
            })
        }
    }

    const validateValues = async appointmentData => {
        let stateAns = await answerStep(appointmentData.questionState);
        let selected = Object.entries(appointmentData.selectedSegment).map(item => item[1].id)

        const noneSelect = []

        stateAns.forEach(ans => {
            const sel = ans.lists.filter(x => selected.includes(x))
            if (sel.length <= 0)
                noneSelect.push(ans.headId)
        })

        if (noneSelect.length > 0)
            return {
                isValidate: false,
                message: 'กรุณากรอกข้อมูลให้ครบถ้วน'
            }
        else
            if (selected.length > 0)
                return {
                    isValidate: true
                }
            else
                return {
                    isValidate: false,
                    message: 'กรุณากรอกข้อมูลให้ครบถ้วน'
                }
    }

    const validateImage = appointmentData => {
        const validateArray = Object.entries(appointmentData.selectedSegment).map(img => img[1])

        var notExist = []
        var boundManNotExist = []

        validateArray.forEach(list => {
            switch (list.id) {
                case 59:
                    [177, 181, 182].forEach(imgId => {
                        let imageObject = Object.entries(appointmentData.imageSource).filter(img => parseInt(img[0]) == imgId)

                        if (imageObject.length <= 0)
                            notExist.push(imgId)
                    })
                    break;

                case 62:
                    [178].forEach(imgId => {
                        let imageObject = Object.entries(appointmentData.imageSource).filter(img => parseInt(img[0]) == imgId)

                        if (imageObject.length <= 0)
                            boundManNotExist.push(imgId)
                    })
                    break;
                case 62:
                    [179].forEach(imgId => {
                        let imageObject = Object.entries(appointmentData.imageSource).filter(img => parseInt(img[0]) == imgId)

                        if (imageObject.length <= 0)
                            boundManNotExist.push(imgId)
                    })
                    break;
                case 62:
                    [180].forEach(imgId => {
                        let imageObject = Object.entries(appointmentData.imageSource).filter(img => parseInt(img[0]) == imgId)

                        if (imageObject.length <= 0)
                            boundManNotExist.push(imgId)
                    })
                    break;
            }
        })

        if (notExist.length > 0) {
            notExist = []
            return {
                message: 'กรุณาอัพโหลดรูปภาพให้ครบทุกประเภท',
                isValidate: false
            }
        }

        if (boundManNotExist.length > 0) {
            boundManNotExist = []
            return {
                message: 'กรุณาอัพโหลดภาพผู้ค้ำ',
                isValidate: false
            }
        }

        if (Object.entries(appointmentData.imageSource).length > 0) {
            var noGpsList = Object.entries(appointmentData.imageSource).filter(imgs => {

                if (imgs[1] != undefined)
                    return Object.entries(imgs[1]).filter(file => (file[1].latitude === undefined && file[1].longitude === undefined)).length > 0
            })

            if (noGpsList.length > 0)
                return {
                    message: 'กรุณาอัพโหลดรูปภาพทีมีพิกัด',
                    isValidate: false
                }
            else
                return {
                    isValidate: true
                }
        }
        else
            return {
                isValidate: true
            }
    }

    const handleSaveฺButton = () => {
        Popup.show({
            type: 'Confirm',
            title: 'บันทึกนัดหมาย',
            textBody: 'คุณต้องการบันทึกการนัดหมายหรือไม่',
            callback: (e) => {
                if (e)
                    Save()

                Popup.hide()
            }
        })
    }

    const prepareData = appointmentData => {

        const selectedArray = Object.entries(appointmentData.selectedSegment).map(item => item[1])
        let GpsImg = []

        if (Object.entries(appointmentData.imageSource).length > 0)
            Object.entries(appointmentData.imageSource).find(gps => {
                if (gps[1] != undefined)
                    Object.entries(gps[1]).find(item => {
                        GpsImg.push(item[1])
                    })
            })
        else
            GpsImg.push({ latitude: 0, longitude: 0 })

        return {
            contractNo: customeDetail.contractNo,
            collectionAppointmentID: appointmentData.appointmentId,
            detail: appointmentData.comment,
            latitude: GpsImg[0].latitude,
            longtitude: GpsImg[0].longitude,
            appointmentDate: appointmentData.appointmentDate,
            appointmentTime: appointmentData.appointmentTime,
            trackingCriterias: selectedArray,
        }
    }

    const prepareMediaFile = appointmentData => {

        const imageArray = Object.entries(appointmentData.imageSource).map(group => {
            if (group[1] != undefined)
                return Object.entries(group[1]).map(list => {
                    return {
                        name: 'MediaFiles',
                        filename: group[0] + '#' + list[0] + '#' + list[1].latitude + '#' + list[1].longitude + '.jpg',
                        filepath: list[1].path,
                        filetype: list[1].type,
                        uri: list[1].uri
                    }
                })
        })
        return imageArray;
    }

    const list = [
        {
            name: 'Login',
            component: LoginPage,
            options: {
                headerShown: false
            }
        },
        {
            name: 'Secondary',
            component: SecondaryPage,
            options: {
                headerShown: false
            }
        },
        {
            name: 'AssignedWorklist',
            component: AssignedWorklistPage,
            options: {
                title: 'เพิ่มรายการงาน',
                headerStyle: {
                    backgroundColor: Environment.SERVER_TYPE == "PORDUCTION" ? PRIMARY_COLOR : RED
                },
                headerTintColor: 'white',
                headerTitleStyle: {
                    fontFamily: BQuarkFont,
                    fontSize: 30
                },
                headerLeft: () => (
                    <View style={{ paddingLeft: 10 }}>
                        <TouchableOpacity onPress={() => {
                            dispatch(resetForm());
                            NavigationService.navigate('WorkList');
                            dispatch(setPrevPage('WorkList'))
                        }} >
                            <MaterialCommunityIcons name="keyboard-backspace" size={30} color={LIGHT_AZURE} />
                        </TouchableOpacity>

                    </View>
                ),
            }
        },
        {
            name: 'WorkList',
            component: DrawerNavigator,
            options: {
                headerShown: false
            }
        },
        {
            name: 'CustomerDetail',
            component: CustomerDetailPage,
            options: {
                title: 'ข้อมูลลูกค้า',
            }
        },
        {
            name: 'BoundsmanDetail',
            component: BoundsmanDetailPage,
            options: {
                headerStyle: {
                    backgroundColor: Environment.SERVER_TYPE == "PORDUCTION" ? PRIMARY_COLOR : RED
                },
                headerTintColor: 'white',
                headerTitleStyle: {
                    fontFamily: BQuarkFont,
                    fontSize: 20
                },
                headerLeft: () => (
                    <View style={{ paddingLeft: 10 }}>
                        <TouchableOpacity onPress={handleBack} >
                            <MaterialCommunityIcons name="keyboard-backspace" size={30} color={LIGHT_AZURE} />
                        </TouchableOpacity>

                    </View>
                ),
            }
        },
        {
            name: 'paymentHistory',
            component: PaymentHistoryPage,
            options: {
                title: 'ประวัติการชำระ',
                headerStyle: {
                    backgroundColor: Environment.SERVER_TYPE == "PORDUCTION" ? PRIMARY_COLOR : RED
                },
                headerTintColor: 'white',
                headerTitleStyle: {
                    fontFamily: BQuarkFont,
                    fontSize: 30
                },
                headerLeft: () => (
                    <View style={{ paddingLeft: 10 }}>
                        <TouchableOpacity onPress={handleBack} >
                            <MaterialCommunityIcons name="keyboard-backspace" size={30} color={LIGHT_AZURE} />
                        </TouchableOpacity>

                    </View>
                ),
            }
        },
        {
            name: 'printTempReceipt',
            component: PaymentPage,
            options: {
                title: 'พิมพ์ใบเสร็จรับเงินชั่วคราว',
                headerStyle: {
                    backgroundColor: Environment.SERVER_TYPE == "PORDUCTION" ? PRIMARY_COLOR : RED,
                },
                headerTintColor: 'white',
                headerTitleStyle: {
                    fontFamily: BQuarkFont,
                    fontSize: 30
                }
            }
        },
        {
            name: 'AddAppointment',
            component: AddAppointmentPage,
            options: {
                title: 'บันทึกนัดหมาย',
                headerStyle: {
                    backgroundColor: Environment.SERVER_TYPE == "PORDUCTION" ? PRIMARY_COLOR : RED
                },
                headerTintColor: 'white',
                headerTitleStyle: {
                    fontFamily: BQuarkFont,
                    fontSize: 30
                },
                headerLeft: () => (
                    <View style={{ paddingLeft: 10 }}>
                        <TouchableOpacity onPress={handleBackAddpointment} >
                            <MaterialCommunityIcons name="keyboard-backspace" size={30} color={LIGHT_AZURE} />
                        </TouchableOpacity>

                    </View>
                ),
                headerRight: () => (
                    <View style={{ paddingRight: 10 }}>
                        <TouchableOpacity onPress={handleSaveฺButton} >
                            <MaterialCommunityIcons name="content-save-outline" size={30} color={LIGHT_AZURE} />
                        </TouchableOpacity>
                    </View>
                ),
            }
        }
    ]

    return (
        <NavigationContainer ref={navigationRef} >
            <Root>
                <DropdownAlert ref={ref} closeInterval closeInterval={1000} />
                <Stack.Navigator initialRouteName="Login"  >
                    {
                        list.map((screen, index) => {
                            return <Stack.Screen key={index} name={screen.name} component={screen.component} options={screen.options} />
                        })
                    }
                    <Stack.Screen
                        name="CollectionFilter"
                        component={CollectionFilter}
                        options={{
                            headerTitle: 'ค้นหารายการ',
                            headerStyle: {
                                backgroundColor: Environment.SERVER_TYPE == "PORDUCTION" ? PRIMARY_COLOR : RED,
                            },
                            headerTintColor: 'white',
                            headerTitleStyle: {
                                fontSize: 30,
                                fontFamily: BQuarkFont
                            },
                            headerLeft: () => (
                                <View style={{ paddingLeft: 10 }}>
                                    <TouchableOpacity onPress={handleBackFromAssigned} >
                                        <MaterialCommunityIcons name="keyboard-backspace" size={30} color={LIGHT_AZURE} />
                                    </TouchableOpacity>

                                </View>
                            ),
                        }}

                    />
                </Stack.Navigator>
            </Root>
        </NavigationContainer>
    )
}

export default MainNavigator;