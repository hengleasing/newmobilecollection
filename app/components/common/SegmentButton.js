import React, { useState, useMemo, useEffect } from "react";
import { TouchableOpacity, Text, View, StyleSheet, Dimensions, ScrollView } from "react-native";
import { SILVER, PRIMARY_GREEN, SECONDARY_GREEN, LQuarkFont } from "../../utils/Color";
import useScreenDimensions from "../../utils/useScreenDimensions";
import DeviceInfo from 'react-native-device-info';

const SegmentControls = (props) => {
    const screenData = useScreenDimensions();
    const [slected, setSlected] = useState()
    let isTablet = DeviceInfo.isTablet();

    let name = props.extractText == undefined ? 'label' : props.extractText
    let lastIndex = props.options.length - 1

    let screenWidth = parseInt(screenData.width / (lastIndex + 1))
   
    const touch = (index, item) => {
        setSlected(index)
        if(props.onSelect)
            props.onSelect(item)
    }

    if(props.options.length > 4 && !isTablet)
        return(
            <View style={[styles.container, {flexDirection:'column', width: '60%'}]}>
            {
                props.options.map((item, index) => {
                    return <TouchableOpacity
                        key={index}
                        onPress={() => touch(index, item)}
                        style={
                            [styles.button,
                            slected == index && { backgroundColor: SECONDARY_GREEN },
                            {borderRadius: 5 }
                            ]}>
                        <Text style={{ textAlign: 'center', color:'white' }} >{item[`${name}`]}</Text>
                    </TouchableOpacity>
                })
            }
        </View>
        )


    return (
        <View  style={styles.container}>
            {
                props.options.map((item, index) => {
                    return <TouchableOpacity
                        key={index}
                        onPress={() => touch(index, item)}
                        style={
                            [styles.button,
                            (index == 0) ? 
                            { 
                                borderTopLeftRadius: 5,
                                borderBottomLeftRadius: 5, 
                                width: screenWidth 
                            } : 
                            { 
                                width: screenWidth *0.7
                            },
                            (index == lastIndex) ? { borderTopRightRadius: 5, borderBottomRightRadius: 5, width: screenWidth, } : null,
                            (lastIndex + 1) >= 4 && { maxWidth: screenData.width * 0.156 },
                            (lastIndex + 1) == 2 && { maxWidth: screenData.width * 0.312 },
                            (lastIndex + 1) == 3 && { maxWidth: screenData.width * 0.207 },
                            (lastIndex + 1) == 1 && { maxWidth: screenData.width * 0.622 },
                            slected == index && { backgroundColor: SECONDARY_GREEN }
                            ]}>
                        <Text style={{ textAlign: 'center', color:'white', fontFamily: LQuarkFont, fontSize:18 }} >{item[`${name}`]}</Text>
                    </TouchableOpacity>
                })
            }
        </View>
    )

}



const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingVertical: 2,
        alignSelf:'center',
    },
    button: {
        display: 'flex',
        padding: 5,
        // flexWrap: 'wrap',
        alignContent: 'center',
        alignItems: 'center',
        backgroundColor: SILVER,
        justifyContent: 'center',
        borderColor: 'white',
        borderWidth: 1
        // maxWidth: screenWidth
    }
})

export default SegmentControls;