import React, { useState } from "react";
import { View, Image, StyleSheet } from "react-native";
import Environment from "../../Environment";
import { useSelector } from "react-redux";

const CircleImage = ({ path }) => {

    const [error, setError] = useState(false)
    const token = useSelector(({ AuthReducer: { tokenData } }) => tokenData)

    const SMBStore = {
        uri: path?.path + "?width=640",
        headers: { Authorization: "Bearer " + token?.access_token }
    }

    const MinIOStore = {
        uri: `${Environment.BASE_API}/api/customers/collections/minioImage?imagePath=${encodeURIComponent(path?.path)}`,
    }

    const handleError = (event) => {
        if (event.nativeEvent.error.includes("code=500") || event.nativeEvent.error.includes("code=404")) {
            setError(!error)
        }
    }

    return (
        <View style={styles.imageContainer}>
            <Image
                source={error ? require("../../../assets/images/defaul-avatar_0.jpg") : path?.imageStore == "SMB" ? SMBStore : MinIOStore}
                style={styles.image}
                onError={handleError}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    image: {
        height: 60,
        width: 60,
        borderRadius: 300
    },
    imageContainer: {
        paddingLeft: 5
    }
})

export default CircleImage;