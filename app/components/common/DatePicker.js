import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity, Text, StyleSheet } from "react-native";
import DatePicker from 'react-native-date-picker'
import moment from "moment";
import { useSelector, useDispatch } from "react-redux";
import { setDateVisible } from "../../state/reducer/GlobalReducer";
import useAction from "../../utils/useAction";
import { DateGlobalAction } from "../../state/action/GlobalAction";
import { LQuarkFont } from "../../utils/Color";

const DatePickerRow = ({saveFunction, type, mode, interval, ...rest}) => {

    const [date, setDate] = useState()
    const dispatch = useDispatch()
    const Visible = useSelector(({GlobalReducer:{datePickerVisible}})=> datePickerVisible)
    const dateFromStore = useSelector(({GlobalReducer:{date}})=> date)
    const setStoreDate = useAction(DateGlobalAction)
    const [disabled, setDisabled] = useState(true)

    useEffect(() => {
        setDate(dateFromStore)
    }, [])

    const Cancel = () => {
     dispatch(setDateVisible(!Visible))
    }

    const Save = async () => {
       
        switch (type) {
            case "0":
                let dateFormat = moment(date).format("YYYY-MM-DD")
                saveFunction(rest.id,dateFormat)
                break;
        
            default:
                let timeFormat = moment(date).format("HH:mm")
                saveFunction(rest.id,timeFormat)
                break;
        }
        setStoreDate(date)
        dispatch(setDateVisible(!Visible))
    }
    
    const handleChange = (e) => {
        setDate(e)
        setDisabled(false)
    }

    return(
        <View>
             <View style={styles.button}>
                        <TouchableOpacity style={{paddingVertical: 10}} onPress={Cancel}>
                            <Text style={styles.text}>ยกเลิก</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{paddingVertical: 10}} onPress={Save} disabled={disabled} >
                            <Text style={styles.text} >บันทึก</Text>
                        </TouchableOpacity>
                </View>
                <DatePicker
                date={dateFromStore}
                style={{width:width, height: width*0.45, backgroundColor: 'white'}}
                // minimumDate={minDate}
                // maximumDate={maxDate}
                mode={mode}
                locale="th_TH"
                onDateChange={handleChange}
                minuteInterval={interval}

                />
        </View>
    )
}

const { width } = Dimensions.get('screen')
const styles = StyleSheet.create({
    button: {
        justifyContent:'space-between', 
        flexDirection: 'row', 
        backgroundColor: 'white', 
        paddingHorizontal: 10,
    },
    text: {
        fontFamily: LQuarkFont,
        fontSize:20
    }
})
export default DatePickerRow;