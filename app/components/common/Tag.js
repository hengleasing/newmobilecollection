import React from "react";
import { Card } from "react-native-shadow-cards";
import { StyleSheet, Text } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { BQuarkFont } from "../../utils/Color";

const Tag = ({
    label,
    name,
    value,
    color,
    bcolor
}) => {

    switch (name) {
        case 'appointmentDate':
            return (
                <Card style={[styles.card, { maxWidth: 120, backgroundColor: color }]}>
                    <Text style={styles.textStyle}>
                        <MaterialCommunityIcons name="calendar-multiselect" size={16} /> {value}
                    </Text>
                </Card>
            )
        case 'remainLast':
            return (
                <Card style={[styles.card, { maxWidth: 105, backgroundColor: color }]}>
                    <Text style={styles.textStyle}>
                        {label} {value}
                    </Text>
                </Card>
            )
        case 'statusAgingEIR':
            return(
                <Card style={[styles.card, { maxWidth: 160, backgroundColor: color }]}>
                    <Text style={styles.textStyle}>
                        {label}
                    </Text>
                </Card>
            )
        default:
            return (
                <Card style={[styles.card, { backgroundColor: color }]}>
                    <Text style={styles.textStyle}>
                        {label} {value}
                    </Text>
                </Card>
            )
    }
}

const styles = StyleSheet.create({
    card: {
        maxWidth: 50,
        marginRight: 8,
        marginVertical:3,
        borderWidth: 1,
        borderColor: 'white'
        
    },
    textStyle: {
        textAlign: 'center',
        color: 'white',
        fontFamily: BQuarkFont,
        fontSize: 14,
        textAlignVertical: 'center',
    }
})
export default Tag;