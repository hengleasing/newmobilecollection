import React, { useState } from "react";
import { View, Text, Dimensions, StyleSheet, Image, TouchableOpacity, ActivityIndicator } from "react-native";
import { Card } from 'react-native-shadow-cards';
import CircleImage from "./CircleImage";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Tag from "./Tag";
import {  BQuarkFont, PRIMARY_GREEN, YELLOW, THIRD_GREEN, NAVI, LQuarkFont, FORTH_GREEN } from "../../utils/Color";
import { buddistEra } from "../../utils/utils";
import TagColor from "../../utils/TagColor";
import Environment from "../../Environment";
import { useSelector } from "react-redux";
import useAction from "../../utils/useAction";
import { GetCustomerImage, getCustomerDetail } from "../../state/action/WorkListAction";
import NavigationService from "../../services/NavigationService";
import LottieView from 'lottie-react-native';
import process from "../../../assets/icon/Loading.json"

const CardWorkList = ({data, action}) => {
   
    const [error, setError] = useState()
    const [loading, setLoading] = useState(false)
    const [imgLoading, setImgLoading] = useState(false)

    const token = useSelector(({AuthReducer:{tokenData}})=>tokenData)
    const getImage = useAction(GetCustomerImage)
    const getDetail = useAction(getCustomerDetail)
    
    const icons = [
        {
            name: 'file',
            text: data.contractNo
        },
        {
            name: 'account-card-details',
            text: data.carRegisNo
        },
        {
            name: 'car',
            text: data.carDescr
        }
    ]

    const tags = [
        {
            label: 'ค้าง',
            name: 'overdue',
            value: data.outstdPeriod.toString(),
            color: TagColor(data.outstdPeriod,data.outstdPeriod, false)
        },
        {
            label: 'ชำระ',
            name: 'mustPay',
            value: data.paidBtwCollPeriod.toString(),
            color: TagColor(data.outstdPeriod,data.paidBtwCollPeriod, true)
        },
        {
            label: 'เหลือ',
            name: 'remain',
            value: data.rmngPeriod.toString(),
            color: TagColor(data.outstdPeriod,data.rmngPeriod, false)
        },
        {
            label: 'เหลือ + ล่าสุด',
            name: 'remainLast',
            value: data.currPeriod.toString(),
            color: TagColor(data.outstdPeriod,data.currPeriod, false)
        },
        {
            label: data.statusAgingEIR,
            name: 'statusAgingEIR',
            value: data.currPeriod.toString(),
            color: NAVI
        },
        {
            label: null,
            name: 'appointmentDate',
            value: data.appointmentDate && buddistEra(data.appointmentDate),
            color: TagColor(null, null, null, data.appointmentDate)
        }
    ]

    const buttonBarIcon = [
        {
            name: 'map-marker'
        },
        {
            name: 'phone'
        }
    ]

    const handleError = (event) => {

        if(event.nativeEvent.error.includes("code=500") || event.nativeEvent.error.includes("code=404")){
            setError(!error)
        }
    }
    const initShowImage = () => {
       
        if(data.thumbnails.length > 0){
            const SMBStore = {
                uri: data.thumbnails[0].path+"?width=640",
                headers: { Authorization: "Bearer " + token.access_token }
            }
            
            const MinIOStore = {
                uri: `${Environment.BASE_API}/api/customers/collections/minioImage?imagePath=${encodeURIComponent(data.thumbnails[1].path)}`,
            }

            if(token && data.thumbnails[0].path !== "") {
                return (
                    <Image 
                    source={error ?require("../../../assets/images/no-image.jpg") : data.thumbnails[0].imageStore == "SMB"? SMBStore : MinIOStore}
                    style={{ width: null, height: 400 }}
                    onError={handleError}
                    />
                )
            }
        }
        else{
          return  <Image 
                    source={require("../../../assets/images/no-image.jpg")}
                    style={{ width: null, height: 400 }}
                />
        }
    }

    const appointButton = () => {
        const handlePress = () => {
            action(data.id, 0, data.contractNo)
        }
        return(
            <TouchableOpacity style={styles.Button} onPress={handlePress} >
                <Text style={styles.appointText}>นัดหมาย</Text>
            </TouchableOpacity>
        )
    }

    const completeButton = () => {
        
        const handlePress = () => {
            action(data.id, 1, data.contractNo)
            setLoading(!loading)

            setTimeout(() => {
                setLoading(false)
            }, 3000);
        }
        if(data.status == 'ปกติ' && data.paidBtwCollPeriod == 0 )
        return(
            <View></View>
        )
        
        return(
            <TouchableOpacity style={styles.Button} onPress={handlePress} disabled={loading} >
                {!loading ?<Text style={styles.completeText}>ยืนยันเสร็จงาน</Text>: <ActivityIndicator size="small" color={FORTH_GREEN} style={{flex: 1}} />}
            </TouchableOpacity>
        )
    }

    const paymentButton = () => {
        const handlePress = () => {
            action(data.id, 2, data.contractNo)
        }
        return(
            <TouchableOpacity style={styles.Button} onPress={handlePress} >
                <Text style={styles.completeText}>รับชำระเงิน</Text>
            </TouchableOpacity>
        )
    }

    const handlePressImage = async () => {
        setImgLoading(!imgLoading)
        let result = await getDetail(data.id, data.contractNo)
        
        if(result)
            NavigationService.navigate('MainDetail')

        
    }

    const handlePressIcon = index => {
        if(index == 0)
            action(data.id, 3, data.contractNo)
        else
            action(data.id, 4, data.contractNo)
    }

    return (
        <Card style={styles.cardContainer} >
            <View style={styles.container}>
                <CircleImage path={data.thumbnails.length > 1  && data.thumbnails[1]} />
                <View style={styles.infomation}>
                    <Text style={styles.name}>{data.name}</Text>
                    {
                        icons.map((icon, index) => (
                            <Text style={styles.detail} key={index}>
                                <MaterialCommunityIcons name={icon.name} /> {icon.text}
                            </Text>
                        ))
                    }
                    <View style={styles.tag}>
                        {
                            tags.map((tag, index) => {
                                if(tag.label == "")
                                    return(
                                        <View key={index}>

                                        </View>
                                    )

                                if (tag.value)
                                    return (
                                        <Tag
                                            key={index}
                                            label={tag.label}
                                            name={tag.name}
                                            value={tag.value}
                                            color={tag.color}
                                        />
                                    )
                            })
                        }
                    </View>
                </View>

            </View>
            <TouchableOpacity onPress={()=>handlePressImage()} disabled={imgLoading} >
                {token && initShowImage()}
                {imgLoading && <LottieView source={process} autoPlay loop />}
            </TouchableOpacity>
            <View style={styles.ButtonBar}>
                {data.docFlag != 300 ?
                <View style={styles.textButton}>
                        {appointButton()}
                        {completeButton()}
                        {paymentButton()}
                </View>:
                <View style={styles.textButton}>
                        {appointButton()}
                        {paymentButton()}
                </View>
                }
                <View style={styles.iconButton}>
                        {
                            buttonBarIcon.map((icon, index) => (
                                <TouchableOpacity key={index} onPress={()=>handlePressIcon(index)} >
                                    <MaterialCommunityIcons name={icon.name} size={25} color={PRIMARY_GREEN} />
                                </TouchableOpacity>
                            ))
                        }
                </View>
            </View>
        </Card>
    )
}

const { height, width } = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        width: width * 0.98,
        flexDirection: 'row',
    },
    cardContainer: {
        width: width * 0.98,
        paddingVertical: 5,
        marginVertical: 5
    },
    infomation: {
        paddingLeft: 10,
        paddingRight: 5,
        marginBottom: 5
    },
    tag: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        maxWidth: '91.5%'
    },
    ButtonBar: {
        flexDirection: 'row',
        justifyContent:'space-between',
        paddingHorizontal: 5,
        marginTop: 10,
    },
    textButton: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    iconButton: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems:'center',
        width: width*0.25
    },
    appointText: {
        fontSize: 20,
        fontFamily: BQuarkFont,
        color: YELLOW
    },
    Button: {
        paddingVertical: 10,
        paddingHorizontal: 10
    },
    completeText: {
        fontSize: 20,
        fontFamily: BQuarkFont,
        color: THIRD_GREEN
    },
    name: {
        fontFamily: LQuarkFont,
        fontSize: 20
    },
    detail: {
        fontSize: 15,
        fontFamily: LQuarkFont,
        flexWrap:'wrap',
        maxWidth: width*0.78
    }
})
export default CardWorkList;