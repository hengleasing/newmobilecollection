import React, { useRef } from "react";
import { View, StyleSheet, Dimensions, Image, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { GRAY } from "../../utils/Color";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";



const ImageButton = ({handlePress, imageSource, handlePhoto, subId, id}) => {

    return (
        <View style={{alignItems:'center'}}>
            {
                (imageSource && imageSource[id] && imageSource[id][subId])&&
                <TouchableOpacity activeOpacity={.5} onPress={()=>handlePhoto( id, subId)} >
                    <Image source={imageSource[id][subId]} style={{width: width * 0.25, height:height*0.1}} />
                </TouchableOpacity>
            }
            {
                (imageSource == undefined || imageSource[id] == undefined || (imageSource[id] && imageSource[id][subId] == undefined)) &&
            <TouchableOpacity  onPress={handlePress}>
                <View style={styles.imgButton} >
                    <MaterialIcons name="add-a-photo" style={{ color: GRAY }} size={20} />
                </View>
            </TouchableOpacity>
            }
        </View>
    )
}

const { width,height } = Dimensions.get('screen')

const styles = StyleSheet.create({
    imgButton: {
        maxHeight: 100,
        maxWidth: 100,
        height: width * 0.25,
        width: width * 0.25,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#99AEBB',
        borderRadius: 5
    },
   
})

export default ImageButton;