import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { SECONDARY_GREEN } from "../../utils/Color";

const FabButton = ({icon, action, iconColor, buttonColor}) => {

    const onPress = () => {
        action()
    }

    return(
        <View style={styles.FabButton}>
            <TouchableOpacity style={[styles.Button, buttonColor && {backgroundColor: buttonColor}]} onPress={onPress}>
                <MaterialCommunityIcons name={icon} size={30} color={iconColor??'white'} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    FabButton:{
        position:'absolute',  
        height: 60, 
        width:60, 
        bottom:30, 
        right: 30,
    },
    Button: {
        backgroundColor: SECONDARY_GREEN,
        borderRadius: 60/2,
        width: 60,
        height:60,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        elevation: 5,
    }
})

export default FabButton;