import React, { useState, useEffect } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import ImageSwiper from "./ImageSwpier";
import CusDetail from "./CusDetail";
import { useSelector } from "react-redux";
import { PRIMARY_GREEN } from "../../utils/Color";

const BoundsmanDetail = ({ route, navigation }) => {
    
    useEffect(() => {
        initLoad()
    }, [])

    const img = useSelector(({ WorkListReducer: { boundManImageList } }) => boundManImageList)
    const cusDetail = useSelector(({ WorkListReducer: { customerBoundsManDetail } }) => customerBoundsManDetail)

    let detail = cusDetail.find(item => item.id == route.params?.id)

    const [resources, setResources] = useState([])

    const initLoad = () => {
        let resources = [
            {
                title: 'ข้อมูลผู้ค้ำ',
                icon: 'account',
                data: [
                    {
                        title: "ชื่อ-นามสกุล:",
                        value: detail.name + ` (${detail.nickName})`,
                        type: 'name'
                    },
                    {
                        title: "เบอร์โทร:",
                        value: detail.tel,
                        type: 'Pnum'
                    },
                    {
                        title: "อาชีพ:",
                        value: detail.occup
                    },
                    {
                        title: "รายละเอียดอาชีพ:",
                        value: detail.occupMemo
                    }
                ]
            },
            {
                title: 'ที่อยู่',
                icon:'home-city',
                data: detail.id && mapAddress(detail.addresses)
            },
        ]
        setResources(resources)
        header()
    }

    const header = () => {
        navigation.setOptions({
            title: `ข้อมูลผู้ค้ำ ${detail.name}`,
            backgroundColor: PRIMARY_GREEN
        })
    }

    const mapAddress = (Address) => {
        let arr = []
        Address.map(ad => {
            arr.push({
                title: `${ad.name}:`,
                value: ad.location.address,
                type: 'address'
            })
        })
        if(arr.length == 0)
             return null
        else
            return arr
    }

    return (
        <ScrollView>
            <ImageSwiper imageList={img} />
            {
                resources && resources.map((re, index) => (
                    <View key={index} style={[styles.container, index == resources.length - 1 && { marginBottom: 10 }]}>
                        <CusDetail icon={re.icon} title={re.title} data={re.data} />
                    </View>
                ))
            }
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: 5,
    }
})

export default BoundsmanDetail