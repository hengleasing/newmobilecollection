import React, { useEffect, useState } from "react";
import SwiperFlatList from 'react-native-swiper-flatlist';
import { View, Text, Image, StyleSheet, Dimensions, TouchableWithoutFeedback, Modal } from "react-native";
import { useSelector } from "react-redux";
import Environment from "../../Environment";
import { TouchableOpacity } from "react-native-gesture-handler";
import ImageViewer from 'react-native-image-zoom-viewer';
import  FontAwesomeIcon  from "react-native-vector-icons/FontAwesome";
const ImageSwiper = ({imageList}) => {
    const token = useSelector(({AuthReducer:{tokenData}})=>tokenData)
    const [visible, setVisible] = useState(false)
    const [imageModalSource, setImageModalSource] = useState([])
    const handdleTouchImage = () => {
        setVisible(!visible)
    }

    useEffect(()=> {
        let list = []
        imageList.forEach(img => {
            let temp = {}
            if(img.path == "undefined"){
                temp = {
                    url:'',
                    props: {
                        source: require("../../../assets/images/no-image.jpg")
                    }
                }
            }
            else if(img.imageStore == "SMB")
                temp = {
                    url: img.path,
                    props:{
                        headers: { Authorization: "Bearer " + token.access_token }
                    }
                }
            else
                temp = {
                    url: `${Environment.BASE_API}/api/customers/collections/minioImage?imagePath=${encodeURIComponent(img.path)}`
                }
            list.push(temp)
        });
        setImageModalSource(list)
    },[])

    return(
        <View style={styles.container}>
            <SwiperFlatList
            showPagination
            paginationActiveColor="green"
            paginationStyleItem={styles.pagination}
            >
                {
                    imageList.map((img, index)=>{

                        const sourceSMB = {
                            uri: img.path,
                            headers: { Authorization: "Bearer " + token.access_token }
                        }
                        
                        const sourceMinIO = {
                            uri: `${Environment.BASE_API}/api/customers/collections/minioImage?imagePath=${encodeURIComponent(img.path)}`
                        }
                           return <View key={index} style={styles.image}>
                                    <TouchableOpacity onPress={handdleTouchImage}>
                                        <Image style={styles.image} source={img.path == "undefined" ? require("../../../assets/images/no-image.jpg"): img.imageStore == "SMB"? sourceSMB : sourceMinIO } resizeMode="cover" />
                                    </TouchableOpacity>
                                </View>
                    })
                }

            </SwiperFlatList>
            <Modal
                visible={visible}
                onRequestClose={handdleTouchImage}
            >
                <ImageViewer
                    imageUrls={imageModalSource}
                    maxOverflow={100}
                    pageAnimateTime={10}
                    useNativeDriver={true}
                    enableSwipeDown={true}
                    onSwipeDown={() => {
                        setVisible(!visible)
                    }}
                />
            </Modal>
        </View>
    )
}

const {width} = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        // borderColor: 'red',
        // borderWidth: 1,
    },
    image: {
        alignContent:'center',
        alignSelf:'center',
        alignItems:'center',
        width:width,
        height: 300,
    },
    pagination: {
        height: 5,
        width: 5,
        // borderRadius: 10/2,
    },
})
export default ImageSwiper;