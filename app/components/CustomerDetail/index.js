import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import ImageSwiper from "./ImageSwpier";
import CusDetail from "./CusDetail";
import { useSelector, useDispatch } from "react-redux";
import { clearCusDetail, clearBoundDetail } from "../../state/reducer/WorkListReducer";

const customerDetail = () => {

    const cusDetail = useSelector(({ WorkListReducer }) => WorkListReducer)
    const imageList = useSelector(({WorkListReducer:{customerImage}})=>customerImage)
    const dispatch =  useDispatch()

    const [resource, setResource] = useState([])
    
    
    useEffect(() => {
        initload()

        return () => {
            dispatch(clearBoundDetail())
            // dispatch(clearCusDetail())
        }
    }, [])

    const mapAddress = (Address) => {

        if(Address.length > 0){ 
            let arr = []
            Address.map(ad=> {
                arr.push({
                    title: `${ad.name}:`,
                    value: ad.location.address,
                    type: 'address'
                })
            })
            if(arr.length == 0)
                return [{
                    title: 'ที่อยู่',
                    value: 'ไม่มีข้อมูลที่อยู่',
                    type: 'address'
                }]
            else
                return arr
        }
        else{
            return [{
                title: 'ที่อยู่',
                value: 'ไม่มีข้อมูลที่อยู่',
                type: 'address'
            }]
        }
            
    }

    const mapBoundsman = (boundsMan) => {
        let arr = []
        if(boundsMan.length > 0 )
            boundsMan.map((bound, index)=>{
                arr.push([
                    {
                        title: bound.relation,
                        type: 'boundsHeader',
                        collectionContactID: bound.id,
                        collectionID :cusDetail.customerDetail.id

                    },
                    {
                        title: 'ชื่อ-นามสกุล:',
                        value: bound.name + ` (${bound.nickName})`,
                        type: 'name'
                    },
                    {
                        title: 'เบอร์โทร:',
                        value: bound.tel,
                        type: 'Pnum'
                    }
                ])
            })

        return arr
    }

    const checkSpouses = () => {
        
        if (cusDetail.customerDetail && cusDetail.customerDetail.spouses && cusDetail.customerDetail.spouses.length == 0){
            return null
        }
        else {
            return [
                {
                    title: 'ชื่อ-นามสกุล:',
                    value: cusDetail.customerDetail.spouses[0].name + ` (${cusDetail.customerDetail.spouses[0].nickName})`,
                    type: 'name'
                },
                {
                    title: 'เบอร์โทร:',
                    value: cusDetail.customerDetail.spouses[0].tel,
                    type: 'Pnum'
                },
                {
                    title: 'ความสัมพันธ์:',
                    value: cusDetail.customerDetail.spouses[0].relation
                },
                ...mapAddress(cusDetail.customerDetail.spouses.length > 0 && cusDetail.customerDetail.spouses[0].addresses)
            ]
        }
    }

    const checkContract = () => {
        let arr = []
        if (cusDetail.customerDetail && cusDetail.customerDetail.contactCustomer && cusDetail.customerDetail.contactCustomer.length == 0){
            return null
        }
        else {
            cusDetail.customerDetail.contactCustomer.map(con=>{
               arr.push([
                    {
                        title: con.relation,
                    },
                    {
                        title: 'ชื่อ-นามสกุล',
                        value: con.name + ` (${con.nickName})`
                    },
                    {
                        title: 'เบอร์โทร:',
                        value: con.tel,
                        type: 'Pnum'
                    },
                    ...mapAddress(con.addresses)
                ])
            })
            return arr
        }
    }

    const initload = () => {
        let resources = [
            {
                title: 'ข้อมูลลูกค้า',
                icon: 'account',
                data: [
                    {
                        title: "ชื่อ-นามสกุล:",
                        value: cusDetail.customerDetail.name + ` (${cusDetail.customerDetail.nickName})`,
                        type: 'name'
                    },
                    {
                        title: "เบอร์โทร:",
                        value: cusDetail.customerDetail.tel,
                        type: 'Pnum'
                    },
                    {
                        title: "อาชีพ:",
                        value: cusDetail.customerDetail.occup
                    },
                    {
                        title: "รายละเอียดอาชีพ:",
                        value: cusDetail.customerDetail.occupMemo
                    }
                ]
            },
            {
                title: 'ที่อยู่',
                icon:'home-city',
                data: cusDetail.customerDetail.id && mapAddress(cusDetail.customerDetail.addresses)
            },
            {
                title: 'คู่สมรส',
                icon: 'account-heart-outline',
                data: checkSpouses()
            },
            {
                title: 'ผู้ค้ำ',
                icon: 'account-group',
                data: cusDetail.customerBoundsManDetail.length > 0 ? mapBoundsman(cusDetail.customerBoundsManDetail):null
            },
            {
                title: 'ผู้ติดต่อ',
                icon: 'account-card-details-outline',
                data: checkContract()
            },
            {
                title: 'หมายเหตุ (เบอร์โทรที่ติดต่อได้)',
                icon: 'clipboard-account',
                data: (cusDetail.customerDetail && cusDetail.customerDetail.memo)?[
                    {
                        title: '',
                        value: cusDetail.customerDetail.memo,
                        type: 'Pnum'
                    }
                ]  : null
            }
        ]
        
        setResource(resources)
    }

    return (
        <ScrollView>
            <ImageSwiper imageList={imageList} />
            {
               resource.length > 0 && resource.map((re, index)=>{
                   return(
                    <View key={index} style={[styles.container, index == resource.length -1 && {marginBottom: 10}]}>
                        <CusDetail icon={re.icon} title={re.title} data={re.data} />
                    </View>
                )})
            }
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: 5,
    }
})
export default customerDetail;