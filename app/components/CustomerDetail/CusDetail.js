import React, { useCallback, useEffect, useState } from "react";
import { Card } from "react-native-shadow-cards";
import { Dimensions, StyleSheet, View, Text, Linking, ActivityIndicator, Modal, TouchableWithoutFeedback } from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FeatherIcons from 'react-native-vector-icons/Feather'
import { LQuarkFont, BQuarkFont, PRIMARY_GREEN, SILVER } from "../../utils/Color";
import { TouchableOpacity } from "react-native-gesture-handler";
import useAction from "../../utils/useAction";
import { getImageBoundMan } from "../../state/action/WorkListAction";
import { useFocusEffect } from "@react-navigation/native";


const CusDetail = ({ icon, title, data }) => {

    const [loading, setLoading] = useState()
    const [phoneNumber, SetPhoneNumber] = useState([])
    const [visible, setVisible] = useState(false)
    const boundImg = useAction(getImageBoundMan)

    useFocusEffect(
        useCallback(() => {
            return () => {
                setLoading()
            };
        }, [])
    )

    const handleBoundmanDetail = (collectionID, collectionContactID) => {
        setLoading(collectionContactID)
        boundImg(collectionID, collectionContactID)
    }

    const handlePhonenumberPress = value => {

        if (!isNaN(value))
            Linking.openURL('tel:' + value)
        else {

            let findComma = /(,)/gm
            let found = value.match(findComma)
            if (found != null) {
                let textsplit = value.split(",")
                let trimtext = textsplit.map(text => {
                    return text.trim();
                })
                SetPhoneNumber(trimtext)
                setVisible(!visible)
            }
            else {
                let finddash = value.match(/(-|[0-9])/gm)
              
                if (finddash != null && finddash[0] == 0) {
                    finddash = finddash.join('')
                    Linking.openURL('tel:' + finddash.replace('-', ''))

                }
            }
        }
    }

    const handlePressNumberOnModal = value => {
        value = value.replace('-', '')

        if (!isNaN(value) && value != '')
            Linking.openURL('tel:' + value)
    }

    const getValue = (val, titles) => {
        if(titles == ''){
           
            let trimText = val.trim().replaceAll(" ","|").replaceAll("-","")
           
            let num = trimText.match(/\d+/gm)

            let text = trimText.replaceAll("|","").match(/[^u0E00-u0E7F]/gm)
        
             num.push(text.join(""))
            let render = <>
                { num.map((i, index) => {
                    if (index == (num.length - 1))
                        return <Text key={index} style={{ textDecorationLine: 'underline', color: 'blue', }}>{i} </Text>
                    else
                        return <Text key={index} style={{ textDecorationLine: 'underline', color: 'blue', }}>{i}, </Text>

                })}
            </>

            return val
        }
        else
            return val
    }

    return (
        <View style={styles.container}>
            <Card style={styles.container}>
                <View style={styles.title}>
                    <MaterialCommunityIcons name={icon} size={25} color={PRIMARY_GREEN} />
                    <Text style={styles.textTitle}>{title}</Text>
                </View>
                {
                    data !== null ?
                        (icon !== 'account-group' && icon !== 'account-card-details-outline') ?
                            data.map((item, index) => {
                                return (
                                    <View key={index} style={[styles.containLayout, (item.type == 'name' || item.type == 'address') && { flexDirection: 'column' }]}>
                                        <Text style={styles.titleContain}>{item.title} </Text>
                                        <Text onPress={() => handlePhonenumberPress(item.value)} style={[styles.contain, item.type == 'Pnum' && { textDecorationLine: 'underline', color: 'blue' }]}>{item.value}</Text>
                                    </View>
                                )
                            })
                            : data.map((item, index) => {
                                return item.map((bound, bindex) => {
                                    return (
                                        <View key={bindex} style={[styles.containLayout, (bound.type == 'name' || bound.type == 'address') && { flexDirection: 'column' }, (bound.type == 'boundsHeader' && { justifyContent: "space-between" })]}>
                                            <Text style={styles.titleContain}>{bound.title} </Text>
                                            {
                                                bound.type == 'boundsHeader' && <TouchableOpacity onPress={() => handleBoundmanDetail(bound.collectionID, bound.collectionContactID)}>
                                                    {
                                                        (loading == bound.collectionContactID && loading != undefined)
                                                            ?
                                                            <ActivityIndicator /> : <MaterialCommunityIcons name="notebook" size={25} color={PRIMARY_GREEN} />
                                                    }
                                                </TouchableOpacity>
                                            }
                                            {
                                                bound.type !== 'boundsHeader' &&
                                                <Text
                                                    style={[styles.contain, bound.type == 'Pnum' && { textDecorationLine: 'underline', color: 'blue' }]}
                                                    onPress={() => handlePhonenumberPress(bound.value)}
                                                >
                                                    {bound.value}
                                                </Text>
                                            }
                                        </View>
                                    )
                                })
                            }) :
                        <View style={styles.containLayout}>
                            <Text>ไม่มีข้อมูล{title}</Text>
                        </View>
                }
            </Card>
            <Modal
                visible={visible}
                onRequestClose={() => setVisible(!visible)}
                transparent={true}
            >
                <TouchableWithoutFeedback onPress={() => {
                    setVisible(!visible)
                }}>
                    <View style={styles.modalPhone}>
                        <View style={styles.modalPhoneContent}>
                            {
                                phoneNumber.map((num, index) => {
                                    return (
                                        <View style={styles.callLayout} key={index}>
                                            <FeatherIcons name="phone-call" size={25} color={PRIMARY_GREEN} />
                                            <Text
                                                key={index}
                                                style={[styles.contain, { paddingLeft: 10, textDecorationLine: 'underline', color: 'blue' }]}
                                                onPress={() => handlePressNumberOnModal(num)}
                                            >
                                                {num}
                                            </Text>
                                        </View>
                                    )
                                })
                            }
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </View>
    )
}

const { width, height } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        width: width * 0.98,
        paddingBottom: 10
    },
    title: {
        paddingVertical: 10,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: SILVER,
        borderBottomWidth: 1,
        marginBottom: 5,
    },
    textTitle: {
        fontFamily: BQuarkFont,
        fontSize: 20,
        marginLeft: 15,
        color: PRIMARY_GREEN,
        textAlignVertical: 'center'
    },
    containLayout: {
        paddingHorizontal: 25,
        flexDirection: 'row',
    },
    titleContain: {
        fontFamily: BQuarkFont,
        fontSize: 18,
        marginRight: 5
    },
    contain: {
        flexWrap: 'wrap',
        fontFamily: LQuarkFont,
        fontSize: 18
        // textAlign: 'left',
        // flexShrink: 1
    },
    modalPhone: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalPhoneContent: {
        paddingHorizontal: 30,
        paddingVertical: 20,
        backgroundColor: 'white',
        borderRadius: 5,
        shadowOpacity: 0.25,
        shadowRadius: 3,
        elevation: 5
    },
    callLayout: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        alignContent: 'center',
        paddingVertical: 10
    }
})

export default CusDetail;