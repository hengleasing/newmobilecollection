import { createRef } from 'react';
import { StackActions } from '@react-navigation/native';
import { NavigationActions } from '@react-navigation/compat';


export const navigationRef = createRef();

const setTopLevelNavigator = (navigatorRef) => {
    _navigator = navigatorRef;
}

const navigate = ( routeName, params ) => {
    
    const navigationAction = NavigationActions.navigate({
        routeName,
        params
    });
    navigationRef.current?.dispatch(navigationAction);
    // _navigator.dispatch(navigationAction)
}

const backNavigate = (key) => {
    const backAction = NavigationActions.back({
        key
    })
    navigationRef.current?.dispatch(backAction)
}

const navigateWithoutStack = (routeName, params) => { 
    const actionToDispatch = StackActions.reset({
        index: 0,
        key: null,
        actions: [NavigationActions.navigate({ routeName })],
      });
      navigationRef.current?.dispatch(actionToDispatch)
}

export default { navigate, backNavigate, setTopLevelNavigator, navigateWithoutStack }