import Axios from "axios";
import { ToastAndroid } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import NavigationService from "./NavigationService";
import qs from "qs";
import { getRefreshToken, getAccessToken } from "../utils/utils";
import Environment from "../Environment";

let axiosInstance = Axios.create();

axiosInstance.interceptors.request.use( async request => {
    const accessToken = await getAccessToken()
   
    request.headers['Authorization'] = `Bearer ${accessToken}`   
  
    return request;
}, (error) => {
    return Promise.reject(error)
})

axiosInstance.interceptors.response.use(async (response) => {
  
    return response;
}, async (error) => {

    const originalRequest  = error.config;
    const refreshToken     = await getRefreshToken();
    const username         = await AsyncStorage.getItem('username');

    if(error.response !== undefined && error.response.status == 401 && !originalRequest._retry && refreshToken !== null) {
        originalRequest._retry = true;

        const data = {
            client_id:      Environment.CLIENT_ID,
            username:       username,
            refresh_token:  refreshToken,
            grant_type:     "refresh_token"
        }
        
        return Axios
          .post(Environment.AUTH_API + '/api/token', qs.stringify(data), {
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          })
          .then(async response => {
            const token = response.data;

            await AsyncStorage.multiSet([
              ['token', JSON.stringify(token)],
              ['accessToken', token.access_token],
              ['refreshToken', token.refresh_token],
            ]);

            originalRequest.headers.Authorization =
              'Bearer' + token.access_token;
            return axiosInstance(originalRequest);
          })
          .catch(error => {
            AsyncStorage.clear(() => NavigationService.navigate('Login'));

            if (error.response.data.message !== undefined)
              ToastAndroid.show(error.response.data.message, ToastAndroid.LONG);
            else if (
              error.response.data.error === 'invalid_grant' &&
              error.response.data.message === undefined
            )
              ToastAndroid.show(
                Environment.MESSAGE.SESSION_EXPIRED,
                ToastAndroid.LONG,
              );
            else
              ToastAndroid.show(error.response.data.error, ToastAndroid.LONG);
          });
    }
    return Promise.reject(error);
});

export default axiosInstance;