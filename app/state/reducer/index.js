import { combineReducers } from "redux";
import AuthReducer from "./AuthReducer";
import WorkListReducer from "./WorkListReducer";
import GlobalReducer from "./GlobalReducer";
import AppointmentReducer from "./AppointmentReducer";
import PaymentReducer from "./PaymentReducer";
import CollectionFilter from "./CollectionFilter";

const indexReducer = combineReducers({
    AuthReducer,
    WorkListReducer,
    GlobalReducer,
    AppointmentReducer,
    PaymentReducer,
    CollectionFilter
})

export default indexReducer;