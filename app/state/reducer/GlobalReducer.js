import { createAction, createReducer } from "redux-act"
import moment from "moment"

const init = {
    loading: true,
    datePickerVisible: false,
    date: new Date()
}

export const setLoading = createAction("loading")
export const setDateVisible = createAction('DateVisible')
export const setDate = createAction('Date')

export default createReducer({
    [setLoading]: (state, payload) => {
        return { ...state, loading: payload }
    },
    [setDateVisible]: (state, payload) => {
        return { ...state, datePickerVisible: payload }
    },
    [setDate]: (state, payload) => {
        return {...state, date: payload}
    }
}, init)