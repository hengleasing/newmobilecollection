import { createAction, createReducer } from "redux-act"

const init = {
    taxId: undefined,
    branchInformation: undefined,
    receiptList: undefined,
    summary: 0,
    waitCancel: 0,
    total: 0,
    eirRemainingPrinciple: {}
}

export const setTaxId = createAction('TaxID')
export const setBranchInfo = createAction('BranchInfo')
export const setReceiptList = createAction('ReceiptList')
export const setEIRRemainingPrinciple = createAction('EirRemainingPrinciple')

export default createReducer({
    [setTaxId]: (state, payload) => {
        return { ...state, taxId: payload }
    },
    [setBranchInfo]: (state, payload) => {
        return { ...state, branchInformation: payload }
    },
    [setReceiptList]: (state, payload) => {
        if(payload.start == 0)
        return {
            ...state, 
            receiptList: payload.data.tempRecord, 
            summary: payload.data.summary,
            waitCancel: payload.data.waitCancel,
            total: payload.data.total
        }
        else{
            return {...state, receiptList: [...state.receiptList, ...payload.data.tempRecord]}
        }
    },
    [setEIRRemainingPrinciple]: (state, payload) => {
        return {...state, eirRemainingPrinciple: payload }
    }
}, init)