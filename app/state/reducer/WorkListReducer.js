import { createAction, createReducer } from "redux-act"

const init = {
    assignedList: [],
    currentList: [],
    completeList: [],
    curPage: 0,
    completePage: 0,
    curLoading: true,
    comLoading: true,
    assignLoading: true,

    customerImage: [],
    customerDetail: {},
    customerBoundsManDetail: [],
    customerPayment: {},
    customerPaymentHistory: {},
    boundManImageList: [],
    assignedListCtl:{
        page:0,
        len:20
    },
    agingEIRGroup:[]

}

export const setAssignedListCtl = createAction('assignedList');
export const updateAssignedList = createAction('updateAssignedList')

export const setAssignedList = createAction('assignedList')
export const setCurrentList = createAction("currentList")
export const setCompleteList = createAction('completeList')
export const setCurPage = createAction('CureentPage')
export const setCompletePage = createAction('completePage')
export const setCurLoading = createAction('currentPageLoad')
export const setComLoading = createAction('completePageLoad')
export const setAssignLoading = createAction('assignPageLoad')

export const setCustomerImage = createAction('CustomerImage')
export const setCustomerDetail = createAction('CustomerDetail')
export const setCustomerBoundsMan = createAction('CustomerBoundsMan')
export const setCustomerPayment = createAction('CustomerPayment')
export const setCustomerPaymentHistory = createAction('CustomerPaymentHistory')

export const setBoundImage = createAction('BoundManImage')

export const clearCusDetail = createAction('clearCustomerDetail')
export const clearBoundDetail = createAction('clearBoundDetail')
export const clearCurrentWorkList = createAction('clearCurrentWorkList')
export const clearAssignedList = createAction('clearAssigned')
export const setAgingEIRFilter = createAction('setAgingEIRFilter')
export const clearCompleteList = createAction('clearCompleteList')


export default createReducer({
    [clearCompleteList]: (state, payload) => ({...state, completeList: []}),
    [setAgingEIRFilter]: (state, payload) => {
        return {...state, agingEIRGroup: payload}
    },
    [setAssignedListCtl]: (state, payload) => {
        return {...state, assignedListCtl: payload}
    },
    [updateAssignedList]: (state, payload) => {
        return {...state, assignedList: payload}
    },
    [setAssignedList]: (state, payload) => {
        if(state.assignedList.length == 0 )
            return {...state, assignedList: payload}
        else
            return {...state, assignedList: [...state.assignedList, ...payload]}
    },
    [setCurrentList]: (state, payload) => {


        let newCurrentList = state.currentList.concat(payload);

        return {...state, currentList: newCurrentList }
            
    },
    [setCompleteList]: (state, payload) => {
        if (Object.entries(state.completeList).length == 0)
            return {
                ...state, completeList: payload
            }
        else
            return {
                ...state, completeList: [...state.completeList, ...payload]
            }
    },
    [setCurPage]: (state, payload) => {
        return { ...state, curPage: payload }
    },
    [setCompletePage]: (state, payload) => {
        return { ...state, completePage: payload }
    },
    [setComLoading]: (state, payload) => {
        return { ...state, comLoading: payload }
    },
    [setCurLoading]: (state, payload) => {
        return { ...state, curLoading: payload }
    },
    [setCustomerImage]: (state, payload) => {
        return { ...state, customerImage: payload }
    },
    [setCustomerDetail]: (state, payload) => {
        return { ...state, customerDetail: payload }
    },
    [setCustomerPayment]: (state, payload) => {
        return { ...state, customerPayment: payload }
    },
    [setCustomerPaymentHistory]: (state, payload) => {
        return { ...state, customerPaymentHistory: payload }
    },
    [setCustomerBoundsMan]: (state, payload) => {
        return { ...state, customerBoundsManDetail: payload }
    },
    [setBoundImage]: (state, payload) => {
        return { ...state, boundManImageList: payload }
    },
    [clearCusDetail]: (state, payload) => {
        return { ...state, customerDetail: {}, customerImage: [], customerBoundsManDetail: [], boundManImageList: [], customerPaymentHistory: {} }
    },
    [clearBoundDetail]: (state, payload) => {
        return { ...state, boundManImageList: [], customerPaymentHistory: {} }
    },
    [clearCurrentWorkList]: (state, payload) => {
        return { ...state, currentList: []}
    },
    [clearAssignedList]: (state, payload) => {
        return { ...state, assignedList: [], curPage: 0}
    },
    [setAssignLoading]: (state, payload) => {
        return { ...state, assignLoading: payload }
    }
}, init)