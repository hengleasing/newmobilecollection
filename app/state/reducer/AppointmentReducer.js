import { createAction, createReducer } from "redux-act"
import moment from "moment"

const initialValue = {
    appointmentList: [],
    flagAppointList: false,
    appointmentListLimit: 0,
    criteriaRelation: undefined,
    segmented: undefined,
    postData: {
        appointmentId: 0,
        questionState: [],
        selectedSegment: {},
        appointmentDate: moment().format('YYYY-MM-DD'),
        appointmentTime: moment().startOf("day").format("HH:ss"),
        imageSource: {},
        comment: undefined,
    },
    progress: 0,
    appointmentLoading: true
}

export const setAppointmentList = createAction('appointmentList')
export const setFlagAppointList = createAction('flag')
export const setAppointmentListLimit = createAction('appointmentListLimit')
export const setCriteriaRelation = createAction('criteriaRelation')
export const setSegmented = createAction('segmented')
export const setPostData = createAction('PostData')
export const destroyFormValue = createAction('destroy')
export const clearAppointmentList = createAction('clearAppointmentList')
export const setProgress = createAction('Progress')
export const setAppointmentLoading = createAction('AppointmentLoading')

export default createReducer({
    [setAppointmentList]: (state, payload) => {
       
        if (Object.entries(state.appointmentList).length == 0)
            return {
                ...state, appointmentList: payload
            }
        else {
            let checkRec = false
            if((state.appointmentList.length > 0) && (payload.length > 0)){
                
                checkRec = state.appointmentList[0].id === payload[0].id
            }
            
            if (checkRec == false)
                return {
                    ...state, appointmentList: [...state.appointmentList, ...payload]
                }
            else
                return { ...state }
        }
    },
    [setFlagAppointList]: (state, payload) => {
        return { ...state, flagAppointList: payload }
    },
    [setAppointmentListLimit]: (state, payload) => {
        return { ...state, appointmentListLimit: payload }
    },
    [setCriteriaRelation]: (state, payload) => {
        return { ...state, criteriaRelation: payload }
    },
    [setSegmented]: (state, payload) => {
        return { ...state, segmented: payload }
    },
    [setPostData]: (state, payload) => {
        switch (payload.type) {
            case 'question':
                let tempQuestion = { ...state.postData}
                tempQuestion.questionState = payload.payload
                
                return { ...state, postData: tempQuestion }
            case 'select':
                let tempSelect = { ...state.postData}
                tempSelect.selectedSegment = payload.payload
                return { ...state, postData: tempSelect }
            case 'date':
                let tempDate = { ...state.postData }
                tempDate.appointmentDate = payload.payload
                return { ...state, postData: tempDate }
            case 'time':
                let tempTime = { ...state.postData }
                tempTime.appointmentTime = payload.payload
                return { ...state, postData: tempTime }
            case 'imageSource':
                let tempImage = { ...state.postData }
                tempImage.imageSource = payload.payload
                return { ...state, postData: tempImage}
            case 'comment': 
                let tempComment = { ...state.postData }
                tempComment.comment = payload.payload
                return { ...state, postData: tempComment }
            }
    },
    [destroyFormValue]:(state, payload)=> {
        return { ...state, postData: {
            questionState: [],
            selectedSegment: {},
            appointmentDate: moment().format('YYYY-MM-DD'),
            appointmentTime: moment().startOf("day").format("HH:ss"),
            imageSource: {},
            comment: undefined
        }}
    },
    [clearAppointmentList]: (state, payload) => {
        return {...state, appointmentList: [], appointmentListLimit: 0}
    },
    [setProgress]: (state, payload) => {
        return {...state, progress: payload}
    },
    [setAppointmentLoading]: (state, payload) => {
        return { ...state, appointmentLoading: payload }
    }
}, initialValue)