import { createAction, createReducer } from "redux-act"

const initialValues = {
    tokenData: null,
    userData: null,
    tempUser:{},
    progress: 0,
    announcement:[]
}

export const setTokenData = createAction('Tokendata')
export const setUserData = createAction('UseData')
export const setTempUser = createAction('tempUser')
export const resetAuthReducer = createAction('Reset')
export const setDownloadProgress = createAction('DownloadProgress')
export const setAnnouncement = createAction('Announcement')

export default createReducer({
    [setTokenData]: (state, payload) => {
        return { ...state, tokenData: payload }
    },
    [setUserData]: (state, payload) => {
        return { ...state, userData: payload }
    },
    [setTempUser]: (state, payload) => {
        return { ...state, tempUser: payload }
    },
    [resetAuthReducer]: (state, payload) => {
        return {...initialValues}
    },
    [setDownloadProgress]: (state, payload) => {
        return {...state, progress: payload }
    },
    [setAnnouncement]: (state, payload) => {
        return {...state, announcement: payload }
    }
}, initialValues)