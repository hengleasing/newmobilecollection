import { createAction, createReducer } from "redux-act";

const init = {
    filter:{
        orderByRemain:null,
        paymentStatus:null,
        overAging:null,
        agingStatus:null,
        contractNo:null,
        cusFullname:null,
        cusRegis:null
    },
    pageStack:null,
    prevPage:null
   
}

export const setForm = createAction("setForm");
export const resetForm = createAction("resetForm");
export const setPageStack = createAction("setPageStack");
export const setPrevPage = createAction("setPrevPage");
export const clearPrevPage = createAction("ClearPrevPage")

export default createReducer({
    [setPrevPage]: (state, payload) => ({...state, prevPage:payload}),
    [setPageStack]: (state, payload) => ({...state, pageStack:payload}),
    [setForm]: (state, payload) => {
       
        let newFilter = {...state.filter};
        newFilter[payload.name] = payload.value;

        return {...state, filter:newFilter};

    },
    [resetForm]: (state) => {
        let newState = {...state,
            filter:{
                orderByRemain:null,
                paymentStatus:null,
                overAging:null,
                agingStatus:null,
                contractNo:null,
                cusFullname:null,
                cusRegis:null
            }
        };
        
        return newState;

    },
    [clearPrevPage]: (state) => {
        return { ...state, prevPage: null }
    }
}, init)