import axiosInstance from "../../services/AxiosService"
import Environment from "../../Environment";
import {
    setCurrentList,
    setCurPage,
    setCompleteList,
    setComLoading,
    setCurLoading,
    setCompletePage,
    setCustomerImage,
    setCustomerDetail,
    setCustomerBoundsMan,
    setBoundImage,
    setCustomerPayment,
    setCustomerPaymentHistory,
    setAssignedList,
    clearCurrentWorkList,
    setAgingEIRFilter,
    clearCompleteList, setAssignLoading
} from "../reducer/WorkListReducer";
import NavigationService from "../../services/NavigationService";
import Axios from "axios";
import { getAppointment } from "./AppointmentAction";
import { setEIRRemainingPrinciple } from "../reducer/PaymentReducer";

export const loadEIRFilter = () => {

    return (dispatch)=>{

        const agingEIRGroup = [
            { label: " -- เลือกชั้นลูกหนี้ --",  value: null },
            { label: "ค้างชำระไม่เกิน 30 วัน",  value: 2 },
            { label: "ค้างชำระ 31 - 90 วัน",  value: 3 },
            { label: "ค้างชำระ 91 - 180 วัน",  value: 4 },
            { label: "ค้างชำระ 181 - 365 วัน",  value: 5 },
            { label: "ค้างชำระเกินกว่า 365 วัน",  value: 6 },
            { label: "ค้างชำระ 151 - 180 วัน",  value: 7 },
            { label: "ค้างชำระ 181 - 365 วัน",  value: 8 },
            { label: "ค้างชำระเกินกว่า 365 วัน",  value: 9 }
        ];

        setTimeout(() => {
            dispatch(setAgingEIRFilter(agingEIRGroup));
        }, 300);

       
    }
}

export const GetAssigned = (page, length, ...props) => {

    return async (dispatch, getState) => {
        try {

            // const data = {
            //     limit_start:page,
            //     limit_len:length,
            //     ...props
            // };

            const {filter} = getState().CollectionFilter;

        
            const data = {
                limit_start : page,
                limit_len : length,
                assignTo: "",
                paymentStatus: (filter.paymentStatus)?filter.paymentStatus.value:"", //→ สถานะการจ่ายเงิน 0, 1
                orderByRemain:  (filter.orderByRemain)?filter.orderByRemain.value:"",//->เรียงตามเงินต้นคงเหลือ 0:ต่ำสุด 1:สูงสุด
                overAging:  (filter.overAging)?filter.overAging.value:"", //→ 0 ลูกหนี้ ปกติ 1:ลูกหนี้นอก aging
                agingStatus:  (filter.agingStatus)?filter.agingStatus.value:"", //→ กรองตามชั้นลูกหนี้  id
                contractNo: (filter.contractNo)?filter.contractNo:"",
                cusFullname: (filter.cusFullname)?filter.cusFullname:"",
                cusRegis:(filter.cusRegis)?filter.cusRegis:""
            };

        
            let url = `${Environment.BASE_API}/api/customers/collections/assigned`
            let response = await axiosInstance.post(url, data)
     
            if(response.data){
                dispatch(setAssignedList(response.data))
                dispatch(setAssignLoading(false))
            }
                
        } catch (error) {
            dispatch(setAssignLoading(false))
        }
    }
}

export const updateAssignedList = value => {
    
    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/collections/tracking`
            let response = await axiosInstance.patch(url,{ids: value})
            
            if(response.data)
                return true
        } catch (error) {
            return false
        }
    }
}

export const GetCurrentWorkList = (start, length) => {

    return async (dispatch, getState) => {
    
        try {

            const {filter} = getState().CollectionFilter;

        
            const data = {
                limit_start : start,
                limit_len : length,
                assignTo: "",
                paymentStatus: (filter.paymentStatus)?filter.paymentStatus.value:"", //→ สถานะการจ่ายเงิน 0, 1
                orderByRemain:  (filter.orderByRemain)?filter.orderByRemain.value:"",//->เรียงตามเงินต้นคงเหลือ 0:ต่ำสุด 1:สูงสุด
                overAging:  (filter.overAging)?filter.overAging.value:"", //→ 0 ลูกหนี้ ปกติ 1:ลูกหนี้นอก aging
                agingStatus:  (filter.agingStatus)?filter.agingStatus.value:"", //→ กรองตามชั้นลูกหนี้  id
                contractNo: (filter.contractNo)?filter.contractNo:"",
                carRegisNo: (filter.carRegisNo)?filter.carRegisNo:"",
                cusName: (filter.cusName)?filter.cusName:"",
                isComplete: "false"
            };

            let url = `${Environment.BASE_API}/api/customers/collections/tracking`
  
            let response = await axiosInstance.post(url, data)

            if (response.data) {
                dispatch(setCurrentList(response.data))
                dispatch(setCurLoading(false))
            }
        } catch (error) {
            dispatch(setCurLoading(false))
        }
    }
}

export const GetCompleteWorkList = (start, length) => {

    return async (dispatch, getState) => {

        let url = `${Environment.BASE_API}/api/customers/collections/trackComplete`
        try {
            const {filter} = getState().CollectionFilter;
            const data = {
                limit_start : start,
                limit_len : length,
                assignTo: "",
                paymentStatus: (filter.paymentStatus)?filter.paymentStatus.value:"", //→ สถานะการจ่ายเงิน 0, 1
                orderByRemain:  (filter.orderByRemain)?filter.orderByRemain.value:"",//->เรียงตามเงินต้นคงเหลือ 0:ต่ำสุด 1:สูงสุด
                overAging:  (filter.overAging)?filter.overAging.value:"", //→ 0 ลูกหนี้ ปกติ 1:ลูกหนี้นอก aging
                agingStatus:  (filter.agingStatus)?filter.agingStatus.value:"", //→ กรองตามชั้นลูกหนี้  id
                contractNo: (filter.contractNo)?filter.contractNo:"",
                carRegisNo: (filter.carRegisNo)?filter.carRegisNo:"",
                cusName: (filter.cusName)?filter.cusName:"",
                isComplete: "true"
            };

            let response = await axiosInstance.post(url, data)

            if (response.data) {
                dispatch(setCompleteList(response.data))
                dispatch(setComLoading(false))
            }
        } catch (error) {
            dispatch(setComLoading(false))
        }

    }
}

export const SetLimit = (num, type) => {
    return async dispatch => {
        if (type == 'cur') {
            dispatch(setCurPage(num))
        }
        else {
            dispatch(setCompletePage(num))
        }
    }
}

export const GetCustomerImage = (customerCollectionID) => {
    return async dispatch => {
        try {

            let url = `${Environment.BASE_API}/api/customers/collections/${customerCollectionID}/images`
            let response = await axiosInstance.get(url)

            if (response.data) {
                dispatch(setCustomerImage(response.data))
            }

        } catch (error) {
            console.log(error, 'error')
        }
    }
}

export const getCustomerDetail = (collectionID, contractNo) => {

    const CusDetail = async () => {
        let url = `${Environment.BASE_API}/api/customers/collections/${collectionID}`
        return await axiosInstance.get(url)
    }

    const Boundsman = async () => {
        let url = `${Environment.BASE_API}/api/customers/collections/${collectionID}/bondsmans`
        return await axiosInstance.get(url)
    }
    const CusImg = async () => {
        let url = `${Environment.BASE_API}/api/customers/collections/${collectionID}/images`
        return await axiosInstance.get(url)
    }

    const CusPayment = async () => {
        let url = `${Environment.BASE_API}/api/customers/collections/${collectionID}/payments`
        return await axiosInstance.get(url)
    }
    const getRemainingEIR = async () => {
        let url = `${Environment.BASE_API}/api/customers/collections/remeningPrincipleByContract/${contractNo}`
        return await axiosInstance.get(url)
    }

    return async dispatch => {
        try {
            let response = await Axios.all([await CusDetail(), await Boundsman(), await CusImg(), await CusPayment(), await getRemainingEIR()])

            if (response) {
                let index = await response.map((rec, index) => {
                    index == 0 && dispatch(setCustomerDetail(rec.data))
                    index == 1 && dispatch(setCustomerBoundsMan(rec.data))
                    index == 2 && dispatch(setCustomerImage(rec.data))
                    index == 3 && dispatch(setCustomerPayment(rec.data))
                    index == 4 && dispatch(setEIRRemainingPrinciple(rec.data))
                    return index
                })

                if (index.length == 5)
                    return {
                        status:true,
                        cusDetail: response[0].data
                    }
            }
        } catch (error) {
            console.log(error, error.response)
        }
    }
}

export const getCustomerPaymentByDate = (collectionID, date) => {
    return async dispatch => {

        try {
            let url = `${Environment.BASE_API}/api/customers/collections/${collectionID}/payments?paymentDate=${date}`
            let response = await axiosInstance.get(url)
            
            if (response.data)
                dispatch(setCustomerPayment(response.data))
        } catch (error) {
            console.log(error)
        }
    }
}

export const getImageBoundMan = (collectionID, collectionContactID) => {
 
    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/collections/${collectionID}/bondsman/${collectionContactID}/images`
            let response = await axiosInstance.get(url)

            if (response.data) {
                dispatch(setBoundImage(response.data))
                NavigationService.navigate('BoundsmanDetail', { id: collectionContactID })
            }
        } catch (error) {
            console.log(error)
        }
    }
}

export const getPaymentHistory = collectionID => {

    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/collections/${collectionID}/payments/histories`
            let response = await axiosInstance.get(url)
            
            if (response.data) {
                dispatch(setCustomerPaymentHistory(response.data))
            }
        } catch (error) {
            console.log(error)
        }
    }
}

export const totalPaidOnMobile = contractId => {
    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/collections/GetTotalTemporaryReceiptByContract/${contractId}`
            let response = await axiosInstance.get(url)
            
            return response.data
          
        } catch (error) {
          
        }
    }
}

export const finishWork = id => {
 
    return async (dispatch, getState) => {
        try {
            let url = `${Environment.BASE_API}/api/customers/collections/completed`
            let response = await axiosInstance.patch(url,{id})
            
            if(response.data){
                dispatch(clearCurrentWorkList())
                dispatch(clearCompleteList())
                await GetCurrentWorkList(0,10)(dispatch, getState)
                // await GetCompleteWorkList(0,10)(dispatch)
            }
          
        } catch (error) {
          console.log(error, error.response)
        }
    }
}

export const deleteAppointDetail = (id, data) => {
    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/appointments/${id}/cancel`
            let response = await axiosInstance.patch(url,data)
          
            if(response.status == 200)
                getAppointment (response.data.contractNo, 0, 10)(dispatch)
        } catch (error) {
            console.log(error)
        }
    }
}