import axiosInstance from "../../services/AxiosService"
import Environment from "../../Environment";
import qs from "qs";
import Axios from "axios";
import { ToastAndroid } from "react-native";
import { setTokenData, setUserData, setTempUser, setAnnouncement } from "../reducer/AuthReducer";
import AsyncStorage from "@react-native-community/async-storage";
import NavigationService from "../../services/NavigationService";


export const getTokenWithSelBranch = (data) => {
    return (dispatch) => {
        var CancelToken     = Axios.CancelToken;
        var CanTokenSource  = CancelToken.source();

        return axiosInstance.post(Environment.AUTH_API + `/api/token?groupId=${data.groupId}&branchId=${data.branchId}`, qs.stringify(data), {
            headers: {'Content-Type': "application/x-www-form-urlencoded"}
        }).then(async (response) => {
            await AsyncStorage.multiSet([
                ['username', data.username],
                ['accessToken', response.data.access_token],
                ['refreshToken', response.data.refresh_token]
            ])
          
            dispatch(setTokenData(response.data))
        }).catch(error => {
       
            if(error && error.response.data){
                if(error.response.data.error = 'invalid_grant')
                    ToastAndroid.showWithGravity('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง',ToastAndroid.LONG,ToastAndroid.CENTER)
            }

            NavigationService.navigate('Login')
            setTimeout(() => {
                CanTokenSource.cancel("ไม่สามารถเชื่อมต่อได้");
                // setSpinner(false)
            }, 30000);

        })
    }
}


export const Auth = (dataUser) => {
   
    return async dispatch => {


        let data = qs.stringify(dataUser)
        let url = `${Environment.AUTH_API}/api/token`
    
        try {
            var CancelToken     = Axios.CancelToken;
            var CanTokenSource  = CancelToken.source();

            let response =  await axiosInstance.post(url, data , {
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                cancelToken: CanTokenSource.token
            }) 
            
            if(response.data){
                await AsyncStorage.multiSet([
                    ['username', dataUser.username],
                    ['accessToken', response.data.access_token],
                    // ['refreshToken', response.data.refresh_token]
                ])
                
                dispatch(setTempUser({password:dataUser.password,username:dataUser.username}))
                dispatch(setTokenData(response.data))
                getUser()(dispatch)
              
                NavigationService.navigate('Secondary')
            }
            
        } catch (error) {
            AsyncStorage.clear()
    
            if(error && error.response.data){
                if(error.response.data.error = 'invalid_grant')
                    ToastAndroid.showWithGravity('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง',ToastAndroid.LONG,ToastAndroid.CENTER)
            }

            NavigationService.navigate('Login')
            setTimeout(() => {
                CanTokenSource.cancel("ไม่สามารถเชื่อมต่อได้");
                // setSpinner(false)
            }, 30000);
        }
    }
}

export const getUser = () => {
    return async dispatch => {
        let url = `${Environment.BASE_API}/api/users/me`
        try {

            let response = await axiosInstance.get(url)
           
            if (response.data) {
                dispatch(setUserData(response.data))
            }
        } catch (error) {
           
            error.response.data.message !== undefined ?
                ToastAndroid.show(error.response.data.message, ToastAndroid.LONG) : ToastAndroid.show(error.message, ToastAndroid.LONG)
        }
    }
}

export const appVersionCheck = () => {
    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/app/vesions/lasted`
            let response = await axiosInstance.get(url)
            if(response.data)
                return response.data
        } catch (error) {
            if(error.response)
                return error.response
            else
                return error
        }
    }
}

export const GetAnnonucement = () => {
    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/appointments/getAnnouncement`
            let response = await axiosInstance.get(url)
            
            if(response.data){
                response.data.map(announceObj => {
                    GetAnnouncementImage(announceObj.imageName)(dispatch)
                })
            }
        } catch (error) {
            
        }
   
    }
}

export const GetAnnouncementImage = imageName => {

    return async dispatch => {
        let url = `${Environment.MINIO_COM_PATH}/v1./download/files`
        const formData = new FormData();

        formData.append("AppName", "com")
        formData.append("BucketName", "beacon")
        formData.append("MediaFiles",imageName)

        let response = await Axios.post(url,formData,{
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })

        if(response.data){
            dispatch(setAnnouncement(response.data))
        }

    }
}

export const saveAnnouncement = data => {
    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/appointments/saveAnnouncement`
            let response =  await axiosInstance.post(url, data )
           
        } catch (error) {
           
        }
       

    }
}