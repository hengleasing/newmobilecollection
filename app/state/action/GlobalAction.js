import { setDate } from "../reducer/GlobalReducer"

export const DateGlobalAction = date => {
    return dispatch => {
        dispatch(setDate(date))
    }
}