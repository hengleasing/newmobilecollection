import Environment from "../../Environment";
import axiosInstance from "../../services/AxiosService";
import { 
    setAppointmentList, 
    setFlagAppointList, 
    setAppointmentListLimit, 
    setCriteriaRelation, 
    setSegmented, 
    setPostData, 
    destroyFormValue, 
    clearAppointmentList,
    setProgress, setAppointmentLoading
} from "../reducer/AppointmentReducer";
import { setLoading } from "../reducer/GlobalReducer";

export const getAppointment = (contract, start, limit) => {

    return async dispatch => {
        try {

            let url = `${Environment.BASE_API}/api/customers/appointments/${contract}?limit_start=${start}&limit_len=${limit}`
            let response = await axiosInstance.get(url)
     
            if (response.data)
                dispatch(setAppointmentList(response.data))
                dispatch(setAppointmentLoading(false))
        } catch (error) {
            dispatch(setAppointmentLoading(false))
        }
    }
}

export const getCriteriaRealation = () => {
    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/appointments/GetTrackingCriteriaRelation`
            let response = await axiosInstance.get(url)

            if(response.data)
                dispatch(setCriteriaRelation(response.data))
        } catch (error) {
            console.log(error)
        }
    }
}

export const getSegmented = (contractNo) => {
    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/appointments/${contractNo}/getTrackingCriteria`
            let response = await axiosInstance.get(url)
           
            if(response.data)
                dispatch(setSegmented(response.data))
        } catch (error) {
            
        }
    }
}

export const setflag = value => {
    return dispatch => {
        if(value == false)
            dispatch(clearAppointmentList())
        dispatch(setFlagAppointList(value))
    }
}

export const setLimitAppointment = limit => {
    return dispatch => {
        dispatch(setAppointmentListLimit(limit))
    }
}

export const getAnswer = values => {
    let data = {
        headIds: values
    }

    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/appointments/getAnswer`
            let response = await axiosInstance.post(url, data)
            
            if(response.data)
                return response.data    
        } catch (error) {
            
        }
        
    }
}

export const preSaveData = (type, data) => {
    let obj = {
        type,
        payload: data
    }
    return dispatch => {
        dispatch(setPostData(obj))
    }
}

export const postAppointmentData = (addAppointmentData, meadiaFile) => {
    
    var formdata = new FormData()
    meadiaFile.forEach( (file) => {
        file.forEach( (image) => {
            formdata.append(
                'MediaFiles',{
                    uri:image.uri,
                    type:image.filetype,
                    name:image.filename
                }
            )
        })
    })
   
    formdata.append(
        'datas',JSON.stringify(addAppointmentData)
    )
    return async dispatch => {
        dispatch(setLoading(true))
        try {

            let url = `${Environment.BASE_API}/api/customers/appointments/save`
            let response = await axiosInstance.post(url, formdata, {
                headers: {
                    'Content-Type':'multipart/form-data'
                },
              onUploadProgress: function(progressEvent) {
                var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                dispatch(setProgress(percentCompleted))
               
              },
            })
          
            if(response.data){
                dispatch(setLoading(false))
                dispatch(destroyFormValue())
                dispatch(setProgress(0))
                return response.data
            }
        } catch (error) { 
            dispatch(setLoading(false))
            if(error.response){
                dispatch(setLoading(false))
                dispatch(setProgress(0))
               
                return error.response
            }
        }
    }
}