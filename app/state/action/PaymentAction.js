import axiosInstance from "../../services/AxiosService";
import Environment from "../../Environment";
import { setTaxId, setBranchInfo, setReceiptList } from "../reducer/PaymentReducer";

export const getTaxId = () => {
    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/collections/CompanyTaxID`
            let response = await axiosInstance.get(url)
            if(response.data){
                dispatch(setTaxId(response.data[0][0].ltD_TAX_NO))
                dispatch(setBranchInfo(response.data[1]))
            }
        } catch (error) {
            
        }
    }
}

export const getTempReceipt = data => {

    return async dispatch => {
        try {
            let postData = {
                contractNo: data.contractNo,
                receiptAmount: parseFloat(data.receiptAmount)
            }
            let url = `${Environment.BASE_API}/api/customers/collections/printTemporaryReceipt`
        
            let response = await axiosInstance.post(url, postData)

            if(response.data)
                return response.data
        } catch (error) {
            return { status: false, data: error.response.data }
        }
    }
}

export const receiptList = (start, length) => {
    return async dispatch => {
        try {
            let url = `${Environment.BASE_API}/api/customers/collections/tempReceiptReport?Start=${start}&Limit=${length}`
            let response = await axiosInstance.get(url)

            if(response.data)
                dispatch(setReceiptList({data: response.data, start}))
        } catch (error) {
            
        }
    }
}

export const cancelReceipt = (id, reasonId, limit) => {
    return async dispatch => {
      
        try {
            let url = `${Environment.BASE_API}/api/customers/collections/CancelTempReceipt?ID=${id}&reason=${reasonId}`
            let response = await axiosInstance.post(url)
           
            if(response.data)
                receiptList(0, limit)(dispatch)
            
        } catch (error) {
            console.log(error,error.response)
        }
    }
}

export const checkIsCloseContract = contractNo => {console.log(contractNo)
    return async dispatch => {
        try{
            let url = `${Environment.BASE_API}/api/customers/collections/CheckColseContract?contractNo=${contractNo}`
            let response = await axiosInstance.get(url);
            
            if(response.data != undefined){
                return {status: true, data: response.data}
            }
        }
        catch(error){
            return {status: false, data: error.response}
        }
    }
}